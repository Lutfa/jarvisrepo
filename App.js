/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from "react"
 import { NavigationContainer } from "@react-navigation/native"
 import {
  
   createStackNavigator,
 } from "@react-navigation/stack"
 
 import Intro from "./Class/Intro/Intro"
 import Login from "./Class/Login/Login"
 import Register from "./Class/Register/Register"
 import Otp from "./Class/OTP/NewOtp"

 import GetHelp from "./Class/Login/GetHelp"
 import Home from './Class/Home/Home';
 import TabNavigationBar from './Class/Home/TabNavigationBar';
 import CreateAddress from "./Class/Address/CreateAddress"
 import Address from "./Class/Address/Address"
 import AddressSearch from "./Class/Address/AddressSearch"
 import AddMedicine from "./Class/AddMedicine/AddMedicine"
 import AllMedicine from "./Class/AddMedicine/AllMedicine"
 import Chat from "./Class/Chat/Chat"
 import HealthRecords from "./Class/HealthRecords/HealthRecords"
 import AddHealthRecord from "./Class/HealthRecords/AddHealthRecord"
 import HealthRecordDetails from "./Class/HealthRecords/HealthRecordDetails"

 import Profile from "./Class/Profile/Profile"
 import CreatePin from "./Class/Pin/Pin"
 import ForgotPassword from "./Class/ForgotPassword/ForgotPassword"
 import EditProfile from "./Class/Profile/EditProfile"
 import AddDiscount from "./Class/Discount/AddDiscount"
 import BookAppointment from "./Class/Appointment/BookAppointment"
 import Pharmacist from "./Class/Appointment/Pharmacist"
 import AppointmentQuestion from "./Class/Appointment/AppointmentQuestion"
 import MyAppointments from "./Class/Appointment/MyAppointments"
 import LoginOptions from "./Class/Login/LoginOptions"
 import VerifyOtp from "./Class/OTP/VerifyOtp"
 import AppointmentConfirmed from "./Class/Appointment/AppointmentConfirmed"
 import {
   DefaultTheme,
   Provider as PaperProvider,
 } from "react-native-paper"
 import { navigationRef } from "./RootNavigation"
 import PillAlert from "./Class/PillAlert"
import AppointmentDetails from "./Class/Appointment/AppointmentDetails"
import OurPharmacist from "./Class/Appointment/OurPharmacist"
 import VideoCall from  "./Class/Appointment/VideoCall"
import LoginWithEmail from "./Class/Login/LoginWithEmail"
import Password from "./Class/Login/Password"
import VerifyNumber from "./Class/Register/VerifyNumber"
import BloodPressure from "./Class/Profile/subScreens/BloodPressure"
import Promotions from "./Class/Promotions"
import Comments from "./Class/Promotions/subscreens/Comments"
 console.disableYellowBox = true
 
 const Stack = createStackNavigator()
 
 const theme = {
   ...DefaultTheme,
   colors: {
     ...DefaultTheme.colors,
     primary: "#f36633",
     primaryAlpha: "#f9b299",
     accent: "yellow",
   },
 }

 function App() {
   return (
     <PaperProvider theme={theme}>
       <NavigationContainer ref={navigationRef}>
         <Stack.Navigator
           initialRouteName="TabNavigationBar"
           screenOptions={{
             headerShown: true,
             cardOverlayEnabled: true,
             gestureEnabled: false,
             cardStyle: {
               backgroundColor: "transparent",
             },
           }}>
           <Stack.Screen
             name="Intro"
             component={Intro}
             options={{ headerShown: false }}
           />
            <Stack.Screen
             name="AppointmentConfirmed"
             component={AppointmentConfirmed}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="GetHelp"
             component={GetHelp}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="VerifyOtp"
             component={VerifyOtp}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="Login"
             component={Login}
             options={{ headerShown: false }}
           />
            <Stack.Screen
             name="LoginOptions"
             component={LoginOptions}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="TabNavigationBar"
             component={TabNavigationBar}
             options={{ headerShown: false }}
           />
            <Stack.Screen
             name="VerifyNumber"
             component={VerifyNumber}
             options={{ headerShown: false }}
           />
            <Stack.Screen
             name="VideoCall"
             component={VideoCall}
             options={{ headerShown: false }}
           />
            <Stack.Screen
             name="AppointmentQuestion"
             component={AppointmentQuestion}
             options={{ headerShown: false }}
           />
             <Stack.Screen
             name="OurPharmacist"
             component={OurPharmacist}
             options={{ headerShown: false }}
           />
            <Stack.Screen
             name="AppointmentDetails"
             component={AppointmentDetails}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="Register"
             component={Register}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="MyAppointments"
             component={MyAppointments}
             options={{ headerShown: false }}
           />
           
           <Stack.Screen
             name="Otp"
             component={Otp}
             options={{ headerShown: false }}
           />
          
           <Stack.Screen
             name="BookAppointment"
             component={BookAppointment}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="Pharmacist"
             component={Pharmacist}
             options={{ headerShown: false }}
           />
          
           <Stack.Screen
             name="Home"
             component={Home}
             options={{ headerShown: false }}
           />
         
           <Stack.Screen
             name="Address"
             component={Address}
             options={{ headerShown: false }}
           />

           <Stack.Screen
             name="AddressSearch"
             component={AddressSearch}
             options={{ headerShown: true }}
           />

           <Stack.Screen
             name="BloodPressure"
             component={BloodPressure}
             options={{ headerShown: false }}
           />

            <Stack.Screen
             name="Promotions"
             component={Promotions}
             options={{ headerShown: false }}
           />

            <Stack.Screen
             name="Comments"
             component={Comments}
             options={{ headerShown: false }}
           />
 
           <Stack.Screen
             name="AddAddress"
             component={CreateAddress}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="AddMedicine"
             component={AddMedicine}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="AllMedicine"
             component={AllMedicine}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="Chat"
             component={Chat}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="HealthRecords"
             component={HealthRecords}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="AddHealthRecord"
             component={AddHealthRecord}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="HealthRecordDetails"
             component={HealthRecordDetails}
             options={{ headerShown: false }}
           />
         
         
           <Stack.Screen
             name="Profile"
             component={Profile}
             options={{ headerShown: false }}
           />
 
           <Stack.Screen
             name="ForgotPassword"
             component={ForgotPassword}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="CreatePin"
             component={CreatePin}
             options={{ headerShown: false }}
           />
 
         
 
           <Stack.Screen
             name="EditProfile"
             component={EditProfile}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="AddDiscount"
             component={AddDiscount}
             options={{ headerShown: false }}
           />
         
          
           <Stack.Screen
             name="PillAlert"
             component={PillAlert}
             options={{ headerShown: false }}
           />
            <Stack.Screen
             name="LoginWithEmail"
             component={LoginWithEmail}
             options={{ headerShown: false }}
           />
           <Stack.Screen
             name="Password"
             component={Password}
             options={{ headerShown: false }}
           />
         </Stack.Navigator>
       </NavigationContainer>
     </PaperProvider>
   )
 }
 
 
 
 export default App
 
