import React, { Component } from "react"
import {
  View,
  Text,
  Image,
  Modal,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
  AsyncStorage,
} from "react-native"
import { StackActions } from "@react-navigation/native"
import images from "./Images"
import BaseStyle from "./BaseStyles"
const { width, height } = Dimensions.get("window")
import ImagePicker from "react-native-image-crop-picker"
import Constant from "./Constant"

class PickImage extends Component {
  render() {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={true}
        onRequestClose={() => {
          this.props.onBack()
        }}>
        <TouchableOpacity
          activeOpacity={2}
          onPress={() => {
            this.props.onBack()
          }}
          style={{
            flex: 1,
            backgroundColor: "rgba(0,0,0,0.5)",
            position: "absolute",
            bottom: 0,
            top: 0,
            left: 0,
            right: 0,
          }}
        />
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
          }}>
          <View
            style={[
              styles.popupCard,
              {
                height: this.props.profile
                  ? 230
                  : 180,
              },
            ]}>
            <View
              style={{
                backgroundColor:
                  Constant.appColorAlpha(),
                alignItems: "center",
                height: 50,
                justifyContent: "center",
              }}>
              <Text
                style={[
                  {
                    color: "#ffffff",
                    fontSize: 16,
                  },
                  BaseStyle.boldFont,
                ]}>
                Upload image
              </Text>
            </View>
            <TouchableOpacity
              style={{
                flexDirection: "row",
                alignItems: "center",
              }}
              onPress={this._pickCamImage}>
              <View style={styles.containerView}>
                <Image
                  tintColor={Constant.appColorAlpha()}
                  style={{ height: 25, width: 25 }}
                  source={images.camera}
                />
              </View>
              <View
                style={{ marginLeft: 15, top: 11 }}>
                <Text
                  style={[BaseStyle.mediumFont]}>
                  Camera
                </Text>
              </View>
            </TouchableOpacity>
            <View
              style={{
                borderBottomWidth: 0.5,
                borderColor: "#c7c7c7",
                top: 1,
              }}>
              <Text></Text>
            </View>
            <TouchableOpacity
              style={{
                flexDirection: "row",
                alignItems: "center",
              }}
              onPress={this._pickImage}>
              <View style={styles.containerView}>
                <Image
                  tintColor={Constant.appColorAlpha()}
                  source={images.gallery}
                  style={{ height: 25, width: 25 }}
                />
              </View>
              <View
                style={{ marginLeft: 15, top: 10 }}>
                <Text
                  style={[BaseStyle.mediumFont]}>
                  Choose from Gallery
                </Text>
              </View>
            </TouchableOpacity>

            {this.props.profile ? (
              <View
                style={{
                  borderBottomWidth: 0.5,
                  borderColor: "#c7c7c7",
                  top: 1,
                }}>
                <Text></Text>
              </View>
            ) : null}
            {this.props.profile ? (
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                }}
                onPress={() => {
                  this.props.selectedImage("null")
                }}>
                <View style={styles.containerView}>
                  <Image
                    source={images.delete}
                    style={{
                      height: 25,
                      width: 25,
                    }}
                  />
                </View>
                <View
                  style={{
                    marginLeft: 15,
                    top: 10,
                  }}>
                  <Text
                    style={[BaseStyle.mediumFont]}>
                    Remove
                  </Text>
                </View>
              </TouchableOpacity>
            ) : null}
          </View>
        </View>
      </Modal>
    )
  }

  _pickCamImage = async () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      compressImageQuality: 0.7,
    }).then((image) => {
      console.log(image)
      this.props.selectedImage(image.path)
    })
  }

  _pickImage = async () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      multiple: false,
      compressImageQuality: 0.7,
    }).then((image) => {
      console.log(image)
      this.props.selectedImage(image.path)
    })
  }
}

PickImage.defaultProps = {
  profile: false,
}

const styles = StyleSheet.create({
  popupCard: {
    height: 230,
    width: width - 90,
    backgroundColor: "#fff",
  },
  containerView: {
    top: 10,
    height: 40,
    width: 40,
    left: 5,
    right: 5,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#c7c7c7",
    alignItems: "center",
    justifyContent: "center",
  },
  popupimageStyles: {
    height: 20,
    width: 20,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    marginTop: 10,
    bottom: 5,
  },
})

export default PickImage
