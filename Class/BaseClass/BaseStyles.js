import { StyleSheet,Dimensions } from 'react-native';
import Constant from './Constant'

const { width, height } = Dimensions.get("window");


export default StyleSheet.create({
  
  headerFont:{
    fontSize:20,
    fontFamily:'Roboto-Bold',
        fontWeight: "500",
       
        color:"#000"
  }, headerFontwhite:{
    fontSize:20,
    fontFamily:'Roboto-Bold',
        fontWeight: "500",
       
        color:"#fff",
        marginLeft:20
  }, headerFontblack:{
    fontSize:20,
    fontFamily:'Boboto-Black',
        fontWeight: "500",
        marginLeft:18,
        color:"#000"
  },
  descFontblack:{
    fontSize:12,
    fontFamily:'Boboto-Regular',
        fontWeight: "200",
        
        color:"#000"
  },
  boldFont:{
    fontFamily:'Roboto-Bold',
        fontWeight: "200",
  },
  mediumFont:{
    fontFamily:'Roboto-Medium',
        fontWeight: "200",
  },
  regularFont:{
    fontFamily:'Roboto-Regular',
        fontWeight: "200",
  },
  circle: {


   
    backgroundColor: "#fff",
    borderRadius: 40,
    height: 33, width: 33, elevation: 6, justifyContent: "center", alignContent: "center", alignItems: "center"

  },
 topRadiusLayout:
 {paddingHorizontal:20,marginTop:30,backgroundColor:"#fff",borderTopLeftRadius:28,borderTopRightRadius:28,width:"100%",height:"100%", paddingTop:20}
,
cornerRadiusView:
 {backgroundColor:"#fff",borderRadius:28, paddingTop:20}
,
cornerRadiusView14:
 {backgroundColor:"#fff",borderRadius:14, paddingTop:20}
,
smallIconStyle:{
  height: 20, width: 20, 
}
});
