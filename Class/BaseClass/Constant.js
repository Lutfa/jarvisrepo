import React from "react"
import {
  View,
  Text,
  Image,
  Modal,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
  AsyncStorage,
} from "react-native"
import { StackActions } from "@react-navigation/native"
import images from "./Images"
import BaseStyle from "./BaseStyles"
const { width, height } = Dimensions.get("window")
import ImagePicker from "react-native-image-crop-picker"
const axios = require("axios")

class Constant {
  static url =
    "https://pablopharmacy.azurewebsites.net/api/"
  static loginTime = ""

  baseUrl() {
    return "https://pablopharmacy.azurewebsites.net/api/"
  }
 pharmacyId(){
return 1
 }
  pushScreen(screenName, props) {
    const pushAction = StackActions.push(screenName)
    props.navigation.dispatch(pushAction)
  }

  price() {
    return "€"
  }
orange(){
  return "#FF0055"
}
  appColor() {
    return "#FF0055"
    // return "#eb3c34"
  }

  appColorAlpha() {
    return "#f36633"
    // return "#eb3c34"
  }

  textFieldBorderColor() {
    return "#BABABA"
    
  }
  
  appLightColor() {
    return "#f9b299"
  }

  colorOne() {
    return "#f9b299"
  }

  colorTwo() {
    return "#f36633"
  }
N
  homeNonSelected() {
    return "#BFC6D6"
  }

  appFullColor() {
    return "#ff2500"
  }

  yellowColor() {
    return "#FCB232"
  }

  blueColor() {
    return "#0059A0"
  }

  prescriptionColorCode() {
    return "#b20000"
  }
  redColor() {
    return "#FF4C61"
  }

  pinkColor() {
    return "#FF359E"
  }

  lightGray() {
    return "#C2C8D4"
  }

  selectedTextColor() {
    return "#fff"
  }

  textColor() {
    return "#62646A"
  }
  textGreyColor() {
    return "#909090"
  }
  
  textAlpha() {
    return "#A0B3BF"
  }
  
  redColor() {
    return "#DE3145"
  }

  headerColor() {
    return "#f36633"
  }
  secondColor() {
    return "#f36633"
  }
gradientColor1(){
  return "#55FCE4"
}
gradientColor2(){
  return "#00DEE0"
}
  showSuccess(message, show) {
    return (
      <Modal
        transparent={true}
        visible={show}
        onRequestClose={() => {
          this.setState({ showAlert: false })
        }}>
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "rgba(0,0,0,0.4)",
          }}>
          <View
            style={{
              height: 200,
              width: width - 40,
              backgroundColor: "#fff",
              borderRadius: 5,
              alignItems: "center",
              justifyContent: "center",
            }}>
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                source={images.success}
                style={{ height: 60, width: 60 }}
              />
              <Text
                style={[
                  { fontSize: 16, top: 20 },
                  BaseStyle.fontMedium,
                ]}>
                {message}
              </Text>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  postMethod = (url, params, callback) => {
    console.log(params)

    fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
       Authorization: `Bearer ${global.userToken}`,
      },
      body: JSON.stringify(params),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        callback({
          success: true,
          result: responseJson,
        })
      })
      .catch((error) => {
        callback({ success: false, result: "" })
      })
  }
  postWithTokenMethod = (url, params, callback) => {
    console.log(global.userToken)

    fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${global.userToken}`,
      },
      body: JSON.stringify(params),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        callback({
          success: true,
          result: responseJson,
        })
      })
      .catch((error) => {
        callback({ success: false, result: "" })
      })
  }

  putMethod = (url, params, callback) => {
    console.log(params)

    fetch(url, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        callback({
          success: true,
          result: responseJson,
        })
      })
      .catch((error) => {
        callback({ success: false, result: "" })
      })
  }

  deleteMethod = (url, params, callback) => {
    console.log(params)

    console.log(url)

    fetch(url, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    })
      .then((response) => response.json())
      .then((responseJson) => {
        callback({
          success: true,
          result: responseJson,
        })
      })
      .catch((error) => {
        callback({ success: false, result: "" })
      })
  }

  getMethod = (url, callback) => {
    console.log(this.baseUrl() + url)

    fetch(url, { method: "GET" })
      .then((response) => response.json())
      .then((responseJson) => {
        callback({
          success: true,
          result: responseJson,
        })
      })
      .catch((error) => {
        callback({ success: false, result: "" })
      })
  }

  uploadImageToServer = (url, uri, callback) => {
    var photo = {
      uri: uri,
      type: "image/jpeg",
      name: "photo.jpg",
    }

    var form = new FormData()
    form.append("file", photo)

    //  let config = {
    //       onUploadProgress: function(progressEvent) {
    //               let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
    //               console.log(percentCompleted);
    //               alert(percentCompleted)

    //       }
    //  };
    // axios.post(url, form, config)
    //       .then(function (res) {
    //        if (res.data != null)
    //         {
    //         callback({success: true,url:res.data});
    //         }
    //       })
    //       .catch(function (err) {
    //        callback({success: false,uri:''});
    //       });

    fetch(url, {
      body: form,
      method: "POST",
      headers: {
       
       Authorization: `Bearer ${global.userToken}`,
      },
    })
      .then((response) => response.json())
      .catch((error) => {
        callback({ success: false, uri: "" })
      })
      .then((responseData) => {
        if (responseData != null) {
          callback({
            success: true,
            url: responseData,
          })
        }
      })
      .done()
  }

  isValidPhone(phone) {
    const reg =
      /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

    if (reg.test(phone) === false) {
      return false
    } else {
      return true
    }
  }

  isValidEmail(email) {
    let reg =
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    if (reg.test(email) === false) {
      return false
    } else {
      return true
    }
  }
  

  noDataView(isLoading) {
    return (
      <View
        style={{
          height: height,
          width: width,
          justifyContent: "center",
          alignItems: "center",
        }}>
        <Image
          resizeMode={"center"}
          style={{
            top: -25,
            width: "80%",
            height: "80%",
          }}
          source={
            isLoading
              ? images.loadingNew
              : images.empty
          }
        />
      </View>
    )
  }

  showLoader(text, color = this.appColorAlpha()) {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={true}
        onRequestClose={() => {}}>
        <View
          style={{
            height: "100%",
            width: "100%",
            elevation: 5,
            alignItems: "center",
            justifyContent: "center",
            position: "absolute",
            zIndex: 2,
            backgroundColor: "rgba(0,0,0,0)",
          }}>
          {/* <View style = {{padding:30,backgroundColor:this.appLightColor(),elevation:4,borderRadius:8}}> */}
          <ActivityIndicator
            size="large"
            color={color}
          />
          <Text
            style={[
              {
                color: this.appColorAlpha(),
                fontSize: 15,
              },
              BaseStyle.regularFont,
            ]}>
            {text}
          </Text>
          {/* </View> */}
        </View>
      </Modal>
    )
  }
}

module.exports = new Constant()
