import React from "react"
import { View, Text } from "react-native"
import BaseStyle from "./BaseStyles"

export default function NoData() {
  return (
    <View
      style={{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        height: "100%",
        flexGrow: 1,
      }}>
      <Text
        style={[
          {
            fontSize: 14,
          },
          BaseStyle.regularFont,
        ]}>
        No data found
      </Text>
    </View>
  )
}
