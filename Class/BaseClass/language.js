import React from "react"
import LocalizedStrings from "react-native-localization"
const strings = new LocalizedStrings({
  en: {
    //   login
    signin: "Sign in",
    emailpassword: "Email/Phone",
    password: "Password",
    forgotpassword: "Forgot Password",
    dontHaveAccount: "Don't have an account?",
    signup: " SIGN UP ",
    selectreporttype: "Select Report Type",
createPassword:"Create Password*",
fullName:"Full Name*",
    //forgot passoword
    send: "Send",
gotoHome:"GO TO HOME",
    //   create pin

    createpin: "Create Pin",
    enterpin: "Enter pin",
    confirmPin: "Confirm pin",
    continue: "Continue",

    // register
    name: "Name",
    email: "Email",

    phonenumber: "Mobile Number",

    allReadyHaveAccount:
      "Already have an account?",

    // sidemenu
    home: "Home",
    healthrecords: "Health Records",
    addresss: "My Address",
    healthArticles: "Health Articles",
    medicationReminder: "Medication Reminder",
    orders: "Orders",
    version: "Version",

    // home
    searchLocation: "Search Location",
    uploadPres: "Upload prescription",
    chatwith: "Chat with pharmacist",
    categories: "Categories",
    healthissues: "Health Issues",
    viewall: "View All",
    bestoffers: "Best Offers",
    articlesforyou: "Articles for you",
    bestselling: "Best Selling",
    newarrivals: "New Arrivals",

    healthcarerecords: "Add Healthcare Record",
    uploadreport: "Upload Report's Snapshot",
    perscription: "Prescriptions",
    diagnosticReport: "Diagnostic Report",
    medicalClaims: "Medical Claims",
    patientName: "Patient's Name",
    doctorname: "Doctor's Name",
    reportname: "Report's Name",
    reportsummary: "Report Summary",

    save: "SAVE",
    update: "UPDATE",

    patient: "Patient",
    doctor: "Doctor",
    summary: "Summary",
    addNewAddress: "ADD NEW ADDRESS",

    type: "Type",
    houseFlat: "House/Flat No",
    landmark: "Landmark",
    city: "City",
    country: "Country",
    zipcode: "ZipCode",
    category: "Category",

    today: "Today",
    week: "Week",
    all: "All",
    bookmark: "Bookmarks",

    // upload prescripción
    Camera: "Camera",
    Gallery: "Gallery",
    myrecords: "My Records",
    secure:
      "Your attached prescription will be secure and private.Only our pharmacist will review it.",
    whyPres: "Why upload a prescription?",
    validPres: "Valid prescription guide",

    //pill reminders

    pillreminder: "Pill Reminder",
    reminder: "Reminder",
    summary: "Summary",
    morning: "Morning",
    afternoon: "Afternoon",
    evening: "Evening",
    addmedicine: "Add medicine",
    medicineform: "Medicine form",
    shape: "shape",
    color: "color",
    dose: "dose",
    times: "times",
    image: "image",
    timeslot: "select time slots",
    switchView: "Switch View",
    recurring: "recurring",
    selectall: "select all",
    chat: "chat",
    orderMedicine: "Order Medicine",
    setreminder: "Edit Reminder",
    searchmedicine: "Search Medicine",
    selectdays: "Select days",

    profile: "Profile",
    seniorDocument: "Senior Citizen Document",
    handicappedDocument: "Handicapped Document",
    bodymass: "Body Mass Index",
    overweight: "Over weight",

    height: "Height",
    weight: "Weight",
    buynow: "Buy now",

    //orders
    orders: "Orders",
    logout: "Logout",
    prescription: "Prescription",
    non_pres: "Non-prescription",
    order_no: "Order No",
    status: "Status",
    purchasedate: "Purchase date",
    review: "Your order is under review",
    confirmOrder: "Confirm order",
    medicinesfor: "Medicines for",
    self: "Self",
    Others: "Others",
    instructions: "Instructions",
    discountCode: "Apply Discount \n Coupon",
    change: "Change",
    billDetails: "Bill Details",
    prescriptionMedicines: "Prescription Medicines",
    nonPrescriptionMedicines:
      "Non Prescription Medicines",
    subtotal: "Sub Total",
    combined: "Combined",
    servicefee: "Service Fee",
    total: "Total",
    ViewPrescription: "View Prescription",
    confirmOrder: "Confirm Order",
    discount: "Discount",

    gender: "Gender",
    linkedAccount: "Linked Account",
    birthdate: "Date of Birth",
    bloodGroup: "Blood Group",
    browseDocument: "Browse Document",
    Add: "Add",

    //cart

    cart: "My cart",
    totalAmount: "Total Amount",
    checkOut: "Check out",
    continueShopping: "Continue shopping",
    next: "Next",
    skip: "Skip",
    addAddress: "Add Address",
    takePrescription: "Take prescription",
    talkToDoctor: "Talk to a doctor",
  },
  sp: {
    //   login
    signin: "Accedere",
    emailpassword: "E-mail/Telefono",
    password: "Password",
    forgotpassword: "Ha dimenticato la password",
    dontHaveAccount: "Non hai un account?",
    signup: "Registrazione",
    selectreporttype: "Seleziona la registrazione del rapporto",

    //forgot passoword
    send: "Send",

    //   create pin

    createpin: "Creare Pin",
    enterpin: "Inserisci Pin",
    confirmPin: "Conferma Pin",
    continue: "Continua",

    // register
    name: "Nome",
    email: "Email",
    phonenumber: "Numero di telefono",

    allReadyHaveAccount:
      "Hai già un account?",

    // sidemenu
    home: "Home",
    healthrecords: "Cartelle Cliniche",
    addresss: "Il mio indirizzo",
    healthArticles: "Articoli sulla salute",
    medicationReminder: "Promemoria pillola",
    orders: "Ordini",
    version: "Versione",

    // home
    searchLocation: "Cerca posizione",
    uploadPres: "Carica prescrizione",
    chatwith: "Chatta con il farmacista",
    categories: "Categorie",
    healthissues: "Problemi di salute",
    viewall: "Mostra tutto",
    bestoffers: "Le migliori offerte",
    articlesforyou: "Articoli per te",
    bestselling: "La migliore vendita",
    newarrivals: "Nuovi arrivi",

    healthcarerecords: "Aggiungi Cartella Sanitaria",
    uploadreport: "Carica l'istantanea del rapporto",
    perscription: "prescrizioni",
    diagnosticReport: "Rapporto Diagnostico",
    medicalClaims: "Affermazioni Mediche",
    patientName: "Nome paziente",
    doctorname: "Nome del dottore",
    reportname: "Nome rapporto",
    reportsummary: "Riepilogo rapporto",

    save: "Salva",
    update: "Aggiornare",

    patient: "Paziente",
    doctor: "Medico",
    summary: "Riepilogo",
    addNewAddress: "AGGIUNGI UN NUOVO INDIRIZZO",

    type: "Tipa",
    houseFlat: "Casa/Appartamento No",
    landmark: "Punto di riferimento",
    city: "Città",
    country: "Nazione",
    zipcode: "Cap",
    category: "Categoria",

    today: "Oggi",
    week: "Settimana",
    all: "Tutto",
    bookmark: "segnalibri",

    // upload prescripción
    Camera: "Telecamera",
    Gallery: "Galleria",
    myrecords: "I miei dischi",
    secure:
      "La tua prescrizione allegata sarà sicura e privata. Solo il nostro farmacista la esaminerà.",
    whyPres: "Perché caricare una prescrizione?",
    validPres: "Guida alla prescrizione valida",

    //pill reminders

    pillreminder: "Promemoria pillola",
    reminder: "Promemoria",
    summary: "Riepilogo",
    morning: "Mattina",
    afternoon: "Pomeriggio",
    evening: "Sera",
    addmedicine: "Aggiungi medicina",
    medicineform: "Modulo di medicina",
    shape: "forma",
    color: "colore",
    dose: "dose",
    times: "volte",
    image: "Immagine",
    timeslot: "seleziona le fasce orarie",
    switchView: "Cambia vista",
    recurring: "ricorrente",
    selectall: "seleziona tutto",
    chat: "chat",
    orderMedicine: "Ordina medicina",
    setreminder: "Modifica promemoria",
    searchmedicine: "Cerca Medicina",
    selectdays: "Seleziona giorni",

    profile: "Profilo",
    seniorDocument: "Documento per anziani",
    handicappedDocument: "Documento per portatori di handicap",
    bodymass: "Indice di massa corporea",
    overweight: "Sovrappeso",

    height: "Altezza",
    weight: "Peso",
    buynow: "Acquista ora",

    //orders
    orders: "Ordini",
    logout: "Disconnettersi",
    prescription: "Prescrizione",
    non_pres: "Senza prescrizione",
    order_no: "Numero d'ordine",
    status: "Stato",
    purchasedate: "Data di acquisto",
    review: "Il tuo ordine è in fase di revisione",
    confirmOrder: "Confermare l'ordine",
    medicinesfor: "medicinali per",
    self: "Se stesso",
    Others: "Altri",
    instructions: "Istruzioni",
    discountCode: "Applica sconto \n buono",
    change: "Modificare",
    billDetails: "Dettagli fattura",
    prescriptionMedicines: "Medicinali su prescrizione",
    nonPrescriptionMedicines:
      "Medicinali senza prescrizione medica",
    subtotal: "Totale parziale",
    combined: "Combinato",
    servicefee: "Costo del servizio",
    total: "Totale",
    ViewPrescription: "Visualizza prescrizione",
    confirmOrder: "Confermare l'ordine",
    discount: "Sconto",

    gender: "Genere",
    linkedAccount: "Account collegata",
    birthdate: "Data di nascita",
    bloodGroup: "Gruppo sanguigno",
    browseDocument: "Sfoglia documento",
    Add: "Aggiungere",

    //cart

    cart: "La mia carta",
    totalAmount: "Importo totale",
    checkOut: "Guardare",
    continueShopping: "Continua a fare acquisti",
    next: "Prossimo",
    skip: "Saltare",
    addAddress: "Aggiungi indirizzo",
    takePrescription: "prendi la prescrizione",
    talkToDoctor: "Parla con un dottore",
  },
})
export default strings
