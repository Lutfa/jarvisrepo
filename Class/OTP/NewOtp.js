import React, { Component } from "react"
import {
  View,
  Platform,
  AsyncStorage,
  Keyboard,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
} from "react-native"
import messaging from "@react-native-firebase/messaging"

import BaseStyle from "../BaseClass/BaseStyles"
import Constant from "../BaseClass/Constant"
import Images from "../BaseClass/Images"
import TextField from "../Login/lib/TextField"
const { width, height } = Dimensions.get("window")
import { useNavigation } from "@react-navigation/core"
import Db from "./../DB/Realm"
import {
  getUniqueId,
  getManufacturer,
} from "react-native-device-info"
import DeviceInfo from "react-native-device-info"
import urls from "../BaseClass/ServiceUrls"
import LinearGradient from "react-native-linear-gradient"
import images from "../BaseClass/Images"
import { KeyboardAvoidingScrollView } from "react-native-keyboard-avoiding-scroll-view"
import { TextInput } from "react-native-gesture-handler"
import { CommonActions } from "@react-navigation/native"
import auth from "@react-native-firebase/auth"
import Toast from "react-native-simple-toast"
import SplashScreen from "react-native-splash-screen"
import OTPInputView from "@twotalltotems/react-native-otp-input"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import BaseStyles from "../BaseClass/BaseStyles"

class NewOtp extends Component {
  static navigationOptions = {
    header: null,
  }

  constructor(props) {
    super(props)
    this.state = {
      dataArray: ["", "", "", "", "", ""],
      showSuccess: false,
      currentIndex: 0,
      phoneNumber: props.route.params.phoneNumber,
      countryCode: props.route.params.countryCode,
      parameters: props.route.params.parameters,
      confirmResult: "",
      setConfirm: "",
      confirm: "",
      code: "",
      promptSmsCode: false,
      timer: 30,
      code: "",
    }
  }

  componentDidMount() {
    SplashScreen.hide()
    this.interval = setInterval(() => {
      this.setState((prevState) => ({
        timer: prevState.timer - 1,
      }))
    }, 1000)

    // auth().signOut()
    this.initFirebase()

    this.sendOtp(
      this.state.countryCode,
      this.state.phoneNumber
    )
  }

  componentDidUpdate() {
    if (this.state.timer === 0) {
      clearInterval(this.interval)
      this.props.navigation.pop(1)

    }
  }

  componentWillUnmount() {
    clearInterval(this.interval)
    // this.props.navigation.pop(1)
    if (this.unsubscribe) this.unsubscribe()
  }

  initFirebase() {
    this.unsubscribe = auth().onAuthStateChanged(
      (user) => {
        if (user && this.state.promptSmsCode) {
          this.setState({
            showLoader: true,
            isCallingService: true,
          })

        } else {
          // User has been signed out, reset the state
          this.setState({
            confirmResult: null,
          })
        }
      }
    )
  }

  async sendOtp(countryCode, phoneNumber) {
    await auth()
      .signInWithPhoneNumber(
        countryCode + phoneNumber,
        true
      )
      .then((confirmResult) => {
        console.log("code" + JSON.stringify(confirmResult))
      //  Toast.show(JSON.stringify(confirmResult))
       // this.showAlert()
        this.setState({
          confirmResult: confirmResult,
          promptSmsCode: true,
          // code:confirmResult
        })
      })
      .catch((error) => {
        console.log(error)
        Toast.show(
          "Something went wrong" ,
          Toast.LONG
        )
      })
  }

  showAlert() {
    this.setState({ showSuccess: true })
    setTimeout(() => {
      this.setState({ showSuccess: false })
    }, 1500)
  }

  async serviceCall() {
    Toast.show(
      "service call",
      Toast.LONG
    )
    console.log("service call");
    const { email, password } = this.state
    let loginInputphone = "", loginInputemail = "", loginType = "";

    loginInputemail = email;
    loginType = "phoneNumber";
    await messaging().registerDeviceForRemoteMessages();
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      console.log("fcm token" + fcmToken);
    }
    global.deviceId = fcmToken
    let parameters = {
      pharmacyId: Constant.pharmacyId(),
      loginType: loginType,
      countryCode: this.state.countryCode,
      email: "",
      password: "",
      phoneNumber: this.state.phoneNumber,
      deviceId: fcmToken,
      deviceType:
        Platform.OS === "android"
          ? "Android"
          : "IOS",
      deviceUniqueId: DeviceInfo.getUniqueId(),
      version: DeviceInfo.getVersion(),
      deviceModel: DeviceInfo.getModel(),
      deviceBrand: DeviceInfo.getBrand(),
      deviceVersion: DeviceInfo.getSystemVersion(),
      isPhoneVerified: true
    }
    console.log("login params " + JSON.stringify(parameters))
    Constant.postMethod(
      urls.login,
      parameters,
      (result) => {
        // this.loadingButton.showLoading(false)
        this.setState({ isLoading: false })
        console.log(
          "login " + JSON.stringify(result)
        )
        if (result.success) {
          let response = result.result

          if (response.Status != "Fail") {
            let userId =
              response.response.patientData.id
            let username =
              response.response.patientData.name
            let userData = response.response.patientData

            console.log("name", username)
            global.CustomerId = userId
            global.userData = userData
            global.userToken = response.response.token

            // this.saveUserData(response.Response)

            try {
              Db.write(() => {
                Db.create(
                  "User",
                  {
                        
                    id: userData.id,
                    name: userData.name,
                    email: userData.email,
                    phoneNumber:
                      userData.phoneNumber,
                 deviceType: userData?.deviceType,
                   // Latitude: userData.Latitude,
                   // Longitude: userData.Longitude,
                    // IsPhoneVerified:
                    //   userData.isPhoneVerified,
                    countryCode:
                      userData.countryCode,
                      pictureUrl:
                      userData.pictureUrl,

                      age: userData.age,
                      sex: userData.sex,
                      temparature: userData.temparature,
                     
                      fastingBloodSugar: userData.fastingBloodSugar,
                      bloodSugar:  userData.bloodSugar,
                      bloodPressureDia: userData.bloodPressureDia,
                      bloodPressureSys: userData.bloodPressureSys,
                      pulse: userData.pulse,
                      deviceUniqueId: userData.deviceUniqueId,
                      token: global.userToken,

                
                    // Dob: userData.Dob,
                    // Gender: userData.Gender,
                    // HealthIssues:
                    //   userData.HealthIssues,
                    // SCProof: userData.SCProof,
                    // SCVerified: userData.SCVerified,
                    // PHCDocument:
                    //   userData.PHCDocument,
                    // PHCVerified:
                    //   userData.PHCVerified,
                    // FirstName: userData.FirstName,
                    // LastName: userData.LastName,
                    // DeviceId: userData.deviceId,
                    // DeviceUniqueId:
                    //   userData.deviceUniqueId,
                    // AuthToken: userData.AuthToken,
                    // ReferralCode:
                    //   userData.ReferralCode,
                    // Version: userData.version,
                    // DeviceModel:
                    //   userData.deviceModel,
                    // DeviceBrand:
                    //   userData.deviceBrand,
                    // DeviceVersion:
                    //   userData.deviceVersion,
                  },
                  "modified"
                )

                AsyncStorage.setItem(
                  "userLoggedIn",
                  "true"
                )
                if (userData.name != "" && userData.name != null && userData.name != "null") {
                  console.log("tabnav")
                  this.props.navigation.push("TabNavigationBar")

                } else {
                  console.log("Register")
                  this.props.navigation.push("Register", {

                    phoneNumber: this.state.phoneNumber,
                    countryCode: this.state.countryCode,
                    loginwithPhone: true,
                    email: ""

                  }
                  )
                }

              })
            } catch (e) {
              console.log(`${e} Error on creation`)
              Toast.show("Something went wrong")
            }
          } else {
            Toast.show("Wrong credentials")
          }
        } else {
          Toast.show("Something went wrong")
        }
      }
    )
  }





  async confirmCode(codeInput) {
    const { confirmResult ,isLoading,isCallingService} = this.state

    try {
      console.log(
        "confirming  code" +
        confirmResult._verificationId
      )

      await confirmResult
        .confirm(codeInput)
        .then((auth) => {
          Toast.show(
            "authenticated",
            Toast.LONG
          )
          console.log(auth)
          this.setState({ isLoading: true })

          // this.props.navigation.navigate('Home');
          this.serviceCall()

          this.setState({
            showLoader: true,
            isCallingService: false,
          })
          //this.registerUser()
        })
        .catch((err) => {
          console.log(err)
          //this.loadingButton.showLoading(false)

          this.setState({
            isCallingService: false,
            isLoading: false,
          })
          Toast.show(
            "Something went wrong"+err,
            Toast.LONG
          )
        })
    } catch (error) {
      console.log("Invalid code.")
      this.setState({ isLoading: false })
      Toast.show(JSON.stringify(error), Toast.LONG)
    }
  }

  render() {
    const { timer, isLoading, countryCode, phoneNumber } = this.state
    return (
      <View style={{ flex: 1, backgroundColor: "#fff" }}>
        <View style={{ flex: 1, paddingHorizontal: 20 }}>
          {this.headerView()}
          <KeyboardAwareScrollView>

            <View

            >
              <ScrollView

                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                ref="mainScroll">
                <View>
                  <View style={{ marginTop: 10, }}>

                    <Text style={[styles.headerTextStyle, BaseStyle.boldFont, { fontSize: 14 }]}>We’ve sent a code to your number</Text>
                    <Text style={[styles.headerTextStyle, BaseStyle.boldFont, { fontSize: 14, color: "#000" }]}>{countryCode} {phoneNumber}</Text>

                  </View>
                  <View
                    style={{
                      flexDirection: "row",


                    }}>
                    <View
                      style={{
                        flexDirection: "row"
                        , alignItems: "center", alignContent: "center",
                        height: 60

                      }}>
                      {/* {this.createingOtp()} */}

                      <OTPInputView
                        style={{
                          width: "60%",
                          height: 60,
                          marginTop: 10,
                          justifyContent: "flex-start"
                        }}
                        pinCount={6}
                        placeholderTextColor="#000"
                        code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                        onCodeChanged={(code) => {
                          this.setState({ code })
                        }}
                        autoFocusOnLoad
                        codeInputFieldStyle={[
                          styles.underlineStyleBase,
                          BaseStyles.boldFont,
                        ]}
                        codeInputHighlightStyle={
                          styles.underlineStyleHighLighted
                        }
                        onCodeFilled={(code) => {
                          this.setState({ code })
                          console.log(
                            `Code is ${code}, you are good to go!`
                          )
                        }}
                      />

                      <Text
                        style={[
                          {
                            color: "#000",
                            fontSize: 15,

                            marginLeft: 15,
                            textAlign: "center",
                          },
                          BaseStyles.boldFont,
                        ]}>
                        {" "}
                        {timer == 0
                          ? `Resend`
                          : `Resend otp\n in 00:${timer}`}
                      </Text>
                    </View>
                  </View>

                  <TouchableOpacity


                    onPress={() => {

                      this.sendAction()
                      // this.props.navigation.push('Otp',{number:'9959814490'})


                    }}>
                    <LinearGradient
                      start={{ x: 0, y: 1 }}
                      end={{ x: 1, y: 1 }}

                      colors={[
                        Constant.gradientColor1(),
                    Constant.gradientColor2(),


                      ]}
                      style={{ marginTop: 30, marginBottom: 10, borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: width - 40 }}>
                      <Text style={{ fontSize: 14, color: "#000" }}>CONTINUE</Text>
                    </LinearGradient>
                  </TouchableOpacity>

                  <View style={{ height: 30, width: "100%", marginTop: 50, alignContent: "center", alignItems: "center" }}>

                    <View style={{ marginTop: 10, flexDirection: "row", alignItems: "center", alignContent: "center" }}>

                      <View style={{ height: 0.5, width: "45%", backgroundColor: "#909090" }}></View>
                      <Text style={[styles.headerTextStyle, { marginTop: 2, marginHorizontal: 10, fontSize: 18, color: Constant.textGreyColor() }]}>or</Text>
                      <View style={{ height: 0.5, width: "45%", backgroundColor: "#909090" }}></View>
                    </View>
                  </View>

                  <View style={[styles.loginEmailBg, { flexDirection: "row", alignItems: "center", alignContent: "center", marginTop: 30 }]}>
                    <Image style={{ height: 20, width: 20, resizeMode: "contain", marginLeft: 30 }} source={require('../../assets/googleLogo.png')}></Image>

                    <Text style={[styles.headerTextStyle, { marginHorizontal: 20, fontSize: 14, color: "#000" }]}>CONTINUE WITH Google</Text>

                  </View>
                  <TouchableOpacity onPress={() => {
                                    this.props.navigation.navigate("LoginWithEmail")
                                }}>
                  <View style={[styles.loginEmailBg, { flexDirection: "row", alignItems: "center", alignContent: "center", marginTop: 20 }]}>
                    <Image style={{ height: 20, width: 20, resizeMode: "contain", marginLeft: 30 }} source={images.email}></Image>

                    <Text style={[styles.headerTextStyle, { marginHorizontal: 20, fontSize: 14, color: "#000" }]}>CONTINUE WITH Email</Text>

                  </View>
                  </TouchableOpacity>
                  <View style={[styles.loginEmailBg, { flexDirection: "row", alignItems: "center", alignContent: "center", marginTop: 20 }]}>
                    <Image style={{ height: 20, width: 20, resizeMode: "contain", marginLeft: 30 }} source={require('../../assets/facebookLogo.png')}></Image>

                    <Text style={[styles.headerTextStyle, { marginHorizontal: 20, fontSize: 14, color: "#000" }]}>CONTINUE WITH Facebook</Text>

                  </View>

                  <View style={{ marginTop: 30, flexDirection: "row", alignItems: "center", alignContent: "center" }}>


                    <Text style={[styles.headerTextStyle, { marginLeft: 5, fontSize: 14, color: Constant.textGreyColor() }]}>Having trouble logging in? </Text>
                    <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("GetHelp")
                    }}>
                       <Text style={[styles.headerTextStyle, { fontSize: 14, }]}>Get help</Text>

                    </TouchableOpacity>
                  </View>
                  <Text style={[styles.headerTextStyle, { marginLeft: 5, marginTop: 20, fontSize: 12, color: Constant.textGreyColor() }]}>By continuing, I agree to the Terms of Use & Privacy Policy </Text>



                  <View style={{ height: 20 }} />
                </View>
              </ScrollView>
            </View>
          </KeyboardAwareScrollView>
          {Constant.showSuccess(
            "OTP sent successfully",
            this.state.showSuccess
          )}
        </View>

      </View>
    )
  }

  sendAction() {


    if (
      this.state.code != "" &&
      this.state.code.length == 6
    ) {
      console.log("otp success ")
      //  this.loadingButton.showLoading(true)
      this.setState({ isLoading: true })
      this.confirmCode(this.state.code)
    } else {
      this.setState({ isLoading: false })

      Toast.show("Please enter OTP")



      this.setState({
        showLoader: true,
        isCallingService: false,
      })
    }

    // mock
    setTimeout(() => {
      // this.loadingButton.showLoading(false);
      this.props.navigation.push('LoginOptions')

    }, 1000);
  }

  headerView() {
    const { drawerOpen } = this.state

    const userData = global.userData

    return (
      <SafeAreaView
        style={[
          styles.headerStyle, { marginTop: 10 }

        ]}>
        <StatusBar
          translucent
          barStyle="light-content"
          //  backgroundColor="rgba(0, 0, 0, 0.251)"
          backgroundColor={Constant.headerColor()}

        />
        <TouchableOpacity
          style={[styles.headerInnerView, { marginTop: 20 }]}>
          <TouchableOpacity
            activeOpacity={2}

          >

            <Image
              resizeMode={"contain"}
              style={{
                height: 50,
                width: 120,
              }}
              source={images.icon}
            />
          </TouchableOpacity>

          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginRight: 15,
            }}>


            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{


                backgroundColor: "#fff",
                borderRadius: 15,
                height: 25, width: 25, elevation: 6, justifyContent: "center", alignContent: "center", alignItems: "center"
              }}>
              <Image

                style={{ width: 20, height: 120, resizeMode: "center" }}
                source={images.backicon}
              />

            </TouchableOpacity>


          </View>
        </TouchableOpacity>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: height,
    width: width,
    paddingTop: 50,
  },
  scrrenStyle: {
    marginTop: 10,
    width: width,
    height: "100%",
  },

  forgot: {
    color: Constant.appFullColor(),
    position: "absolute",
    right: 10,
    top: 17,
    fontSize: 13,
  },
  register: {
    marginTop: 25,
    color: Constant.appColorAlpha(),
    marginLeft: 15,
    fontSize: 16,
  },
  headerStyle: {
    flex: 0,


    paddingTop: 10,
    justifyContent: "center",
  },
  startView: {
    marginTop: 20,
    borderRadius: 25,
    height: 50,
    marginLeft: 15,
    marginRight: 15,
    alignItems: "center",
    justifyContent: "center",
  },
  startBtn: {
    color: "#fff",
    fontSize: 16,
  },
  headerTextStyle: { letterSpacing: 0, color: Constant.appColor(), fontSize: 20, fontFamily: 'Roboto-Regular', fontWeight: "500" }
  ,
  underlineStyleBase: {
    color: "#000",

    height: 35,
    width: 30,
    borderRadius: 6,
    backgroundColor: "#FFE7E4",


    justifyContent: "center",
    alignItems: "center",
  },
  underlineStyleHighLighted: {},
  pageNoStyle: {
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  pageStyle: {
    width: 60,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  dotStyle: {
    marginTop: 5,
    height: 6,
    width: 6,
    backgroundColor: Constant.appColorAlpha(),
    borderRadius: 3,
  },
  title: {
    marginLeft: 20,
    marginRight: 20,
    lineHeight: 30,
    fontSize: 20,
    color: Constant.appColorAlpha(),
  },
  subtitle: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    lineHeight: 20,
  },
  pageCount: {
    fontSize: 16,
  },
  headerInnerView: {
    height: 50,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  startBtn: {
    color: "#fff",
    fontSize: 16,
  },
  loginEmailBg: {
    height: 40, width: width - 40, backgroundColor: "#F6F6F6",
    borderRadius: 9,
    elevation: 8,
  },
})

export default NewOtp
