import React, { Component } from "react"
import {
    View,
    Platform,
    AsyncStorage,
    Keyboard,
    Text,
    ScrollView,
    Image,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    SafeAreaView,
    StatusBar,
} from "react-native"

import BaseStyle from "../BaseClass/BaseStyles"
import Constant from "../BaseClass/Constant"
import Images from "../BaseClass/Images"
import TextField from "../Login/lib/TextField"
import AnimateLoadingButton from "../ExtraClass/LoadingButton"
const { width, height } = Dimensions.get("window")
import { useNavigation } from "@react-navigation/core"
import Db from "./../DB/Realm"
import {
    getUniqueId,
    getManufacturer,
} from "react-native-device-info"

import LinearGradient from "react-native-linear-gradient"
import images from "../BaseClass/Images"
import { KeyboardAvoidingScrollView } from "react-native-keyboard-avoiding-scroll-view"
import { TextInput } from "react-native-gesture-handler"
import { CommonActions } from "@react-navigation/native"
import auth from "@react-native-firebase/auth"
import Toast from "react-native-simple-toast"
import SplashScreen from "react-native-splash-screen"
import OTPInputView from "@twotalltotems/react-native-otp-input"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import BaseStyles from "../BaseClass/BaseStyles"

class VerifyOtp extends Component {
    static navigationOptions = {
        header: null,
    }

    constructor(props) {
        super(props)
        this.state = {
            showSuccess : false,
            phoneNumber: props.route.params.phoneNumber,
            countryCode: props.route.params.countryCode,
            parameters: props.route.params.parameters,
            confirmResult: "",
            setConfirm: "",
            confirm: "",
            code: "",
            promptSmsCode: false,
            timer: 30,
            code: "",
        }
    }

    componentDidMount() {
        SplashScreen.hide()
        this.interval = setInterval(() => {
            this.setState((prevState) => ({
                timer: prevState.timer - 1,
            }))
        }, 1000)

        // auth().signOut()
        this.initFirebase()

        this.sendOtp(
            this.state.countryCode,
            this.state.phoneNumber
        )
    }

    componentDidUpdate() {
        if (this.state.timer === 0) {
            clearInterval(this.interval)
            this.props.navigation.pop(1)
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval)
        if (this.unsubscribe) this.unsubscribe()
    }

    initFirebase() {
        this.unsubscribe = auth().onAuthStateChanged(
            (user) => {
                if (user && this.state.promptSmsCode) {
                    this.setState({
                        showLoader: true,
                        isCallingService: true,
                    })
                    // this.serviceCall()
                } else {
                    // User has been signed out, reset the state
                    this.setState({
                        confirmResult: null,
                    })
                }
            }
        )
    }

    async sendOtp(countryCode, phoneNumber) {
        await auth()
            .signInWithPhoneNumber(
                countryCode + phoneNumber,
                true
            )
            .then((confirmResult) => {
                this.showAlert()
                this.setState({
                    confirmResult: confirmResult,
                    promptSmsCode: true,
                })
            })
            .catch((error) => {
                console.log(error)
                Toast.show(
                    "Something went wrong" + error,
                    Toast.LONG
                )
            })
    }

    showAlert() {
        this.setState({ showSuccess: true })
        setTimeout(() => {
            this.setState({ showSuccess: false })
        }, 1500)
    }

   
    
    async confirmCode(codeInput) {
        const { confirmResult } = this.state

        try {
            console.log(
                "confirming  code" +
                confirmResult._verificationId
            )

            await confirmResult
                .confirm(codeInput)
                .then((auth) => {
                    console.log(auth)
                    this.setState({ isLoading: true })

                    // this.props.navigation.navigate('Home');
                    this.props.navigation.push("Register", {
                        phoneNumber: phoneNumber,
                         countryCode: countryCode,
                         loginwithPhone: false,
      
                        
                        
                     })
                   
                    this.setState({
                        showLoader: true,
                        isCallingService: false,
                    })
                    //this.registerUser()
                })
                .catch((err) => {
                    console.log(err)
                    //this.loadingButton.showLoading(false)

                    this.setState({
                        isCallingService: false,
                        isLoading: false,
                    })
                    Toast.show(
                        "Something went wrong",
                        Toast.LONG
                    )
                })
        } catch (error) {
            console.log("Invalid code.")
            this.setState({ isLoading: false })
            Toast.show(JSON.stringify(error), Toast.LONG)
        }
    }

    render() {
        const { timer, isLoading, countryCode, phoneNumber } = this.state
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>

                <View style={{ marginTop: 30, flexDirection: 'row' }}>
                    {this.headerView()}
                </View>



                <KeyboardAwareScrollView>

                    <View style={[BaseStyles.cornerRadiusView14, { marginHorizontal: 20, elevation: 5, paddingHorizontal: 20, marginVertical: 30 }]}>



                        <Text style={[styles.headerTextStyle, BaseStyle.boldFont, { fontSize: 14 }]}>We’ve sent a code to your number</Text>
                        <Text style={[styles.headerTextStyle, BaseStyle.boldFont, { fontSize: 14, color: "#000", marginTop: 10 }]}>{countryCode} {phoneNumber}</Text>



                        <View
                            style={{
                                flexDirection: "row",


                            }}>
                            <View
                                style={{
                                    flexDirection: "row",
                                    alignContent: "center",
                                    alignItems: "center"

                                }}>
                                {/* {this.createingOtp()} */}

                                <OTPInputView
                                    style={{
                                        width: "60%",
                                        height: 60,
                                        marginTop: 10,
                                        justifyContent: "flex-start"
                                    }}
                                    pinCount={6}
                                    placeholderTextColor="#000"
                                     code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                                    onCodeChanged={(code) => {
                                        this.setState({ code })
                                    }}
                                    autoFocusOnLoad
                                    codeInputFieldStyle={[
                                        styles.underlineStyleBase,
                                        BaseStyles.boldFont,
                                    ]}
                                    codeInputHighlightStyle={
                                        styles.underlineStyleHighLighted
                                    }
                                    onCodeFilled={(code) => {
                                        this.setState({ code })
                                        console.log(
                                            `Code is ${code}, you are good to go!`
                                        )
                                    }}
                                />

                                <Text
                                    style={[
                                        {
                                            color: "#000",
                                            fontSize: 15,
                                            marginTop: 20,
                                            marginLeft: 15,
                                            textAlign: "center",
                                        },
                                        BaseStyles.boldFont,
                                    ]}>
                                    {" "}
                                    {timer == 0
                                        ? `      Resend`
                                        : `Resend otp\n in 00:${timer}`}
                                </Text>
                            </View>
                        </View>

                        <TouchableOpacity


                            onPress={() => {
                               
                                this.sendAction()


                            }}>
                            <LinearGradient
                                start={{ x: 0, y: 1 }}
                                end={{ x: 1, y: 1 }}

                                colors={[
                                    Constant.gradientColor1(),
                    Constant.gradientColor2(),


                                ]}
                                style={{ marginVertical: 20, borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: width - 80 }}>
                                <Text style={{ fontSize: 14, color: "#000" }}>CONFIRM OTP</Text>
                            </LinearGradient>
                        </TouchableOpacity>

                    </View>


                    <View style={{ height: 20 }} />

                </KeyboardAwareScrollView>
                {Constant.showSuccess(
                    "OTP sent successfully",
                    this.state.showSuccess
                )}


            </SafeAreaView>
        )
    }

    sendAction() {

        if (
          this.state.code != "" &&
          this.state.code.length == 6
        ) {
          console.log("otp success ")
          this.loadingButton.showLoading(true)
          this.setState({ isLoading: true })
          this.confirmCode(this.state.code)
        } else {
          this.setState({ isLoading: false })

          Toast.show("Please enter OTP")



          this.setState({
            showLoader: true,
            isCallingService: false,
          })
        }

        // mock
        // setTimeout(() => {
        //   this.loadingButton.showLoading(false);
        //   this.props.navigation.push('Register')

        // }, 1000);
    }

    headerView() {
        const { drawerOpen } = this.state

        const userData = global.userData

        return (
            <SafeAreaView
                style={[
                    styles.headerStyle,

                ]}>
                <StatusBar
                    translucent
                    barStyle="light-content"
                    //  backgroundColor="rgba(0, 0, 0, 0.251)"
                    backgroundColor={Constant.headerColor()}

                />
                <TouchableOpacity
                    style={[styles.headerInnerView, { marginTop: 20 }]}>
                    <TouchableOpacity
                        activeOpacity={2}

                    >

                        <Image
                            resizeMode={"contain"}
                            style={{
                                height: 50,
                                width: 120,
                            }}
                            source={images.icon}
                        />
                    </TouchableOpacity>

                    <View
                        style={{
                            flexDirection: "row",
                            alignItems: "center",
                            marginRight: 15,
                        }}>


                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.pop(1)
                            }}
                            style={{


                                backgroundColor: "#fff",
                                borderRadius: 15,
                                height: 25, width: 25, elevation: 6, justifyContent: "center", alignContent: "center", alignItems: "center"
                            }}>
                            <Image

                                style={{ width: 20, height: 120, resizeMode: "center" }}
                                source={images.backicon}
                            />

                        </TouchableOpacity>


                    </View>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
    headerView() {


        return (


            <View style={styles.headerStyle}>
                <TouchableOpacity onPress={() => {
                    this.props.navigation.pop(1)
                }} style={styles.goBackButton}>
                    <Image
                        style={{ width: 10, height: 14, marginLeft: -2, position: 'absolute' }}
                        source={images.backicon}
                    />
                </TouchableOpacity>
                <Text style={[styles.nameProfile, { marginLeft: 20 }]}>Phone Number</Text>
            </View>


        )
    }

}

const styles = StyleSheet.create({
    goBackButton: {
        marginLeft: width / 18,
        backgroundColor: '#fff',
        width: width / 12,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 40,
        elevation: 3
    },
    nameProfile: {
        fontWeight: '500',
        fontSize: 18,
        fontFamily: "Roboto-Regular",
        color: "#000"
    },
    headerStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },


    headerTextStyle: { letterSpacing: 0, color: Constant.appColor(), fontSize: 20, fontFamily: 'Roboto-Regular', fontWeight: "500" }
    ,
    underlineStyleBase: {
        color: "#000",

        height: 35,
        width: 30,
        borderRadius: 6,
        backgroundColor: "#FFE7E4",


        justifyContent: "center",
        alignItems: "center",
    },
    underlineStyleHighLighted: {},
    pageNoStyle: {
        width: "100%",
        height: 50,
        alignItems: "center",
        justifyContent: "center",
    },




})

export default VerifyOtp
