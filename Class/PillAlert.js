import React, { useEffect, useState } from "react"
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyles from "./BaseClass/BaseStyles"
import Constant from "./BaseClass/Constant"
import SplashScreen from "react-native-splash-screen"
import images from "./BaseClass/Images"
import urls from "./BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")
import Db from "../Class/DB/Realm"

export default function PillAlert({
  props,
  route,
}) {
  useEffect(() => {
    SplashScreen.hide()
  }, [])

  const [isSent, setIsSent] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const { pillData } = route.params || {}

  const {
    pillType,
    shape,
    color,
    image,
    dose,
    name,
    id,
  } = pillData

  const getShape = (shape) => {
    const shapesArray = [
      { image: images.roundRect, name: "Rect" },
      { image: images.circle, name: "Circle" },
      { image: images.oval, name: "Oval" },
      { image: images.rectangle, name: "Squr" },
      { image: images.pentagon, name: "Pent" },
    ]
    const shapeItem = shapesArray.find(
      (item) => item.name == shape
    )
    return !!shapeItem ? shapeItem.image : "null"
  }

  const updatePillStatus = (isTaken) => {
    let params = {
      id: id,
      isTaken: isTaken,
      isSkipped: !isTaken,
    }
    setIsLoading(true)
    Constant.postMethod(
      urls.updatePillStatus,
      params,
      (result) => {
        setIsLoading(false)
        if (result.success) {
          try {
            Db.write(() => {
              let pillData = Db.objectForPrimaryKey(
                "Pill",
                id
              )
              if (isTaken) {
                pillData.IsTaken = isTaken
              } else {
                pillData.IsSkipped = !isTaken
              }
            })
            setIsSent(true)
          } catch (e) {
            console.log("Error on creation")
            console.log(e)
          }
        }
      }
    )
  }

  return (
    <View style={styles.container}>
      {isLoading
        ? Constant.showLoader("", "#fff")
        : null}
      <Text
        style={[
          {
            width: "100%",
            textAlign: "center",
            fontSize: 25,
            color: "#fff",
          },
          BaseStyles.boldFont,
        ]}>
        {"FirstMeds"}
      </Text>
      <View style={styles.shapeMainView}>
        <View style={styles.shapeItem}>
          <Text
            style={[
              styles.textStyle,
              BaseStyles.mediumFont,
            ]}>
            Type
          </Text>

          <Text
            style={[
              styles.textStyle,
              { marginTop: 10 },
              BaseStyles.mediumFont,
            ]}>
            {pillType}
          </Text>
        </View>

        <View style={styles.shapeItem}>
          <Text
            style={[
              styles.textStyle,
              BaseStyles.mediumFont,
            ]}>
            Shape
          </Text>
          <Image
            style={{
              height: 30,
              width: 30,
              marginTop: 10,
              tintColor: "#fff",
            }}
            resizeMode={"center"}
            source={getShape(shape)}
          />
        </View>

        <View style={styles.shapeItem}>
          <Text
            style={[
              styles.textStyle,
              BaseStyles.mediumFont,
            ]}>
            Color
          </Text>

          <View
            style={{
              height: 28,
              width: 28,
              marginTop: 10,
              borderRadius: 16,
              backgroundColor: color,
            }}
          />
        </View>
      </View>

      <View
        style={{
          marginTop: 10,
          alignItems: "center",
          justifyContent: "center",
        }}>
        <Image
          style={{ height: "40%", width: "80%" }}
          resizeMode={"cover"}
          source={{ uri: image }}
        />
      </View>

      <Text
        style={[
          {
            width: "100%",
            textAlign: "center",
            fontSize: 20,
            color: "#fff",
            marginTop: 10,
          },
          BaseStyles.regularFont,
        ]}>
        {name}
      </Text>

      <Text
        style={[
          {
            width: "100%",
            textAlign: "center",
            fontSize: 20,
            marginTop: 10,
            color: "#fff",
          },
          BaseStyles.regularFont,
        ]}>
        {`Dosage : ${dose} Pill`}
      </Text>

      <View
        style={{
          height: 60,
          marginTop: 20,
          width: width,
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        }}>
        <TouchableOpacity
          onPress={() => {
            updatePillStatus(true)
          }}
          style={{
            width: width / 2,
            height: 60,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "#fff",
          }}>
          <Text
            style={[
              {
                width: "100%",
                textAlign: "center",
                fontSize: 20,
                color: Constant.appColorAlpha(),
              },
              BaseStyles.regularFont,
            ]}>
            TAKEN
          </Text>
        </TouchableOpacity>
        {isSent && (
          <TouchableOpacity
            onPress={() => {
              props.navigation.goBack()
            }}
            style={{
              width: 60,
              height: 60,
              alignItems: "center",
              justifyContent: "center",
              backgroundColor: "#fff",
              borderRadius: 30,
            }}>
            <Text
              style={[
                {
                  width: "100%",
                  textAlign: "center",
                  fontSize: 20,
                  color: Constant.appColorAlpha(),
                },
                BaseStyles.regularFont,
              ]}>
              OK
            </Text>
          </TouchableOpacity>
        )}

        <TouchableOpacity
          onPress={() => {
            updatePillStatus(false)
          }}
          style={{
            width: width / 2,
            height: 60,
            alignItems: "center",
            justifyContent: "center",
          }}>
          <Text
            style={[
              {
                width: "100%",
                textAlign: "center",
                fontSize: 20,
                color: "#fff",
              },
              BaseStyles.regularFont,
            ]}>
            SKIP
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Constant.appColorAlpha(),
  },
  shapeItem: {
    flex: 0.333,
    alignItems: "center",
    justifyContent: "center",
  },
  shapeMainView: {
    width: "100%",
    flexDirection: "row",
    marginTop: 20,
  },
  textStyle: {
    color: "#fff",
    fontSize: 20,
    textAlign: "center",
  },
})
