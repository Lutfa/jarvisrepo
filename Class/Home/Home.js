import React, { Component } from "react"
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  FlatList,
  Image,
  Platform,
  Animated,
  StatusBar,
  StyleSheet,
  Modal,
  Alert,
  AsyncStorage,
  ScrollView,
  TextInput,
} from "react-native"
import { useState } from 'react';

import Sidemenu from "./Sidemenu"
import Drawer from "react-native-drawer"
import Constant from "../BaseClass/Constant"
import BaseStyles from "../BaseClass/BaseStyles"
import SplashScreen from "react-native-splash-screen"
import images from "../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import language from "../BaseClass/language"
import urls from "../BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")
import Db from "../DB/Realm"
import ImageLoad from "../BaseClass/ImageLoader"
import moment from "moment"
import CartDb from "../DB/CartDb"
import Toast from "react-native-simple-toast"
import HTMLView from "react-native-htmlview"
import BannersView from "./Banners"
import { CommonActions, NavigationContainer } from "@react-navigation/native"
import SidemenuAnimation from "./SideMenuAnimation"
import axios from "axios"
import LinearGradient from "react-native-linear-gradient"
import StarRating from "react-native-star-rating"
import AgoraUIKit from 'agora-rn-uikit';
import Icon from 'react-native-vector-icons/FontAwesome5';

//Import some screens

import AllMedicine from "../AddMedicine/AllMedicine"
import Chat from "../Chat/Chat"
import HealthRecords from "../HealthRecords/HealthRecords"
import VideoCall from "../Appointment/VideoCall"
import LoginWithEmail from "../Login/LoginWithEmail";

var Realm = require("realm")

class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      drawerOpen: false,
      isCategories: true,
      isBestSelling: true,
      bestOffersArray: [],
      bestSellingArray: [],
      newProductsArray: [],
      bannersArray: [],
      categoriesArray: [],
      healthIssueArray: [],
      articlesArray: [],
      bannerPosition: 0,
      articlePosition: 0,
      allDataArray: [],
      pharmacistArray: [],
      optionsArray: [
        "1"
      ]
    }

    language.setLanguage("en")
  }

  async componentDidMount() {
    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
          this.forceUpdate()
        }
      )


    SplashScreen.hide()
    this.checkLogin()
    this.getAllPharmacist()
  }


  getAllPharmacist() {
    console.log("global " + JSON.stringify(global.userData))

    let params = {
      pharmacyId: 1,
      patientId: global.CustomerId,
      LastUpdatedTimeTicks: 0,
    }
    Constant.postMethod(
      urls.getPharmacist,
      params, (result) => {
        console.log("pharmacist res " + JSON.stringify(result))

        if (result.success) {
          if (result.result.response != null) {
            this.setState({
              pharmacistArray: result.result.response.pharmacists
            })

          }
        }
      }
    )
  }


  toggle = () => {
    const { drawerOpen } = this.state
    !drawerOpen
      ? this.drawer.openMenu()
      : this.drawer.closeMenu()
  }

   checkLogin() {
    
    
    
    // const value =  AsyncStorage.getItem(
    //   "userLoggedIn"
    // )
    let Data = Db.objects("User")
      console.log("db Data", Data[0])
  


      if (Data.length > 0) {
        global.userData = Data[0]
        global.CustomerId = Data[0].id
        console.log("token", Data[0].token)
        global.userToken = Data[0].token
       

      
        
      
    } else {
     
    }
  }
  onMenuSelected(name) {
    this.setState({ drawerOpen: false })
    this.drawer.closeMenu()
    if (name == "Logout") {
      // this.deleteData()
      const deleted = this.removeItemValue(
        "userLoggedIn"
      )

      // this.props.navigation.dispatch(
      //   CommonActions.reset({
      //     index: 0,
      //     routes: [
      //       {
      //         name: "Login",
      //       },
      //     ],
      //   })
      // )
    } else if (name != "Home") {
      this.props.navigation.push(name)
    }
  }
  async removeItemValue(key) {
    try {
      await AsyncStorage.removeItem(key)
      return true
    } catch (exception) {
      return false
    }
  }
  render() {
    const {
      userData,
      articlesArray,
      bestSellingArray,
      drawerOpen,
      isCategories,
      bestOffersArray,
      isBestSelling,
      newProductsArray,
      bannersArray,
      categoriesArray,
      pharmacistArray,
      isLoading,
    } = this.state

    return (

      <SidemenuAnimation
        ref={(menu) => (this.drawer = menu)}
        menuSize={150}
        onMenuOpen={() => {
          this.setState({ drawerOpen: true })
        }}
        onCloseOpen={() => {
          this.setState({ drawerOpen: false })
        }}
        menuView={
          <Sidemenu
            onMenuSelected={(name) => {
              this.onMenuSelected(name)
            }}
          />
        }>

        <ScrollView>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => {
              drawerOpen
                ? this.drawer.closeMenu()
                : null
            }}
            style={{
              flex: 1,
              backgroundColor: "#e5e5e5",

            }}>
            {isLoading ? Constant.showLoader() : null}
            <ImageBackground style={{ height: "100%", width: "100%" }} source={images.bg}>
              <View >
                {this.headerView()}
                <View

                  pointerEvents={
                    drawerOpen ? "none" : "auto"
                  }>
                  <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    ref="mainScroll">
                    <View>
                      <View style={styles.searchMainView}>
                        <TouchableOpacity
                          activeOpacity={2}
                          // onPress={() => {
                          //   this.props.navigation.navigate(
                          //     "Search"
                          //   )
                          // }}
                          style={styles.searchInner}>
                          <Image
                            source={images.location_new}
                            style={{
                              marginLeft: 10,
                              width: 40,
                              height: 40, marginTop: 5
                            }}
                          />
                          <Text
                            style={[
                              BaseStyles.boldFont,
                              styles.searchIcon,
                            ]}>
                            Bloom Store 36 Meryl Street...
                          </Text>
                          <Text
                            style={[
                              BaseStyles.boldFont,
                              styles.searchIcon, { color: "red", marginRight: 8 }
                            ]}>
                            VIEW PHARMACY LOCATION
                          </Text>
                        </TouchableOpacity>
                      </View>
                      <View style={[BaseStyles.cornerRadiusView, { marginTop: 10 }]}>
                        <ImageBackground style={{ borderTopLeftRadius: 28, borderTopRightRadius: 28, position: "absolute", top: 0, width: width, height: 100 }} source={images.pharma_top_curve}></ImageBackground>

                        <Image style={{ position: "absolute", bottom: 0, borderBottomLeftRadius: 28, borderBottomRightRadius: 28, width: "100%", height: 80 }} source={images.pharma_bottom_curve}></Image>


                        <View style={{ flexDirection: "row", marginTop: 10 }}>
                          <View
                            style={{ marginLeft: 15 }}>

                            <Text
                              style={[{ fontSize: 14, color: "#000" }, BaseStyles.boldFont]}
                            >
                              Chat OR Book an Appointment
                            </Text>
                            <Text
                              style={[{ fontSize: 12, color: "#000" }, BaseStyles.regularFont]}
                            >
                              Connect with our pharmacists
                            </Text>
                          </View>
                          <TouchableOpacity







                            onPress={() => {


                              this.props.navigation.navigate("OurPharmacist")



                            }}>
                            <LinearGradient
                              start={{ x: 0, y: 1 }}
                              end={{ x: 1, y: 1 }}

                              colors={[
                                Constant.gradientColor1(),
                                Constant.gradientColor2(),

                              ]}
                              style={{ marginLeft: 20, marginTop: 5, marginBottom: 10, borderRadius: 111, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: width * 0.4 }}>
                              <Text style={{ fontSize: 14, color: "#000" }}>VIEW OTHERS</Text>
                            </LinearGradient>
                          </TouchableOpacity>
                        </View>

                        <FlatList
                          style={{ marginTop: 0, paddingRight: 10, marginBottom: 20 }}
                          data={pharmacistArray}
                          numColumns={2}
                          renderItem={({ item, index }) => (
                            <View style={{ flex: 1 }}>
                              <TouchableOpacity


                                onPress={() => {
                                  this.props.navigation.navigate(
                                    "Pharmacist",
                                    { data: item }
                                  )
                                }}

                                style={{ height: 150, marginLeft: 12, marginTop: 12, borderRadius: 20, }}>

                                <ImageLoad loadingStyle={{
                                  size: "large",
                                  color: "blue",
                                }} style={{ flex: 1, borderRadius: 20, height: "100%", resizeMode: "contain" }} source={{
                                  uri: item.pictureUrl,
                                }}
                                  placeholderSource={
                                    images.manicon
                                  }>

                                  <LinearGradient
                                    start={{ x: 0, y: 1 }}
                                    end={{ x: 0, y: 0 }}
                                    style={[styles.linearGradient]}
                                    colors={['rgba(0,0,0,0.60)', 'rgba(0,0,0,0.00)']} style={{ flex: 1, borderRadius: 10, }}>
                                    <View style={{ flex: 1, height: 40, alignContent: "flex-end", marginLeft: 15, justifyContent: "flex-end", }}>
                                      <View style={{ flexDirection: "row", flex: 0 }}>
                                        <View>
                                          <Text style={{ fontSize: 14, color: "#fff", alignContent: "center" }}>{item?.name}</Text>
                                          <Text style={{ marginTop: 5, fontSize: 12, color: "#fff", alignContent: "center", marginBottom: 10 }}>{item?.age} {item?.pronuouns}</Text>
                                        </View>
                                        <View style={{ flexDirection: "row", position: "absolute", bottom: 10, right: 10, alignContent: "flex-end", justifyContent: "center", alignItems: "center" }}>
                                          <StarRating
                                            disabled={false}
                                            maxStars={1}
                                            rating={1}
                                            fullStarColor="#FFD500"
                                            starSize={15}></StarRating>
                                          <Text style={{ marginLeft: 4, fontSize: 14, color: "#fff", alignContent: "center" }}>{item?.rating}</Text>

                                        </View>
                                      </View>
                                    </View>
                                  </LinearGradient>

                                </ImageLoad>






                              </TouchableOpacity>
                            </View>
                          )}
                        />
                      </View>

                      {/* Pill reminder/health record */}

                      <View style={{ width: "100%", flex: 0, flexDirection: "row", marginTop: 10, justifyContent: "space-evenly" }}>



                        <TouchableOpacity onPress={() => {
                          this.props.navigation.navigate(
                            "AllMedicine"
                          )
                        }} style={[styles.patientContainer, { marginLeft: 5 }]}
                        >
                          <ImageBackground style={{ marginTop: 10, height: 50, width: 50, justifyContent: "center", alignContent: "center", alignItems: "center" }} source={images.ellips}>
                            <Image style={{ height: 30, width: 30 }} source={images.alarm}></Image>

                          </ImageBackground>

                          <Text style={[{ letterSpacing: 0, color: "#000", marginTop: 5, fontSize: 12 }, BaseStyles.regularFont]}>Pill Reminder</Text>
                          <Text style={[styles.bottomTextStyle, BaseStyles.boldFont]}>SET ALARM</Text>
                        </TouchableOpacity>


                        <TouchableOpacity
                          onPress={() => {
                            this.props.navigation.navigate(
                              "HealthRecords"
                            )
                          }} style={[styles.patientContainer, { marginLeft: 15 }]}
                        >
                          <ImageBackground style={{ marginTop: 10, height: 50, width: 50, justifyContent: "center", alignContent: "center", alignItems: "center" }} source={images.ellips}>
                            <Image style={{ height: 30, width: 30 }} source={images.health_record}></Image>

                          </ImageBackground>

                          <Text style={[{ letterSpacing: 0, color: "#000", marginTop: 5, fontSize: 12 }, BaseStyles.regularFont]}>Health Rcord</Text>
                          <Text style={[styles.bottomTextStyle, BaseStyles.boldFont]}>UPLOAD</Text>




                        </TouchableOpacity>
                      </View>

                      {/* Promotions */}
                      <View style={BaseStyles.topRadiusLayout}>
                        <Text style={BaseStyles.headerFont}>Our Promotions</Text>

                        <View style={{ flexDirection: "row", width: "100%" }}

                        >

                          <Image style={{ marginTop: 15, height: 120, width: 90, borderRadius: 15, }} source={images.manicon}></Image>
                          <View>
                            <Text numberOfLines={4}
                              ellipsizeMode="tail" style={[{ marginTop: 20, marginHorizontal: 20, }, BaseStyles.descFontblack,]}>Our Promotions</Text>
                            <View style={{ flexDirection: "row", marginTop: 15 }}

                            >
                              <View style={{ flexDirection: "row", marginLeft: 20 }}

                              ><Image style={BaseStyles.smallIconStyle} source={images.chatIcon}></Image>
                                <Text
                                  style={[{ marginHorizontal: 10, }, BaseStyles.descFontblack,]}>51</Text>

                              </View>
                              <View style={{ flexDirection: "row" }}

                              ><Image style={[BaseStyles.smallIconStyle, { marginLeft: 15 }]} source={images.chatIcon}></Image>
                                <Text
                                  style={[{ marginHorizontal: 10, }, BaseStyles.descFontblack,]}>31</Text>

                              </View>
                            </View>

                          </View>
                        </View>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Promotions')}>
                          <LinearGradient
                            start={{ x: 0, y: 1 }}
                            end={{ x: 1, y: 1 }}

                            colors={[
                              Constant.gradientColor1(),
                              Constant.gradientColor2(),
          

                            ]}
                            style={{ marginTop: 5, marginBottom: 10, borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: "100%" }}>
                            <Text style={{ fontSize: 14, color: "#000" }}>VIEW ALL</Text>
                          </LinearGradient>
                        </TouchableOpacity>
                      </View>


                      <View style={{ height: 20 }} />
                    </View>
                  </ScrollView>
                </View>

              </View>
            </ImageBackground>

          </TouchableOpacity>
        </ScrollView>

        {this.callWidget()}
      </SidemenuAnimation>
    )
  }

  callWidget() {
    return (
      <View style={{ width: width, height: height / 8, backgroundColor: "#121212" }}>
        {/* VIEW OF PHARMACIST CALL */}
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          {/* ROW VIEW */}
          <Image style={{ marginTop: height / 36, marginLeft: width / 15, paddingRight: width / 15, height: 55, width: 55, borderRadius: 20 }} source={images.manicon}></Image>
          <View style={{ flexDirection: 'column', justifyContent: 'center', marginTop: height / 50, marginLeft: width / 28 }}>
            {/* COLUMN VIEW */}
            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 16 }}>David Ninh</Text>
            <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 11 }}>Join your appointment call...</Text>
          </View>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('VideoCall')} style={styles.recordButton}>
            <Icon name="video" size={14} color="#FFF" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  headerView() {
    const { drawerOpen } = this.state

    const userData = global.userData

    return (
      <SafeAreaView
        style={[
          styles.headerStyle,
          {
            borderTopLeftRadius: drawerOpen ? 8 : 0,
          },
        ]}>
        <StatusBar
          translucent
          barStyle="light-content"
          //  backgroundColor="rgba(0, 0, 0, 0.251)"
          backgroundColor={Constant.headerColor()}

        />
        <TouchableOpacity
          style={styles.headerInnerView}>
          <TouchableOpacity
            activeOpacity={2}
            style={styles.menuStyle}
            onPress={this.toggle}>
            <Image
              style={{ width: 50, height: 60 }}
              source={images.menu}
            />
            <Image
              resizeMode={"contain"}
              style={{
                height: 50,
                width: 120,
              }}
              source={images.icon}
            />
          </TouchableOpacity>

          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginRight: 15,
            }}>


            <TouchableOpacity
              // onPress={() => {
              //   this.props.navigation.navigate(
              //     "Wishlist"
              //   )
              // }}
              style={{

                marginRight: 15,
                backgroundColor: "#fff",
                borderRadius: 15,
                height: 30, width: 30, elevation: 6, justifyContent: "center", alignContent: "center", alignItems: "center"
              }}>
              <Image

                style={{ width: 15, height: 15 }}
                source={images.notif}
              />

            </TouchableOpacity>

            <TouchableOpacity
              style={{}}
              onPress={() => {
                console.log("cus id" + global.CustomerId)
                const value =global.CustomerId
                if (value!=null && value != "") {
                  this.props.navigation.navigate("Profile"
                  
                 
                  )
                }  else{
                  this.props.navigation.navigate("LoginOptions")
                  //this.props.navigation.navigate("GetHelp")
                }

              }}>
              <Image style={{
                width: 60,
                height: 60,
                borderRadius: 17.5,
                resizeMode: "cover",
                alignItems: "center", marginTop: 10
              }} source={images.default_home}></Image>
              {/* <ImageLoad
                borderRadius={17.5}
                placeholderStyle={{
                  width: 35,
                  height: 35,
                  borderRadius: 17.5,
                }}
                style={{
                  width: 35,
                  height: 35,
                  borderRadius: 17.5,
                }}
                loadingStyle={{
                  size: "large",
                  color: Constant.appColorAlpha(),
                }}
                source={{
                  // uri: userData.ProfilePicture,
                }}
                placeholderSource={images.work}
              /> */}
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </SafeAreaView>
    )
  }

  _onMomentumBanner = ({ nativeEvent }: any) => {
    const index = Math.round(
      nativeEvent.contentOffset.x / width
    )

    if (index !== this.state.bannerPosition) {
      this.setState({ bannerPosition: index })
    }
  }

  _onMomentumArticle = ({ nativeEvent }: any) => {
    const index = Math.round(
      nativeEvent.contentOffset.x / width
    )

    if (index !== this.state.articlePosition) {
      this.setState({ articlePosition: index })
    }
  }
}


const styles = StyleSheet.create({
  recordButton: {
    marginLeft: width / 4.5,
    marginTop: 10,
    backgroundColor: 'red',
    width: width / 12,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 40,
    elevation: 3
  },
  headerStyle: {
    flex: 0,
    paddingTop: 10,
    justifyContent: "center",
  },
  headerInnerView: {
    height: 50,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
  },


  menuStyle: {

    height: 50,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  mainView: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: -110,
    zIndex: 101,
  },
  topBox: {
    height: 100,
    paddingRight: 5,
    paddingLeft: 5,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  topInnerBox: {
    flex: 0.2,

    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 6,
    elevation: 2,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
  },

  categoriesBox: {
    width: width / 4 - 16,
    marginRight: 10,

    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 6,
    elevation: 2,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
  },
  subMenu: { fontSize: 16, marginRight: 15 },
  viewAll: {
    fontSize: 13,
    color: Constant.homeNonSelected(),
  },
  topInnerBox2: {
    width: 40,
    height: 40,
    borderRadius: 6,
    elevation: 2,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
    backgroundColor: Constant.appLightColor(),
    alignItems: "center",
    justifyContent: "center",
  },
  topInnerText: {
    color: Constant.textColor(),
    paddingLeft: 5,
    paddingRight: 5,
    marginTop: 10,
    fontSize: 11,
    textAlign: "center",
    lineHeight: 13,
    height: 26,
  },

  boxTitle: {
    height: 40,
    width: "100%",
    backgroundColor: "#fff",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingRight: 5,
    paddingLeft: 5,
  },
  bestOffersMain: {
    height: 120,
    width: "100%",
    borderRadius: 6,
    padding: 4,
    paddingLeft: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  bestOffersInner: {
    width: "100%",
    borderRadius: 6,
    backgroundColor: "#fff",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
    elevation: 2,
    marginRight: 2,
    paddingTop: 6,
    alignItems: "center",
    justifyContent: "center",
  },
  bestOffersText: {
    width: "100%",
    backgroundColor: Constant.appColorAlpha(),
    color: Constant.selectedTextColor(),
    textAlign: "center",
    padding: 2,
    fontSize: 12,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
  },
  bannerMain: {
    marginTop: 20,
    backgroundColor: Constant.appColorAlpha(),
    borderRadius: 6,
  },
  banneritem: {
    width: width - 30,
    height: 200,
    borderRadius: 6,
    backgroundColor: Constant.appColorAlpha(),
    overflow: "hidden",
  },
  bannerTitle: {
    color: Constant.selectedTextColor(),
    fontSize: 16,
  },
  bannerSub: {
    color: Constant.selectedTextColor(),
    fontSize: 13,
    marginTop: 10,
  },
  bannerBuy: {
    marginTop: 15,
    width: 100,
    borderRadius: 20,
    elevation: 2,
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    backgroundColor: Constant.appFullColor(),
  },
  bannerDotView: {
    flexDirection: "row",
    paddingBottom: 0,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    bottom: 5,
    left: 0,
    right: 0,
  },
  bannerDot: {
    height: 9,
    width: 9,
    elevation: 2,
    backgroundColor: Constant.appColorAlpha(),
    margin: 3,
    borderRadius: 4.5,
  },
  searchMainView: {
    width: "100%",
    marginTop: 10,
    paddingRight: 5,
    paddingLeft: 5,
    height: 50,
    marginBottom: 20,
    alignItems: "center",
    justifyContent: "center",
    elevation: 8
  },

  searchInner: {
    marginRight: 5,
    marginLeft: 5,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
    elevation: 0,
    height: 50,
    backgroundColor: "#fff",
    borderRadius: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  searchIcon: {
    marginLeft: 0,
    color: Constant.textColor(),
    textAlign: "left",
    left: 5,
    fontSize: 12,
    flex: 1,
  },

  articleitem: {
    width: width - 36,
    height: 120,
    borderRadius: 6,
    backgroundColor: "#fff",
    flexDirection: "row",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
    elevation: 2,
  },
  articleTitle: { fontSize: 16 },
  articleSubmenu: {
    color: Constant.textAlpha(),
    fontSize: 13,
    marginTop: 10,
  },
  bestSellingsMain: {
    height: 170,
    width: 140,
    borderRadius: 6,
    padding: 4,
    paddingLeft: 2,
    justifyContent: "center",
    alignItems: "center",
  },

  bottomFlatlist: {
    flex: 1,
    width: 120,
    borderRadius: 6,
    backgroundColor: "#fff",
    elevation: 2,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowColor: "#d3d3d3",
    shadowRadius: 5,
    shadowOpacity: 0.6,
    marginRight: 2,
    paddingTop: 6,
  },
  linearGradient: {

    width: width * 0.44,
    height: '100%',
    borderRadius: 20
  }, patientContainer: {
    backgroundColor: "#fff",
    borderRadius: 8,
    elevation: 2,
    alignContent: "center",
    alignItems: "center",
    width: (width - 50) / 2


  }, bottomTextStyle: { marginBottom: 10, marginHorizontal: 8, letterSpacing: 0, color: Constant.appColor(), fontSize: 14, }
  , circle: {


    marginRight: 15,
    backgroundColor: "#fff",
    borderRadius: 15,
    height: 30, width: 30, elevation: 6, justifyContent: "center", alignContent: "center", alignItems: "center"

  },

})
export default Home;