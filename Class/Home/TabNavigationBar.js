import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
//import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
//createBottomTabNavigator

import AllMedicine from "../AddMedicine/AllMedicine"
import Chat from "../Chat/Chat"
import MyAppointments from '../Appointment/MyAppointments'; 
import Home from './Home';

import { View } from 'react-native-animatable';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Text, { Image } from 'react-native'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
const Tab = createMaterialBottomTabNavigator();








// const SettingsStack = createStackNavigator();
// const HomeScreenStack = () => (
//     <HomeScreenStack.Navigator>
//         <HomeScreenStack.Screen name="Pi" component={SettingsScreen} />
//         <HomeScreenStack.Screen name="EditProfileScreen" component={EditProfileScreen} options={{ title: 'Edit Profile' }} />
//     </HomeScreenStack.Navigator>
// )

// const Tab = createBottomTabNavigator();
// const TabNavigator = () => (
//     <NavigationContainer>
//         <Tab.Navigator>
//             <Tab.Screen name="Home" component={HomeScreen} />
//             <Tab.Screen name="Search" component={SearchScreen} />
//             <Tab.Screen name="Messages" component={MessagesScreen} />
//             <Tab.Screen name="Settings" component={SettingsNavigator} />
//         </Tab.Navigator>
//     </NavigationContainer>
// )






export default class TabNavigationBar extends Component {
  constructor(props) {
    super(props)

  }
  render() {
    return (
      // <NavigationContainer independent>
      <AppTabsNavigation />

      // </NavigationContainer>
      // <View>
      //   {this.AppTabsNavigation()}
      // </View>

    )
  }


}
function AppTabsNavigation() {
  return (





    <Tab.Navigator
      initialRouteName="Home"
      activeColor="#000"
      inactiveColor="#909090"
      barStyle={{ backgroundColor: "#fff", color:'red' }}
    >


      <Tab.Screen name="Home" component={Home}
        options={{
          headerShown: true,
          tabBarIcon: ({ color }) => (
            <View style={{flexDirection:'row', justifyContent:'flex-end'}}>
              <Image source={require('../../assets/logo1.png')} style={{width:24, height:24, marginTop:10}}/>
              <Image source={require('../../assets/logo2.png')} style={{width:10, height:23}}/>
            </View>
            // <MaterialCommunityIcons name="home" color={color} size={26} />
          ),

        }}

      />
      <Tab.Screen name="AllMedicine" component={AllMedicine}
        options={{
          tabBarLabel: 'Pill Reminder',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="alarm" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen name="MyAppointments" component={MyAppointments}
        options={{
          tabBarLabel: 'Appointments',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="card-account-phone-outline" color={color} size={24} />
          ),
        }}
      />
      <Tab.Screen name="Chat" component={Chat}
        options={{
          tabBarLabel: 'Chat',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="message-outline" color={color} size={26} />
          ),
        }}
      />

    </Tab.Navigator>
  );
}
