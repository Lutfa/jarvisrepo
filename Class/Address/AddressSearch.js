import React from "react"
import {
  View,
  Text,
  StyleSheet,
} from "react-native"
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete"
import MapView, {
  PROVIDER_GOOGLE,
} from "react-native-maps"
const GooglePlacesInput = () => {
  return (
    <View
      style={{
        flex: 1,
      }}>
      <GooglePlacesAutocomplete
        placeholder="Enter Location"
        minLength={2}
        autoFocus={false}
        returnKeyType={"default"}
        fetchDetails={true}
        styles={{
          textInputContainer: {
            backgroundColor: "grey",
          },
          textInput: {
            height: 38,
            color: "#5d5d5d",
            fontSize: 16,
          },
          predefinedPlacesDescription: {
            color: "#1faadb",
          },
        }}
        query={{
          key: "AIzaSyA3NbFeXcOG9LJOKou9tRLovjPmSMxqidM",
          language: "en",
        }}
      />
    </View>
  )
}

export default GooglePlacesInput
