import React, { Component } from "react"
import {
  SafeAreaView,
  View,
  Text,
  ScrollView,
  Modal,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import Db from "./../DB/Realm"
import messaging from "@react-native-firebase/messaging"
import Icon from 'react-native-vector-icons/FontAwesome5';
import { StatusBar } from "react-native";
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import Images from "./../BaseClass/Images"
import AnimateLoadingButton from "./../ExtraClass/LoadingButton"
import { KeyboardAvoidingScrollView } from "react-native-keyboard-avoiding-scroll-view"
import CountryPicker from "react-native-country-picker-modal"
import urls from "./../BaseClass/ServiceUrls"
import Toast from "react-native-simple-toast"
import DeviceInfo from "react-native-device-info"
import {
  getUniqueId,
  getManufacturer,
} from "react-native-device-info"
import LinearGradient from "react-native-linear-gradient"
import language from "./../BaseClass/language"
import {
  TextField,
  OutlinedTextField,
} from "react-native-material-textfield"
const { width, height } = Dimensions.get("window")
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import { ImageBackground } from "react-native"
import images from "./../BaseClass/Images"
import BackButtonHeader from "../Profile/components/backButtonHeader"
class Register extends Component {
  constructor(props) {
    super(props)
    this.state = {
      countryName: "IN",
      showEnterPin: false,
      isNumberCorrect: "",
      showConfirmPin: false,
      confirmPin: "",
      enterPin: "",
      isConfirm: false,
      isEmailCorrect: "",
      email: props.route.params.email,
      pinColor: "",
      showMenu: false,
      name: "",
      nameError: "",
      phoneNumber: props.route.params.phoneNumber,
      enterPinError: "",
      number: props.route.params.phoneNumber,
      countryCode: props.route.params.countryCode,
      confirmPinError: "",
      password: "",
      gender: "Select",
      genderArray: ["Him/His", "Her/She"],
      menuType: "",
      menuArray: [],
      loginwithPhone: props.route.params.loginwithPhone,
      isLoading: false

    }
  }

  render() {
    const {
      name,
      nameError,
      enterPinError,
      confirmPinError,
      showEnterPin,
      isNumberCorrect,
      email,
      enterPin,
      menuType,
      menuArray,
      isEmailCorrect,
      pinColor,
      phoneNumber,
      showConfirmPin,
      genderArray,
      confirmPin,
      isConfirm,
      loginwithPhone,
      password,
      gender,
      countryCode, isLoading

    } = this.state
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>

        <View style={{ marginTop: 30, flexDirection: 'row' }}>
          {this.headerView()}
        </View>
       

        {/* top track view */}
        <View style={[BaseStyle.cornerRadiusView14, {alignContent:"center",alignItems:"center", marginHorizontal: 20, elevation: 5, paddingHorizontal: 20, marginTop: 30 }]}>
        <View style={{ flexDirection: "row", marginHorizontal: 20 ,alignContent:"center",alignItems:"center",}}>
          <Image style={{ height: 15, width: 15 }} source={phoneNumber != null ? require('../../assets/bluecheck.png') : require('../../assets/bluecircle.png')}></Image>
          <View style={{ height: 0.5, width: "35%", backgroundColor: "#5D5FEF" }}></View>
          <Image style={{  height: 15, width: 15 }} source={loginwithPhone&&phoneNumber != "" ? require('../../assets/bluecheck.png') : require('../../assets/bluecircle.png')}></Image>
          <View style={{ height: 0.5, width: "35%", backgroundColor: "#5D5FEF" }}></View>
          <Image style={{ height: 15, width: 15 }} source={require('../../assets/bluecircle.png') }></Image>

        </View>
        <View style={{ flexDirection: "row", marginHorizontal: 20 ,marginTop:10,marginBottom:20}}>
         <View flexDirection={"column"}>
         <Text style={styles.tracktext}> Phone </Text>
         <Text style={styles.tracktext}>Verified</Text></View> 
       

      
        
         <View flexDirection={"column"}style={{marginLeft:"27%"}}>
         <Text style={styles.tracktext}> Create </Text>
         <Text style={styles.tracktext}>Account</Text></View> 
         
         <View flexDirection={"column"}style={{marginLeft:"27%"}}>
         <Text style={styles.tracktext}> Login </Text>
         </View>
        </View>
        </View>
        

        <KeyboardAwareScrollView>
          {/* password */}
          {!loginwithPhone && (<View style={[BaseStyle.cornerRadiusView14, { marginHorizontal: 20, elevation: 5, paddingHorizontal: 20, marginTop: 20 }]}>
            <Text style={styles.nameProfile}>Create a Password</Text>
            <View>


              <TextField
                label={language.createPassword}
                value={password}
                lineWidth={1}
                fontSize={15}
                labelFontSize={13}
                secureTextEntry={true}
                labelTextStyle={[
                  { paddingTop: 5 },
                  BaseStyle.regularFont,
                ]}
                baseColor={Constant.textGreyColor()}
                tintColor={Constant.textGreyColor()}
                textColor={"#000"}
                secureTextEntry={
                  !this.state.showPassword
                }
                onChangeText={(text) =>
                  this.setState({
                    password: text,
                    //isPasswordCorrect: "",
                  })
                }
              // error={isPasswordCorrect}
              />
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    showPassword:
                      !this.state.showPassword,
                  })
                }}
                style={{
                  position: "absolute",
                  top: 30,
                  height: 28,
                  width: 28,
                  right: 15,
                }}>
                <Image
                  tintColor={Constant.textGreyColor()}
                  style={{
                    height: 28,
                    width: 28,
                  }}
                  source={
                    this.state.showPassword
                      ? images.showPassword
                      : images.hidePassword
                  }
                />
              </TouchableOpacity>
            </View>
            <View style={{ flexDirection: "row", marginBottom: 20, justifyContent: "space-evenly" }}>

              <View style={[styles.passwordgreenview, { backgroundColor: password.length > 7 ? "#CAFFC7" : "#EEEEEE" }]}>

                <Text style={[styles.passSeqText, { color: password.length > 7 ? "#28A420" : "#7C7C7C" }]}> 8 Characters </Text>
              </View>
              <View style={[styles.passwordgreenview, { marginLeft: 5, backgroundColor: this.validatSplChar() ? "#CAFFC7" : "#EEEEEE" }]}>

                <Text style={[styles.passSeqText, { color: this.validatSplChar() ? "#28A420" : "#7C7C7C" }]}> 1 Special </Text>
              </View>
              <View style={[styles.passwordgreenview, { marginLeft: 5, backgroundColor: this.validatNumeric() ? "#CAFFC7" : "#EEEEEE" }]}>

                <Text style={[styles.passSeqText, { color: this.validatNumeric() ? "#28A420" : "#7C7C7C" }]}> 1 Numeric </Text>
              </View>
              <View style={[styles.passwordgreenview, { marginLeft: 5, backgroundColor: this.validatUppercase() ? "#CAFFC7" : "#EEEEEE" }]}>

                <Text style={[styles.passSeqText, { color: this.validatUppercase() ? "#28A420" : "#7C7C7C" }]}> 1 Uppercase </Text>
              </View>

            </View>

          </View>)}
          {/* details view */}
          <View style={[BaseStyle.cornerRadiusView14, { marginHorizontal: 20, marginBottom: 20, paddingBottom: 20, elevation: 5, paddingHorizontal: 20, marginTop: 20 }]}>

            <View
              style={{


              }}>

              <Text style={styles.nameProfile}>Share account details</Text>
              <Text style={[BaseStyle.descFontblack, { color: "#B0B0B0", marginTop: 5 }]}>You can also share these details later</Text>





              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",

                }}

                onPress={() => {

                  this.props.navigation.push("VerifyNumber", {
                    onlyVerifyNumber: true,
                  })

                }}
              >
                {/* <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    marginTop: 0,
                    height: 56,
                    borderBottomWidth: 1,
                    borderBottomColor:
                      "#BABABA",
                  }}>
                  <CountryPicker
                    style={{
                      height: 40,
                      width: 60,
                      marginLeft:20
                    }}
                    countryCode={
                      this.state.countryName
                    }
                   
                    translation={"ita"}
                    withCallingCodeButton
                    withAlphaFilter
                    withFilter
                    visible={false}
                    onClose={() => {
                      this.setState({
                        showCountry: false,
                      })
                    }}
                    onSelect={(country) => {
                      this.setState({
                        countryName: country.cca2,
                        countryCode:
                          country.callingCode,
                      })
                    }}
                  />
                </View> */}
                <View
                  style={{
                    width: "100%",

                  }}>





                  <TextField

                    label={language.phonenumber}
                    value={this.getPhoneNumber(phoneNumber,countryCode)}
                    lineWidth={1}
                    fontSize={15}
                    editable={false}
                    labelFontSize={13}
                    labelTextStyle={[
                      { paddingTop: 5 },
                      BaseStyle.regularFont,
                    ]}
                    keyboardType="numeric"
                    baseColor={Constant.textFieldBorderColor()}
                    tintColor={Constant.textFieldBorderColor()}
                    textColor={"#000"}
                    // error={isNumberCorrect}
                    onBlur={() => {
                      if (
                        Constant.isValidPhone(
                          phoneNumber
                        )
                      ) {
                        this.isPhoneAvailable()
                      } else {
                        this.setState({
                          isNumberCorrect:
                            "Enter valid phone number",
                        })
                      }
                    }}
                    onChangeText={(text) => {
                      let number = text.replace(
                        /[^0-9]/g,
                        ""
                      )
                      this.setState({
                        phoneNumber: number,
                        isNumberCorrect: "",
                      })
                    }}
                  />

                </View>
              </TouchableOpacity>
              <TextField
                label={language.fullName}
                value={name}
                lineWidth={1}
                fontSize={15}
                labelFontSize={13}
                labelTextStyle={[
                  { paddingTop: 5 },
                  BaseStyle.regularFont,
                ]}
                baseColor={Constant.textFieldBorderColor()}
                tintColor={Constant.textFieldBorderColor()}
                textColor={"#000"}
                onChangeText={(text) =>
                  this.setState({
                    name: text,
                    nameError: "",
                  })
                }
                error={nameError}
              />
              <TextField
                label={language.email}
                value={email}
                lineWidth={1}
                fontSize={15}
                keyboardType="email-address"
                labelTextStyle={[
                  { paddingTop: 5 },
                  BaseStyle.regularFont,
                ]}
                labelFontSize={13}
                baseColor={Constant.textFieldBorderColor()}
                tintColor={Constant.textFieldBorderColor()}
                textColor={"#000"}
                onChangeText={(text) =>
                  this.setState({
                    email: text,
                    isEmailCorrect: "",
                  })
                }
                error={isEmailCorrect}
                onBlur={() => {
                  if (
                    Constant.isValidEmail(email)
                  ) {
                    if (loginwithPhone) { this.isEmailAvailable() }
                  } else {
                    this.setState({
                      isEmailCorrect:
                        "Enter valid Email",
                    })
                  }
                }}
              />
              <TouchableOpacity
                activeOpacity={2}
                onPress={() => {
                  this.setState({
                    menuType: "Gender",
                    showMenu: true,
                    menuArray: genderArray,
                  })
                }}
                style={{
                  height: 100,
                  width: "100%",
                  marginTop: 15,
                }}>
                <Text
                  style={[
                    {
                      fontSize: 12,
                      color:
                        Constant.textFieldBorderColor(),
                    },
                    BaseStyle.regularFont,
                  ]}>
                  {language.gender}
                </Text>
                <View
                  style={{
                    marginTop: 7,
                    width: "100%",
                    paddingRight: 20,
                    alignItems: "center",
                    justifyContent:
                      "space-between",
                    flexDirection: "row",
                  }}>
                  <Text
                    style={[
                      {
                        fontSize: 14,
                        color:
                          "#000",
                      },
                      BaseStyle.regularFont,
                    ]}>
                    {gender}
                  </Text>
                  <Icon name="chevron-down" size={14} color="#000" />
                </View>
                <View
                  style={{
                    backgroundColor:
                      "#000",
                    marginTop: 4,
                    height: 1,
                    width: "100%",
                  }}
                />
              </TouchableOpacity>
            </View>
            {/* </KeyboardAvoidingScrollView> */}




          </View>
        </KeyboardAwareScrollView>
        <StatusBar barStyle="dark-content" translucent backgroundColor="white" />
        <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: 0, width: width, height: height / 12, backgroundColor: '#ebebeb', elevation: 10 }}>
          <TouchableOpacity onPress={() => { isLoading ? null : this.loginAction() }}
          >
            <LinearGradient
              start={{ x: 0, y: 1 }}
              end={{ x: 1, y: 1 }}

              colors={[
                Constant.gradientColor1(),
                    Constant.gradientColor2(),


              ]}
              style={{ borderRadius: 12, height: 45, alignContent: "center", alignItems: "center", justifyContent: "center", width: width - 40 }}>
              <Text style={{ fontSize: 14, color: "#000" }}>CREATE ACCOUNT & LOGIN</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>

        {this.showBottomMenu(menuType, menuArray)}
      </SafeAreaView>
    )
  }

  showBottomMenu(header, data) {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.showMenu}
        onRequestClose={() => { }}>
        <TouchableOpacity
          style={{
            flex: 1,
            height: height,
            width: width,
            position: "absolute",
            bottom: 0,
            backgroundColor: "rgba(0,0,0,0.5)",
          }}
          onPress={() => {
            this.setState({ showMenu: false })
          }}></TouchableOpacity>
        <View
          style={{
            width: width,
            bottom: 0,
            position: "absolute",
            backgroundColor: "#fff",
          }}>
          <View
            style={{
              width: width,
              padding: 10,
              flexDirection: "column",
            }}>
            <View
              style={{
                height: 50,
                justifyContent: "center",
                width: "100%",
                borderBottomWidth: 0.5,
                borderBottomColor:
                  Constant.appColorAlpha(),
              }}>
              <Text style={[BaseStyle.regularFont]}>
                {header}
              </Text>
            </View>

            {data.map((item, index) => {
              return (
                <TouchableOpacity
                  style={{
                    height: 40,
                    justifyContent: "center",
                    width: width,
                  }}
                  onPress={() => {
                    this.setMenuData(header, index)
                  }}>
                  <Text
                    style={[BaseStyle.regularFont]}>
                    {item}
                  </Text>
                </TouchableOpacity>
              )
            })}
          </View>
        </View>
      </Modal>
    )
  }

  getPhoneNumber(phoneNumber,countryCode) {
    if (phoneNumber != "" && phoneNumber != null) 
    {return (countryCode + "-" + phoneNumber) }else{
      return null
    }
  }


  headerView() {
    const { drawerOpen } = this.state

    const userData = global.userData

    return (


      <View style={styles.headerStyle}>
        <TouchableOpacity onPress={() => {
          this.props.navigation.pop(1)
        }} style={styles.goBackButton}>
          <Image
            style={{ width: 10, height: 14, marginLeft: -2, position: 'absolute' }}
            source={images.backicon}
          />
        </TouchableOpacity>
        <Text style={[styles.nameProfile, { marginLeft: 20 }]}>Complete Signup</Text>
      </View>


    )
  }
  isEmailAvailable() {
    const { email } = this.state
    let params = {
      pharmacyId: Constant.pharmacyId(),
      email: email
    }
    Constant.postMethod(
      urls.validateEmail,
      params,
      (result) => {
        console.log(
          "email log" + JSON.stringify(result)
        )
        if (result.success) {
          if (result.result.status == "Success") {
            this.setState({ isEmailCorrect: "" })
          } else {
            Toast.show(result.result.Error)

            this.setState({
              isEmailCorrect:
                "Customer already exist with this email",
            })
          }
        } else {
          this.setState({
            isEmailCorrect:
              "Customer already exist with this email",
          })
        }
      }
    )
  }

  isPhoneAvailable() {
    const { phoneNumber, countryCode } = this.state
    let params = { pharmacyId: Constant.pharmacyId(), phoneNumber: phoneNumber, countryCode: countryCode }
    Constant.postMethod(
      urls.validatePhone,
      params,
      (result) => {
        console.log(
          "phone log" + JSON.stringify(result))
        if (result.success) {
          if (result.result.status == "Success") {
            this.setState({ isNumberCorrect: "" })
          } else {
            // Toast.show('test')

            Toast.show(result.result.Error)
            this.setState({
              isNumberCorrect:
                "Customer already exist with this phone number",
            })
          }
        } else {
          this.setState({ isNumberCorrect: false })
        }
      }
    )
  }
  validatSplChar() {
    const reg =
      /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/

    if (reg.test(this.state.password) === false) {
      return false
    } else {
      return true
    }

  }
  validatNumeric() {
    const reg =
      /[0-9]/

    if (reg.test(this.state.password) === false) {
      return false
    } else {
      return true
    }

  }
  validatUppercase() {
    const reg =
      /[A-Z]/

    if (reg.test(this.state.password) === false) {
      return false
    } else {
      return true
    }

  }

  validate() {
    const {
      email,
      name,
      phoneNumber,
      enterPin,
      confirmPin,
    } = this.state
    var Status = true
    if (name == "") {
      this.setState({ nameError: "Enter Name" })
      Status = false
    }
    if (!Constant.isValidEmail(email)) {
      this.setState({
        isEmailCorrect: "Enter Valid Email",
      })
      Status = false
    }

    if (phoneNumber == "") {
      this.setState({ isNumberCorrect: false })
      Status = false
    }



    if (enterPin != confirmPin) {
      this.setState({
        confirmPinError: "pin is not matching",
      })
      Status = false
    }

    return Status
  }

  
  setMenuData(header, index) {
    this.setState({ showMenu: false })
    if (header == "Gender") {
      this.setState({
        gender: this.state.genderArray[index],
      })
    } else {
      this.setState({
        bloodGroup: this.state.bloodArray[index],
      })
    }
  }

  loginAction() {
    const { isNumberCorrect, isEmailCorrect, loginwithPhone } =
      this.state
    if (
      this.validate() &&
      isNumberCorrect == "" &&
      isEmailCorrect == ""
    ) {

      if (loginwithPhone) {
        this.profileUpdate()
      } else { this.serviceCall() }

    }
    // this.props.navigation.push('Otp',{number:'9959814490'})
  }
  async serviceCall() {
    const {
      email,
      confirmPin,
      countryCode,
      phoneNumber,
      name,
      passWord,
      gender
    } = this.state
    await messaging().registerDeviceForRemoteMessages();
    let fcmToken = await messaging().getToken();
    global.deviceId = fcmToken
    let parameters = {
      pharmacyId: Constant.pharmacyId(),
      email: email,
      passWord: passWord,
      countryCode: countryCode,
      phoneNumber: phoneNumber,
      name: name,
      gender: gender,
      deviceId: fcmToken,
      deviceType:
        Platform.OS === "android"
          ? "Android"
          : "IOS",
      deviceUniqueId: DeviceInfo.getUniqueId(),
      version: DeviceInfo.getVersion(),
      deviceModel: DeviceInfo.getModel(),
      deviceBrand: DeviceInfo.getBrand(),
      deviceVersion: DeviceInfo.getSystemVersion(),
      loginType: "email",
      isPhoneVerified: true

    }

    // this.props.navigation.push("Otp", {
    //   number: phoneNumber,
    //   countryCode: countryCode,
    //   parameters: parameters,
    // })
    this.serviceCall1(parameters)
  }
  serviceCall1(parameters) {
    // const { parameters } = this.state
 console.log("reg param "+JSON.stringify(parameters))
     Constant.postMethod(
       urls.registerCustomer,
       parameters,
       (result) => {
         this.setState({ isLoading: false })
        // this.loadingButton.showLoading(false)
         console.log(
           "servicecall",
           JSON.stringify(result)
         )
         if (result.success) {
           let response = result.result
 
           if (
             response != null &&
             response.Status != "Fail"
           ) {
             let userId =
               response.response.patientData.id
             let userData = response.response.patientData
 
             global.CustomerId = userId
             global.userData = userData
             global.userToken =response.response.token
             console.log("userData", userData)
 
             // this.saveUserData(response.Response)
 
             try {
               Db.write(() => {
                 Db.create(
                   "User",
                   {
                        
                    id: userData.id,
                    name: userData.name,
                    email: userData.email,
                    phoneNumber:
                      userData.phoneNumber,
                 deviceType: userData?.deviceType,
                   // Latitude: userData.Latitude,
                   // Longitude: userData.Longitude,
                    // IsPhoneVerified:
                    //   userData.isPhoneVerified,
                    countryCode:
                      userData.countryCode,
                      pictureUrl:
                      userData.pictureUrl,

                      age: userData.age,
                      sex: userData.sex,
                      temparature: userData.temparature,
                     
                      fastingBloodSugar: userData.fastingBloodSugar,
                      bloodSugar:  userData.bloodSugar,
                      bloodPressureDia: userData.bloodPressureDia,
                      bloodPressureSys: userData.bloodPressureSys,
                      pulse: userData.pulse,
                      deviceUniqueId: userData.deviceUniqueId,
                      token: global.userToken,

                
                    // Dob: userData.Dob,
                    // Gender: userData.Gender,
                    // HealthIssues:
                    //   userData.HealthIssues,
                    // SCProof: userData.SCProof,
                    // SCVerified: userData.SCVerified,
                    // PHCDocument:
                    //   userData.PHCDocument,
                    // PHCVerified:
                    //   userData.PHCVerified,
                    // FirstName: userData.FirstName,
                    // LastName: userData.LastName,
                    // DeviceId: userData.deviceId,
                    // DeviceUniqueId:
                    //   userData.deviceUniqueId,
                    // AuthToken: userData.AuthToken,
                    // ReferralCode:
                    //   userData.ReferralCode,
                    // Version: userData.version,
                    // DeviceModel:
                    //   userData.deviceModel,
                    // DeviceBrand:
                    //   userData.deviceBrand,
                    // DeviceVersion:
                    //   userData.deviceVersion,
                  },
                   "modified"
                 )
                 AsyncStorage.setItem(
                   "userLoggedIn",
                   "true"
                 )
                 this.props.navigation.navigate(
                   // "EditProfile",
                   "Home",
                   { isHome: true }
                 )
 
                 //   this.props.navigation.dispatch(
                 // CommonActions.reset({
                 //   index: 0,
                 //   routes: [
                 //     {
                 //       name: 'Home',
                 //     },
                 //   ],
                 // })
                 //);
               })
             } catch (e) {
               console.log("Error on creation "+ e)
               Toast.show("Unable to save data")
             }
 
             // this.props.navigation.dispatch(
             //           CommonActions.reset({
             //             index: 0,
             //             routes: [
             //               {
             //                 name: 'Home',
             //               },
             //             ],
             //           })
             //         );
           } else {
             Toast.show("Something went wrong")
           }
         } else {
         }
       }
     )
   }
 

async   profileUpdate() {
    const {
      name,
      phoneNumber,
      countryCode,
      gender,
      email
    } = this.state


    var parameters = {
      id: global.CustomerId,
      pharmacyId: Constant.pharmacyId(),
      name: name,

      sex: gender == "Select" ? "" : gender,
      email: email,
      countryCode: countryCode,
      phoneNumber: phoneNumber,


      isPhoneVerified: true,


    }



    console.log("log", parameters)
    Constant.postWithTokenMethod(
      urls.profileUpdate,
      parameters,
      (result) => {
        console.log("profileupdate", JSON.stringify(result))
        this.setState({ isLoading: false })

        if (result.success) {
          let response = result.result

          if (response.status != "Fail") {
            let userId =
              response.response.patientData.id


            let userData = response.response.patientData
            global.userData = userData

            // this.saveUserData(response.Response)

            try {
              Db.write(() => {
                Db.create(
                  "User",
                  {
                    CustomerId: userData.id,
                    Username: userData.name,
                    Email: userData.email,
                    PhoneNumber:
                      userData.phoneNumber,

                    IsPhoneVerified:
                      userData?.isPhoneVerified,
                    CountryCode:
                      userData?.countryCode,

                    Gender: userData.sex,


                  },
                  "modified"
                )


                this.props.navigation.dispatch(
                  CommonActions.reset({
                    index: 0,
                    routes: [
                      {
                        name: "TabNavigationBar",
                      },
                    ],
                  })
                )
              }
              )
            } catch (e) {
              console.log("Error on creation")
              Toast.show("Unable to save data")
            }
          } else {
            Toast.show("Something went wrong")
          }
        }
      }
    )
  }
}

const styles = StyleSheet.create({
  goBackButton: {
    marginLeft: width / 18,
    backgroundColor: '#fff',
    width: width / 12,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 40,
    elevation: 3
  },
  nameProfile: {
    fontWeight: '500',
    fontSize: 18,
    fontFamily: "Roboto-Regular",
    color: Constant.orange()
  },
  headerStyle: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  tracktext: {
    fontSize: 12, fontFamily: "Roboto-Regular",

    color: "#000"
  },
  scrrenStyle: {
    marginTop: 10,
    width: width,
    height: "100%",
  },

  forgot: {
    marginTop: 25,
    color: Constant.appColorAlpha(),
    marginLeft: 15,
    fontSize: 16,
  },
  register: {
    marginTop: 25,
    color: Constant.appColorAlpha(),
    marginLeft: 15,
    fontSize: 16,
  },

  startView: {
    marginTop: 10,
    borderRadius: 25,
    height: 50,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 10,
    alignItems: "center",
    justifyContent: "center",
    elevation: 8
  },
  startBtn: {
    color: "#fff",
    fontSize: 16,

  },
  pageNoStyle: {
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  pageStyle: {
    width: 60,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  dotStyle: {
    marginTop: 5,
    height: 6,
    width: 6,
    backgroundColor: Constant.appColorAlpha(),
    borderRadius: 3,
  },
  title: {
    marginLeft: 20,
    marginRight: 20,
    lineHeight: 30,
    fontSize: 20,
    color: Constant.appColorAlpha(),
  },
  subtitle: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    lineHeight: 20,
  },
  pageCount: {
    fontSize: 16,
  },

  startBtn: {
    color: "#fff",
    fontSize: 16,

  },
  passwordgreenview: {

    height: 20, borderRadius: 5,
    paddingHorizontal: 5, alignContent: "center", justifyContent: "center",
  },
  passSeqText: {
    fontSize: 12, fontFamily: "Roboto-Regular",
  }
})

export default Register
