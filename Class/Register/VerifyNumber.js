import React, { Component } from "react"
import {
  SafeAreaView,
  View,
  Text,
  ScrollView,
  AsyncStorage,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import Db from "./../DB/Realm"
import  messaging  from "@react-native-firebase/messaging"
import Icon from 'react-native-vector-icons/FontAwesome5';
import { StatusBar } from "react-native";
import BaseStyles from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import Images from "./../BaseClass/Images"
import AnimateLoadingButton from "./../ExtraClass/LoadingButton"
import { KeyboardAvoidingScrollView } from "react-native-keyboard-avoiding-scroll-view"
import CountryPicker from "react-native-country-picker-modal"
import urls from "./../BaseClass/ServiceUrls"
import Toast from "react-native-simple-toast"
import DeviceInfo from "react-native-device-info"
import {
  getUniqueId,
  getManufacturer,
} from "react-native-device-info"
import LinearGradient from "react-native-linear-gradient"
import language from "./../BaseClass/language"
import {
  TextField,
  OutlinedTextField,
} from "react-native-material-textfield"
const { width, height } = Dimensions.get("window")
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import { ImageBackground } from "react-native"
import images from "./../BaseClass/Images"
import BackButtonHeader from "../Profile/components/backButtonHeader"
class VerifyNumber extends Component {
  constructor(props) {
    super(props)
    this.state = {
      countryName: "IN",
      countryCode:"+91",
      showEnterPin: false,
      isNumberCorrect: "",
      showConfirmPin: false,
      confirmPin: "",
      enterPin: "",
      isConfirm: false,
      isEmailCorrect: "",
      pinColor: "",
      name: "",
      nameError: "",
      phoneNumber: "",
      enterPinError: "",
     
    
      
  

      loginwithPhone:props.route.params.loginwithPhone,

    }
  }

  render() {
    const {
     
      isNumberCorrect,
     
      phoneNumber,
     countryCode

    } = this.state
    return (
      <SafeAreaView style={{ flex: 1,backgroundColor:"#fff" }}>
     
      <View style={{ marginTop: 30, flexDirection: 'row' }}>
        {this.headerView()}
      </View>



<KeyboardAwareScrollView>

<View style={[BaseStyles.cornerRadiusView14,{marginHorizontal:20,elevation:5,paddingHorizontal:20,marginVertical:30}]}>



<Text style={[styles.headerTextStyle, { marginLeft: 5, fontSize: 14, color: Constant.textGreyColor() }]}>You will be required to verify your new phone number using OTP. You can change your phone number anytime.</Text>

<View
style={{
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center", marginTop: 20
}}>
<View
    style={{
        justifyContent: "center",
        alignItems: "center",
        marginTop: 0,
        height: 56,
        borderBottomWidth: 1,
        borderBottomColor:
            Constant.textFieldBorderColor(),
    }}>
    <CountryPicker
        style={{
            height: 40,
            width: 60,
        }}
        countryCode={
            this.state.countryName
        }
        translation={"ita"}
        withCallingCodeButton
        withAlphaFilter
        withFilter
        visible={false}
        onClose={() => {
            this.setState({
                showCountry: false,
            })
        }}
        onSelect={(country) => {
            this.setState({
                countryName: country.cca2,
                countryCode:
                    country.callingCode,
            })
        }}
    />
</View>
<View
    style={{
        width: width - 120,
        paddingLeft: 10,
    }}>
    <TextField
        label={language.phonenumber}
        value={phoneNumber}
        lineWidth={1}
        fontSize={15}
        labelFontSize={13}
        labelTextStyle={[
            { paddingTop: 5 },
            BaseStyles.regularFont,
        ]}
        keyboardType="numeric"
        baseColor={Constant.textFieldBorderColor()}
        tintColor={Constant.textFieldBorderColor()}
        textColor={"#000"}
        error={isNumberCorrect}
        onBlur={() => {
          if (
            Constant.isValidPhone(
              phoneNumber
            )
          ) {
            this.isPhoneAvailable()
          } else {
            this.setState({
              isNumberCorrect:
                "Enter valid phone number",
            })
          }
        }}
        onChangeText={(text) => {
            let number = text.replace(
                /[^0-9]/g,
                ""
            )
            this.setState({
                phoneNumber: number,
                isNumberCorrect: "",
            })
        }}
    />
</View>
</View>

<TouchableOpacity


onPress={() => {
    const { isNumberCorrect } =
    this.state
  if (
   
   ! isNumberCorrect
    
  ) {
    this.props.navigation.push("VerifyOtp", {
        phoneNumber: phoneNumber,
         countryCode: countryCode,
        
         parameters: "",
     })
  }
 
   

    // this.props.navigation.push('Otp',{number:'9959814490'})


}}>
<LinearGradient
    start={{ x: 0, y: 1 }}
    end={{ x: 1, y: 1 }}

    colors={[
        Constant.gradientColor1(),
        Constant.gradientColor2(),

    ]}
    style={{ marginTop: 30, marginBottom: 20, borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: width - 80 }}>
    <Text style={{ fontSize: 14, color: "#000" }}>VERIFY NUMBER</Text>
</LinearGradient>
</TouchableOpacity>


</View>

            

           
            
          </KeyboardAwareScrollView>
          <StatusBar barStyle="dark-content" translucent backgroundColor="white" />
          
        
       
      </SafeAreaView>
    )
  }
  headerView() {
   

    return (


      <View style={styles.headerStyle}>
      <TouchableOpacity  onPress={() => {
            this.props.navigation.pop(1)
          }} style={styles.goBackButton}>
       <Image
            style={{ width: 10, height: 14,marginLeft:-2,position:'absolute'}}
            source={images.backicon}
          />
      </TouchableOpacity>
      <Text style={[styles.nameProfile,{marginLeft:20}]}>Phone Number</Text>
    </View>

     
    )
  }
 

  isPhoneAvailable() {
    const { phoneNumber,countryCode } = this.state
    let params = { pharmacyId:Constant.pharmacyId(),phoneNumber: phoneNumber,countryCode:"+"+countryCode }
    Constant.postMethod(
      urls.validatePhone,
      params,
      (result) => {
        console.log(
          "phone log" + JSON.stringify(result))
        if (result.success) {
          if (result.result.status == "Success") {
            this.setState({ isNumberCorrect: "" })
          } else {
            // Toast.show('test')

            Toast.show(result.result.Error)
            this.setState({
              isNumberCorrect:
                "Customer already exist with this phone number",
            })
          }
        } else {
          this.setState({ isNumberCorrect: false })
        }
      }
    )
  }


 

  
 
  
}

const styles = StyleSheet.create({
  goBackButton: {
    marginLeft: width / 18, 
    backgroundColor: '#fff', 
    width: width / 12, 
    height: 30, 
    justifyContent: 'center', 
    alignItems: 'center', 
    borderRadius: 40, 
    elevation: 3 
  },
  nameProfile: {
    fontWeight: '500', 
    fontSize: 18,
    fontFamily:"Roboto-Regular", 
    color:"#000"
  },
  headerStyle: { 
    flexDirection: 'row', 
    alignItems: 'center' 
  },
  

  
 

 
  
})

export default VerifyNumber
