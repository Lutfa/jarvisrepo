import React, { Component } from "react"
import {
  View,
  Text,
  Animated,
  Alert,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import SplashScreen from "react-native-splash-screen"
import { FlatList } from "react-native-gesture-handler"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import Accordion from "react-native-collapsible/Accordion"
import ImageLoad from "./../BaseClass/ImageLoader"
import CartDb from "./../DB/CartDb"
import Db from "./../DB/Realm"
import Urls from "./../BaseClass/ServiceUrls"
import Toast from "react-native-simple-toast"
const { width, height } = Dimensions.get("window")
import HTMLView from "react-native-htmlview"

class ProductDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: false,
      dotPosition: 0,
      activeSections: [0, 1],
      productData: props.route.params.product,
       
      sections: [],
      productId:
        props.route.params != null
          ? props.route.params.product.Id
          : 0,
    }
    console.log("productdata "+JSON.stringify(this.state.productData))

  }

  componentDidMount() {
    SplashScreen.hide()
    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
          this.forceUpdate()
        }
      )

    const { productData, productId } = this.state
console.log("productid "+productId)
    if (productId > 0) {
      console.log("backend "+productId)


      this.getProductDetails()
    } else {
      let NewDataArray = []

      let DateArray = [
        {
          name: "Short Description",
          data: productData.ShortDescription,
        },
        {
          name: "Full Description",
          data: productData.FullDescription,
        },
        {
          name: "AdultDosage",
          data: productData.AdultDosage,
        },
        {
          name: "ChildrenDosage",
          data: productData.ChildrenDosage,
        },
        {
          name: "ContraIndication",
          data: productData.ContraIndication,
        },
        {
          name: "SideEffectsOrAdverseEffects",
          data: productData.SideEffectsOrAdverseEffects,
        },
        {
          name: "PrecautionOrWarning",
          data: productData.PrecautionOrWarning,
        },
        {
          name: "DrugInteractions",
          data: productData.DrugInteractions,
        },
        {
          name: "RecomandAdultDosage",
          data: productData.RecomandAdultDosage,
        },
        {
          name: "PregnancyAlert",
          data: productData.PregnancyAlert,
        },
        {
          name: "LactationAlert",
          data: productData.LactationAlert,
        },
      ]

      DateArray.map((item) => {
        if (item.data != null) {
          // console.log(item.data);

          let dic = {
            title: item.name,
            content: item.data,
          }

          NewDataArray.push(dic)
        }
      })

      this.setState({ sections: NewDataArray })
    }
  }
  render() {
    const { isLoading, productData, sections } =
      this.state

    if (productData == null) {
      return (
        <SafeAreaView
          style={{
            flex: 1,
            backgroundColor: "#fff",
          }}>
          {isLoading ? Constant.showLoader() : null}
          {this.headerView()}
        </SafeAreaView>
      )
    }

    let Isexit = CartDb.productExist(productData.Id)
    let IsWishlistExist =
      CartDb.productExistInWishList(productData.Id)

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        {isLoading ? Constant.showLoader() : null}

        {this.headerView()}

        <ScrollView>
          <View style={{ height: 280 }}>
            <ScrollView
              horizontal={true}
              scrollEventThrottle={16}
              pagingEnabled={true}
              showsHorizontalScrollIndicator={false}
              onMomentumScrollEnd={
                this._onMomentumBanner
              }>
              {productData.Images.map((url, i) => {
                // the _ just means we won't use that parameter
                return (
                  <View style={styles.imageStyle}>
                    <ImageLoad
                      resizeMode={"contain"}
                      placeholderStyle={{
                        height: "60%",
                        width: width,
                      }}
                      style={{
                        height: "60%",
                        width: width,
                      }}
                      loadingStyle={{
                        size: "large",
                        color:
                          Constant.appColorAlpha(),
                      }}
                      source={{ uri: url.Src }}
                      placeholderSource={
                        images.placeholder
                      }
                    />
                    {/* <Image  resizeMode={'contain'} style = {{height:'60%',width:width}} source = {images.testNew}/> */}
                  </View>
                )
              })}

              {productData.Images.length <= 0 ? (
                <View style={styles.imageStyle}>
                  <Image
                    resizeMode={"contain"}
                    style={{
                      height: "60%",
                      width: width,
                    }}
                    source={images.placeholder}
                  />
                </View>
              ) : null}
            </ScrollView>

            <View style={styles.bannerDotView}>
              {productData.Images.map((_, i) => {
                // the _ just means we won't use that parameter

                return (
                  <Animated.View
                    key={i}
                    style={[
                      styles.bannerDot,
                      {
                        backgroundColor:
                          i ==
                          this.state.dotPosition
                            ? Constant.appColorAlpha()
                            : Constant.headerColor(),
                      },
                    ]}
                  />
                )
              })}

              <View
                style={{
                  flexDirection: "row",
                  position: "absolute",
                  right: 20,
                  height: -20,
                  height: 70,
                }}>
                {productData.ColorCode ==
                Constant.prescriptionColorCode() ? (
                  <TouchableOpacity
                    onPress={() => {}}
                    style={{
                      alignItems: "center",
                      justifyContent: "center",
                      height: 40,
                      width: 40,
                      backgroundColor:
                        Constant.appColorAlpha(),
                      borderRadius: 20,
                      zIndex: 100,
                    }}>
                    <Image
                      resizeMode={"center"}
                      style={{
                        height: 20,
                        width: 20,
                      }}
                      source={images.prescription}
                    />
                  </TouchableOpacity>
                ) : null}

                <TouchableOpacity
                  onPress={() => {
                    if (IsWishlistExist) {
                      CartDb.deleteWishList(
                        productData.Id
                      )
                    } else {
                      let isAdded =
                        CartDb.addWishListData(
                          productData
                        )
                      if (isAdded) {
                        Toast.show(
                          "Product added to wishlist"
                        )
                      }
                    }
                    this.forceUpdate()
                  }}
                  style={{
                    alignItems: "center",
                    marginLeft: 15,
                    justifyContent: "center",
                    height: 40,
                    width: 40,
                    backgroundColor:
                      Constant.appColorAlpha(),
                    borderRadius: 20,
                    zIndex: 100,
                  }}>
                  <Image
                    tintColor={"#fff"}
                    resizeMode={"center"}
                    style={{
                      height: 20,
                      width: 20,
                    }}
                    source={
                      IsWishlistExist
                        ? images.wishlistSelected
                        : images.pwishlist
                    }
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View
            style={{
              paddingLeft: 15,
              paddingRight: 15,
              paddingTop: 10,
              flex: 1,
              elevation: 0,
              backgroundColor: "#fff",
            }}>
            <Text
              style={[
                { fontSize: 18, color: "#000" },
                BaseStyle.boldFont,
              ]}>
              {productData.Name}
            </Text>
            <Text
              style={[
                {
                  fontSize: 14,
                  marginTop: 5,
                  color: Constant.appColorAlpha(),
                },
                BaseStyle.regularFont,
              ]}>
              {productData.ManufactureName}
            </Text>

            <Accordion
              activeSections={[0]}
              sections={sections}
              activeSections={
                this.state.activeSections
              }
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
              onChange={this._updateSections}
              underlayColor={""}
            />
          </View>
        </ScrollView>

        <View
          style={{
            height: 50,
            width: "100%",
            flexDirection: "row",
            paddingRight: 15,
            paddingLeft: 15,
            elevation: 4,
            alignItems: "center",
            justifyContent: "space-between",
            overflow: "visible",
            marginTop: 2,
            backgroundColor: "#fff",
          }}>
          <Text
            style={[
              { fontSize: 18, color: "#000" },
              BaseStyle.boldFont,
            ]}>
            {Constant.price()}
            {parseFloat(productData.Price).toFixed(
              2
            )}
          </Text>
          <View
            style={{
              width: 180,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "flex-end",
            }}>
            {Isexit ? (
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  width: 65,
                  marginRight: 20,
                }}>
                <TouchableOpacity
                  onPress={() => {
                    let cartItem =
                      Db.objectForPrimaryKey(
                        "CartDB",
                        productData.Id
                      )
                    if (cartItem.count - 1 > 0) {
                      try {
                        Db.write(() => {
                          cartItem.count =
                            cartItem.count - 1
                        })
                      } catch (e) {
                        console.log(
                          "Error on creation"
                        )
                      }

                      this.forceUpdate()
                    }
                  }}>
                  <Image
                    resizeMode={"contain"}
                    style={{
                      height: 23,
                      width: 23,
                    }}
                    source={images.pMinus}
                  />
                </TouchableOpacity>
                <Text
                  style={[
                    {
                      fontSize: 14,
                      padding: 8,
                      color: "#000",
                    },
                    BaseStyle.boldFont,
                  ]}>
                  {CartDb.CartDataCount(
                    productData.Id
                  )}
                </Text>

                <TouchableOpacity
                  onPress={() => {
                    try {
                      Db.write(() => {
                        let cartItem =
                          Db.objectForPrimaryKey(
                            "CartDB",
                            productData.Id
                          )
                        cartItem.count =
                          cartItem.count + 1
                      })
                    } catch (e) {
                      console.log(
                        "Error on creation"
                      )
                      console.log(e)
                    }
                    this.forceUpdate()
                  }}>
                  <Image
                    resizeMode={"contain"}
                    style={{
                      height: 23,
                      width: 23,
                    }}
                    source={images.pPlus}
                  />
                </TouchableOpacity>
              </View>
            ) : null}

            <TouchableOpacity
              onPress={() => {
                if (Isexit) {
                  this.props.navigation.navigate(
                    "Cart"
                  )
                } else {
                  let isAdded =
                    CartDb.addData(productData)

                  let cartData =
                    Db.objects("CartDB")
                  // alert(JSON.stringify(cartData))
                  // console.log(JSON.stringify(cartData));
                  if (isAdded) {
                    Toast.show(
                      "Product added to cart"
                    )
                  }
                  this.forceUpdate()
                }
              }}
              style={{
                width: 100,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                height: 40,
                borderRadius: 20,
                backgroundColor:
                  Constant.appColorAlpha(),
              }}>
              <Text
                style={[
                  { fontSize: 14, color: "#fff" },
                  BaseStyle.boldFont,
                ]}>
                {Isexit
                  ? "Go to Cart"
                  : "Add to cart"}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    )
  }

  _onMomentumBanner = ({ nativeEvent }: any) => {
    const index = Math.round(
      nativeEvent.contentOffset.x / width
    )

    if (index !== this.state.dotPosition) {
      this.setState({ dotPosition: index })
    }
  }

  headerView() {
    const { isLoading, productData, sections } =
      this.state

    return (
      <View
        style={{
          backgroundColor: Constant.headerColor(),
          elevation: 4,
          borderBottomLeftRadius: 30,
          borderBottomRightRadius: 30,
        }}>
        <View
          style={{
            paddingTop: 10,
            paddingBottom: 15,
            backgroundColor: Constant.headerColor(),
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.pop(1)
            }}
            style={{
              marginLeft: 15,
              height: 35,
              borderRadius: 15,
              alignItems: "center",
              justifyContent: "center",
            }}>
            <Image
              tintColor="#fff"
              style={{
                height: 20,
                width: 20,
                left: 0,
              }}
              source={images.back}
            />
          </TouchableOpacity>
          <Text
            style={[
              { flex: 1 },
              BaseStyle.headerFont,
            ]}>
            PRODUCT DETAILS
          </Text>

          <View
            style={{
              flexDirection: "row",
              marginRight: 15,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate(
                  "Cart"
                )
              }}
              style={{
                marginRight: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                resizeMode={"center"}
                style={{ width: 20, height: 20 }}
                source={images.cart}
              />
              {CartDb.Count() > 0 ? (
                <View
                  style={{
                    position: "absolute",
                    left: 12,
                    top: -6,
                    width: 20,
                    justifyContent: "center",
                    alignItems: "center",
                    height: 20,
                    borderRadius: 10,
                    backgroundColor:
                      Constant.appColorAlpha(),
                  }}>
                  <Text
                    style={[
                      {
                        color: "#fff",
                        fontSize: 12,
                      },
                      BaseStyle.boldFont,
                    ]}>
                    {CartDb.Count()}
                  </Text>
                </View>
              ) : null}
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate(
                  "Wishlist"
                )
              }}
              style={{
                marginLeft: 5,
                marginRight: 10,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                resizeMode={"center"}
                style={{ width: 20, height: 20 }}
                source={images.pwishlist}
              />
              {CartDb.WishListCount() > 0 ? (
                <View
                  style={{
                    position: "absolute",
                    left: 12,
                    top: -6,
                    width: 20,
                    justifyContent: "center",
                    alignItems: "center",
                    height: 20,
                    borderRadius: 10,
                    backgroundColor:
                      Constant.appColorAlpha(),
                  }}>
                  <Text
                    style={[
                      {
                        color: "#fff",
                        fontSize: 12,
                      },
                      BaseStyle.boldFont,
                    ]}>
                    {CartDb.WishListCount()}
                  </Text>
                </View>
              ) : null}
            </TouchableOpacity>

            {/* <TouchableOpacity
              onPress = {()=>{
                  this.props.navigation.pop(1)
              }}
               style  ={{marginLeft:15,height:35,borderRadius:15,alignItems:'center',justifyContent:'center'}}>
              <Image resizeMode = {'center'} tintColor = '#fff' style = {{height:20,width:20,left:0}} source = {images.pwishlist}/>
              </TouchableOpacity> */}
          </View>
        </View>
      </View>
    )
  }

  _renderHeader = (
    content,
    index,
    isActive,
    sections
  ) => {
    return (
      <View
        style={{ paddingTop: 6, marginBottom: 10 }}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
          }}>
          <Text
            style={[
              {
                fontSize: 18,
                color: "#000",
                flex: 1,
              },
              BaseStyle.boldFont,
            ]}>
            {content.title}
          </Text>
          <Image
            style={{ height: 16, width: 16 }}
            source={
              isActive
                ? images.upArrow
                : images.downArrow
            }
          />
        </View>
        <View
          style={{
            marginTop: 4,
            width: 60,
            height: 4,
            backgroundColor:
              Constant.appColorAlpha(),
          }}
        />
      </View>
    )
  }

  _renderContent = (section) => {
    return (
      <View style={{ marginTop: 5 }}>
        <HTMLView
          value={section.content}
          stylesheet={styles}
        />
      </View>
    )
  }

  _updateSections = (activeSections) => {
    this.setState({ activeSections })
  }

  getProductDetails() {
    const { productId } = this.state

    this.setState({ isLoading: true })
    let params = {
      ProductId: productId,
    }

    Constant.postMethod(
      Urls.getProductDetails,
      params,
      (result) => {
        console.log("product details "+ JSON.stringify(result))
        this.setState({ isLoading: false })
        if (result.success) {
          if (result.result.Response != null) {
            this.setState({
              productData: result.result.Response,
            })
            alert(JSON.stringify(productData))
          }
        }
      }
    )
  }
}

const styles = StyleSheet.create({
  bannerDotView: {
    flexDirection: "row",
    paddingBottom: 5,
    paddingTop: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  bannerDot: {
    height: 8,
    width: 8,
    elevation: 4,
    backgroundColor: Constant.appColorAlpha(),
    margin: 3,
    borderRadius: 4,
  },
  imageStyle: {
    height: 260,
    width: width,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F2F7F4",
  },
  p: {
    fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
})

export default ProductDetails
