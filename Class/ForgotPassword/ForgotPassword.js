import React,{Component}from "react"
import { View,Platform,AsyncStorage,Keyboard,Text,ScrollView,Image,StyleSheet,Dimensions,TouchableOpacity} from "react-native";
import BaseStyle from '../BaseClass/BaseStyles'
import  Constant from '../BaseClass/Constant'
import Images from '../BaseClass/Images'
import TextField from '../Login/lib/TextField'
import AnimateLoadingButton from '../ExtraClass/LoadingButton'
const { width,height } = Dimensions.get('window');
import { useNavigation } from "@react-navigation/core";
import { getUniqueId, getManufacturer } from 'react-native-device-info';
import DeviceInfo from 'react-native-device-info';
import urls from '../BaseClass/ServiceUrls'
import Toast from 'react-native-simple-toast';
import LinearGradient from 'react-native-linear-gradient';
import images from "../BaseClass/Images";
import {KeyboardAvoidingScrollView} from 'react-native-keyboard-avoiding-scroll-view';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import language from './../BaseClass/language'

class ForgotPassword extends Component
{

    static navigationOptions = 
    {
        header: null,
  
    };

    constructor(props)
    {
        super(props);
        this.state={
            email:'',
            isMailCorrect : true,
            isPasswordCorrect:true,
            password:''
          
        }
    }

    render()
    {

        const {email,isMailCorrect,password,isPasswordCorrect} = this.state
        return(
            <LinearGradient start={{x: 0, y: 0.5}} end={{x: 0, y: 1}} colors={[ Constant.gradientColor1(),
                Constant.gradientColor2(),
]} style={{flex:1}}>



{/* <KeyboardAwareScrollView extraHeight={90} extraScrollHeight={50}> */}


                <View style = {styles.container}>

                <View style = {{height:'48%', alignItems:'center',justifyContent:'center'}}>
                <Image resizeMode={'contain'} style = {{height:'100%',width:width-20}} source = {images.logoBack}/>
                </View>
           
              

        <View style = {{height:60,paddingTop:30, width:'100%',justifyContent:'center',alignItems:'center'}}>
        
        <Text style ={[{color:'#000',fontSize:20},BaseStyle.boldFont]}>{language.forgotpassword}</Text>

        </View>

              
            <View style = {{padding:15,paddingBottom:50,height:'20%'}}>
        <TextField
          inputStyle = {[{height:50,color:'#fff',fontSize:16}]}
          ref="email"
          labelColor={Constant.appFullColor()}
          value={email}
          label={language.emailpassword}
         textFocusColor={!isMailCorrect ? 'red'  : '#fff'}
         onChangeText={(text)=>{
            this.setState({email:text,isMailCorrect:true})
          }}
          highlightColor={!isMailCorrect ? 'red' : Constant.appFullColor() }
          onBlur={()=>{
          }}
        />

        </View>



       
        <TouchableOpacity style = {styles.startView}>
       
                <AnimateLoadingButton
                 ref={c => (this.loadingButton = c)}
                 width={width-30}
                 height={50}
                 title={language.send}
                 titleFontSize={20}
                 titleWeight={'100'}
                 titleColor={Constant.appFullColor()}
                 backgroundColor={'#fff'}
                 borderRadius={6}
                 onPress={()=>{
                     this.sendAction()
                 }}
               />
                </TouchableOpacity>



       

        </View>

       

        {/* </KeyboardAwareScrollView> */}


        </LinearGradient>
        )
    }

   
   

    checkPassword()
    {
        const {password} = this.state
          if(password == '')
            {
             this.setState({isPasswordCorrect:false})
             Toast.show('Enter password');

             return false
            }
            return true
            
    }
    

    sendAction() {

this.props.navigation.push('CreatePin')

        // this.loadingButton.showLoading(true);
    
        // // mock
        // setTimeout(() => {
        //   this.loadingButton.showLoading(false);
        //   this.props.navigation.push('Register')

        // }, 1000);
      }

    

}


const styles = StyleSheet.create({
 container:{
     flex: 1,
     height:height,
     width:width,
     paddingTop:50
 },
 scrrenStyle:{
     marginTop:10,
    width:width,
    height:'100%',
 },

forgot:{
     color:Constant.appFullColor(),
     position:'absolute',
     right:10,
     top:17,
     fontSize:13
 },
 register:{
     marginTop:25,
     color:Constant.appColorAlpha(),
     marginLeft:15,
     fontSize:16
 },

 startView:{
     marginTop:20,
    borderRadius : 25,
    height:50,
    marginLeft:15,
    marginRight:15,
    alignItems:'center',justifyContent:'center'
 },
 startBtn:
 {
     color:'#fff',
     fontSize:16

 },
 pageNoStyle:
 {
     width:'100%',
     height:50,
     alignItems:'center',
     justifyContent: 'center',
 },
 pageStyle:{
     width:60,
     height:'100%',
     alignItems:'center',
     justifyContent: 'center',
 },
 dotStyle:{
     marginTop:5,
     height:6,
     width:6,
     backgroundColor:Constant.appColorAlpha(),
     borderRadius:3
 },
 title:{
     marginLeft:20,marginRight:20,lineHeight:30,
     fontSize:20,
     color:Constant.appColorAlpha()
 },
 subtitle:{
     marginTop:15, marginLeft:15,marginRight:15,lineHeight:20
 },
 pageCount:{
     fontSize:16
 },

 startBtn:
 {
     color:'#fff',
     fontSize:16

 }

 
});



export default ForgotPassword