import React, { Component } from "react"
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  FlatList,
  Image,
  Platform,
  Animated,
  StatusBar,
  StyleSheet,
  Modal,
  Alert,
  AsyncStorage,
  ScrollView,
  TextInput,
} from "react-native"
import LinearGradient from "react-native-linear-gradient"

import StarRating from "react-native-star-rating"
import Constant from "../BaseClass/Constant"
import BaseStyles from "../BaseClass/BaseStyles"
import SplashScreen from "react-native-splash-screen"
import images from "../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import language from "../BaseClass/language"
import urls from "../BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")
import Db from "../DB/Realm"
import ImageLoad from "../BaseClass/ImageLoader"
import BackButtonHeader from "../Profile/components/backButtonHeader"

var Realm = require("realm")

class Pharmacist extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selectedItem: props.route.params.data,

    }
console.log("item"+JSON.stringify(this.state.selectedItem))
    language.setLanguage("en")
  }

  async componentDidMount() {
    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
          this.forceUpdate()
        }
      )


    SplashScreen.hide()

  }

  render() {
    const
      {
        selectedItem
      } = this.state
    return (
      <SafeAreaView style={{backgroundColor:"#fff"}}>
      <ImageBackground style={{ height: "100%", width: "100%" }} source={images.bg}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ flex: 1 }}>
            {this.headerView()}
            <Image style={{ resizeMode: "center", width: width, position: "absolute", height: 199, marginTop: 130 }} source={images.background_vectors}></Image>
            <View style={{ position: "absolute", marginTop: 270, backgroundColor: "#fff", borderTopLeftRadius: 28, borderTopRightRadius: 28, width: "100%", height: "100%", paddingTop: 20 }}>
            </View>
            <View>
              <View
                style={{
                  backgroundColor: "#fff",
                  marginHorizontal: 20,
                  height: 300,
                  elevation: 5,
                  padding: 0,
                  marginTop: 25,

                  shadowOffset: {
                    width: 0,
                    height: 3,
                  },
                  shadowColor: "#d3d3d6",
                  shadowRadius: 5,
                  shadowOpacity: 0.6,
                  overflow: "hidden",
                  borderRadius: 10,
                }}>
                {/* <Image
              style={{
                resizeMode:"contain",
                width: "100%",
                height: "100%",
              }}
              source={images.manicon}></Image> */}
                <ImageLoad
                  style={{
                    width: "100%",
                    height: "100%",
                  }}
                  loadingStyle={{
                    size: "large",
                    color: "blue",
                  }}
                  source={{
                    uri: selectedItem.pictureUrl,
                  }}
                  placeholderSource={
                    images.manicon
                  }
                />
              </View>
              <View style={{ marginHorizontal: 20 }}>
                <View style={{ flexDirection: "row", }}>
                  <Text style={[{ letterSpacing: 2, color: Constant.appColor(), marginTop: 15, fontSize: 18 }, BaseStyles.boldFont]}> {selectedItem.name}</Text>
                  <Text style={[{ marginLeft: 5, letterSpacing: 0, color: "#000", marginTop: 15, fontSize: 16 }, BaseStyles.regularFont]}> {selectedItem.age} {selectedItem.pronuouns}</Text>

                </View>
                <Text style={[{ marginLeft: 5, letterSpacing: 0, color: "#000", marginTop: 15, fontSize: 14 }, BaseStyles.boldFont]}>{selectedItem.qualification}</Text>
                <View style={{ flexDirection: "row", marginTop: 10, }}>
                  <StarRating
                    disabled={false}
                    maxStars={5}
                    rating={4}
                    fullStarColor="#FFD500"
                    starSize={15}
                    starStyle={{ padding: 3 }}

                  />
                  <Text style={[{ marginLeft: 15, letterSpacing: 0, color: Constant.textColor(), fontSize: 12 }, BaseStyles.regularFont]}>{selectedItem.rating} ratings | {selectedItem.totalCalls} calls</Text>

                </View>
                <Text style={[{ marginLeft: 5, letterSpacing: 0, color: Constant.appColor(), marginTop: 10, fontSize: 14 }, BaseStyles.boldFont]}>About {selectedItem.name}</Text>
                <Text style={[{ marginLeft: 5, letterSpacing: 0, color: "#000", marginTop: 10, fontSize: 14 }, BaseStyles.regularFont]}>{selectedItem.about}</Text>
                <View style={{ width: "100%", flex: 0, flexDirection: "row", marginTop: 10, justifyContent: "space-evenly" }}>


                  <View style={[styles.patientContainer]}>
                    <Text style={[{ letterSpacing: 0, color: Constant.appColor(), marginTop: 15, fontSize: 14 }, BaseStyles.boldFont]}> {selectedItem.experience} Years</Text>
                    <Text style={[styles.bottomTextStyle, BaseStyles.regularFont]}>EXPERIENCE</Text>
                  </View>

                  <View style={[styles.patientContainer, { marginLeft: 15 }]}>
                    <Text style={[{ letterSpacing: 0, color: Constant.appColor(), marginTop: 15, fontSize: 14 }, BaseStyles.boldFont]}> {selectedItem.totalReviews}</Text>
                    <Text style={[styles.bottomTextStyle, BaseStyles.regularFont]}>REVIEWS</Text>



                  </View>
                </View>

                <View style={{ width: "100%", marginTop: height / 10, paddingBottom: 20, flexDirection: "row", justifyContent: "space-around" }}>
                  <LinearGradient
                    start={{ x: 0, y: 1 }}
                    end={{ x: 1, y: 1 }}
                    colors={[
                      "#1d90ff",
                      "#8307ff",

                    ]}
                    style={{ borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: width * 0.3 }}>
                    <Text style={{ paddingHorizontal: 20, fontSize: 14, color: "#fff", alignContent: "center" }}>CHAT</Text>
                  </LinearGradient>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate(
                        "BookAppointment",{
                          data:selectedItem
                        }
                      )
                    }}
                  >
                    <LinearGradient
                      start={{ x: 0, y: 1 }}
                      end={{ x: 1, y: 1 }}

                      colors={[
                        Constant.gradientColor1(),
                    Constant.gradientColor2(),


                      ]}
                      style={{ marginLeft: 30, borderRadius: 8, height: 40, alignContent: "center", justifyContent: "center", alignItems: "center", width: width * 0.55 }}>
                      <Text style={{ fontSize: 14, color: "#000" }}>BOOK APPOINTMENT</Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
      </SafeAreaView>
    )
  }


  headerView() {
    return (
      <SafeAreaView>
        <StatusBar
          translucent
          barStyle="dark-content"
          backgroundColor="transparent"

        />

        <BackButtonHeader navigation={this.props.navigation} titleHeader={this.state.selectedItem.name} />
      </SafeAreaView>
    )
  }
}
const styles = StyleSheet.create({
  headerInnerView: {
    height: 50,
    alignItems: "center",
    flexDirection: "row",
  },
  homeTitle: {
    color: "#fff",
    fontSize: 16,
    fontFamily: "montserrant-semi-bold",
    fontWeight: "200",
    left: 20,
  },
  header: {
    fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
  patientContainer: {
    backgroundColor: "#fff",
    borderRadius: 8,
    elevation: 2,
    alignContent: "center",
    alignItems: "center",
    width: (width - 50) / 2


  }, bottomTextStyle: { marginBottom: 10, marginHorizontal: 8, letterSpacing: 0, color: Constant.appColor(), marginTop: 5, fontSize: 10, }
})

export default Pharmacist