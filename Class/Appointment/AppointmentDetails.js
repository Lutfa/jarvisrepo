import React, { Component } from "react"
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  FlatList,
  Image,
  Platform,
  Animated,
  StatusBar,
  StyleSheet,
  Modal,
  Alert,
  AsyncStorage,
  ScrollView,
  TextInput,
  ImageBackground
} from "react-native"
import LinearGradient from "react-native-linear-gradient"
import ViewPager from "@react-native-community/viewpager"
import Constant from "./../BaseClass/Constant"
import BaseStyles from "../BaseClass/BaseStyles"
import SplashScreen from "react-native-splash-screen"
import images from "../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import language from "../BaseClass/language"
import urls from "../BaseClass/ServiceUrls"
import StarRating from "react-native-star-rating"
const { width, height } = Dimensions.get("window")


var Realm = require("realm")

class AppointmentDetails extends Component {
  constructor(props) {
    super(props)

    this.state = {

      isCategories: true,
      isBestSelling: true,
      bestOffersArray: [],
      answersArray: ["","","",""],
     
    }
   
    language.setLanguage("en")
  }

  async componentDidMount() {
    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
          this.forceUpdate()
        }
      )


    SplashScreen.hide()

  }

  render() {
    const{ optionsArray}=this.state
    return (
      <SafeAreaView
      style={[
        styles.headerStyle,

      ]}
    >
      <ImageBackground style={{ height: "100%", width: "100%" }} source={images.bg}>
        <View >
          {this.headerView()}
        <View
            style={{
             
              width: "100%",
              paddingTop: 8,
             
             
            }}>
                <ScrollView >
                  <View>
           {this.appointmentui()} 
           {this.questionsui()}
           </View>
         </ScrollView>

          
       </View>
       
</View>
</ImageBackground>


        
     
      </SafeAreaView >
    )
  }
  headerView() {
    const { drawerOpen } = this.state

    const userData = global.userData

    return (
      <SafeAreaView
      style={{alignContent:"center",}}
      >
        <StatusBar
          translucent
          barStyle="dark-content"
          //  backgroundColor="rgba(0, 0, 0, 0.251)"
          backgroundColor={Constant.headerColor()}

        />

        <TouchableOpacity
          activeOpacity={2}
          style={styles.headerInnerView}
          onPress={() => {
            this.props.navigation.pop(1)
          }}
        >
           <View style={[BaseStyles.circle]}>
          <Image
            style={{ width: 10, height: 14,marginLeft:-2,position:'absolute'}}
            source={images.backicon}
          />
          </View>
          <Text style={[BaseStyles.headerFontblack]}>
            Appointment Details
          </Text>


        </TouchableOpacity>
       
      </SafeAreaView>
    )
  }
  
  appointmentui(){
    return(
        <View>
          
            <View style={[BaseStyles.cornerRadiusView,{marginTop:30}]}>
            <Image style={{position:"absolute",bottom:0,borderBottomLeftRadius:28,borderBottomRightRadius:28, width:"100%"}} source={images.appt_bottom_curve}></Image>

                      <View style={{ flexDirection: "row", width: "100%" ,paddingLeft:20}}

                      >

                        <Image style={{marginLeft:15,  height: 66, width: 66, borderRadius: 15, }} source={images.manicon}></Image>
                        <View>
                            <Text   style={[{ marginLeft:20, color:"#000",fontSize:16 }, BaseStyles.regularFont,]}> Appointment with  
                          <Text 
                            style={[{ marginLeft:5,marginTop: 12,color:Constant.appColor(),fontSize:16 }, BaseStyles.regularFont,]}> David Nihn</Text>
                         </Text>
                          <View style={{ flexDirection: "row", marginTop: 15 }}

                          >
                            <View style={{ flexDirection: "row", marginLeft: 20 }}

                            ><Image style={{height:20,width:18}} source={images.calender}></Image>
                              <Text
                                style={[{ marginHorizontal: 10, }, BaseStyles.descFontblack,]}>Aug 4, 22</Text>

                            </View>
                            <View style={{ flexDirection: "row" }}

                            ><Image style={[BaseStyles.smallIconStyle, { marginLeft: 10 }]} source={images.schedule}></Image>
                              <Text
                                style={[{ marginHorizontal: 10, }, BaseStyles.descFontblack,]}>5.30 PM to 6.30 PM</Text>

                            </View>
                          </View>

                        </View>
                      </View>


                    </View>
        </View>
    )
}

questionsui(){
  return(
      <View>
        {this.questionTypeDesc()}
{this.ratingView()}

        </View>
  )}
  questionTypeDesc(){
    return(
    <View>
      <View style={[BaseStyles.cornerRadiusView,{padding:20,marginTop:30}]}>

<Text style={[styles.questionLeft,BaseStyles.boldFont]}>
Describe your current situation and why do you need this call?
</Text>
<Text
              style={[
                {fontSize:12,color:"#000",marginTop:10},
                BaseStyles.regularFont,
              ]}
            
             
            >
                Zayed, based on what I understood in our call. Here are my recommended meds for you:
Calpol
Azethramycim
Doxycycline 500 mg
            </Text>
</View>
    </View>)
  }
  ratingView(){
    const enableSubmitButton=false;
    return(
    <View>
      <View >

              <LinearGradient
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}

                colors={[
                  "#000",
                  "#000",

                ]}
                style={{padding:20, marginTop:5,marginBottom:10,borderRadius: 28, width: width}}>

<View>
<Text style={[styles.questionLeft,BaseStyles.boldFont,{color:"#fff"}]}>
How would you rate David
</Text> 
<View style={{flexDirection:"row",width:"100%",marginTop:10,alignItems:"center",alignContent:"center",justifyContent:"flex-start",}}> 
<Text style={BaseStyles.descFontblack}>Rate David</Text>


<View style={{marginLeft:5,alignItems:"center",justifyContent:"center",flexDirection:"row",height:24,width:"14%",borderRadius:7,borderColor:"#8F8F8F",borderWidth:1}}>
                      <Text 
                          style={[{  marginHorizontal: 5,color:Constant.orange(),fontSize:16,fontWeight:400 }, BaseStyles.regularFont,]}>1</Text>
                      
                    { <StarRating
                   disabled={false}
                   maxStars={1}                   rating={1}
                   fullStarColor={Constant.orange()}
                   starSize={12}
                  
                  
                    ></StarRating> }
                      </View>
                      <View style={{marginLeft:5,alignItems:"center",justifyContent:"center",flexDirection:"row",height:24,width:"14%",borderRadius:7,borderColor:"#8F8F8F",borderWidth:1}}>
                      <Text 
                          style={[{  marginHorizontal: 5,color:Constant.orange(),fontSize:16,fontWeight:400 }, BaseStyles.regularFont,]}>2</Text>
                      
                    { <StarRating
                   disabled={false}
                   maxStars={1}                   rating={1}
                   fullStarColor={Constant.orange()}
                   starSize={12}
                  
                  
                    ></StarRating> }
                      </View>
                      <View style={{marginLeft:5,alignItems:"center",justifyContent:"center",flexDirection:"row",height:24,width:"14%",borderRadius:7,borderColor:"#8F8F8F",borderWidth:1}}>
                      <Text 
                          style={[{  marginHorizontal: 5,color:Constant.orange(),fontSize:16,fontWeight:400 }, BaseStyles.regularFont,]}>3</Text>
                      
                    { <StarRating
                   disabled={false}
                   maxStars={1}                   rating={1}
                   fullStarColor={Constant.orange()}
                   starSize={12}
                  
                  
                    ></StarRating> }
                      </View>
                      <View style={{marginLeft:5,alignItems:"center",justifyContent:"center",flexDirection:"row",height:24,width:"14%",borderRadius:7,borderColor:"#8F8F8F",borderWidth:1}}>
                      <Text 
                          style={[{  marginHorizontal: 5,color:Constant.orange(),fontSize:16,fontWeight:400 }, BaseStyles.regularFont,]}>4</Text>
                      
                    { <StarRating
                   disabled={false}
                   maxStars={1}                   rating={1}
                   fullStarColor={Constant.orange()}
                   starSize={12}
                  
                  
                    ></StarRating> }
                      </View>
                      <View style={{marginLeft:5,alignItems:"center",justifyContent:"center",flexDirection:"row",height:24,width:"14%",borderRadius:7,borderColor:"#8F8F8F",borderWidth:1}}>
                      <Text 
                          style={[{  marginHorizontal: 5,color:Constant.orange(),fontSize:16,fontWeight:400 }, BaseStyles.regularFont,]}>5</Text>
                      
                    { <StarRating
                   disabled={false}
                   maxStars={1}                   rating={1}
                   fullStarColor={Constant.orange()}
                   starSize={12}
                  
                  
                    ></StarRating> }
                      </View>
                      
</View>  
 {/* TextInput */}

<View
style={{borderRadius:11,width:"100%",padding:20,backgroundColor:"#fff",minHeight:"25%",marginRight:20,marginTop:10}}>

<TextInput
            style={[
              { fontSize: 12, color: "#000",padding:20 },
              BaseStyles.regularFont,
            ]}

            onChangeText={(text) => {
              if(text.length>0){
              
              this.setState({
                enableSubmitButton:true
              })
            }else{
              enableSubmitButton:false
            }
            

            }}
            placeholder="Write your review here.."
          />
</View>


<LinearGradient
                        start={{ x: 0, y: 1 }}
                        end={{ x: 1, y: 1 }}
                      // if  enableSubmitButton =true 
                        colors={
                          
                          [
                           
                            Constant.gradientColor1(),
                            Constant.gradientColor2(),

                        ]}
                      // else  both colors #BDBDBD
                        style={{ marginLeft:35,marginRight:35, marginTop: 20, marginBottom: 10, borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: width-70 }}>
                        <Text style={{ fontSize: 14, color: "#000" }}>ANSWER QUESTIONS</Text>
                      </LinearGradient>

</View>


  </LinearGradient>
             
</View>
    </View>
    )
  }

}
const styles = StyleSheet.create({
  headerStyle: {
    flex: 0,
backgroundColor:"#e5e5e5",
 




    justifyContent: "center",
  },
  headerInnerView: {
    height: 50,
    alignItems: "center",
    flexDirection: "row",
    alignContent: "center",
    paddingHorizontal:20
  },

  homeTitle: {
    color: "#fff",
    fontSize: 16,
    fontFamily: "Roboto-bold",
    fontWeight: "200",
    left: 20,
  },
  header: {
    fontSize: 12,
    fontFamily: "Roboto-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
  patientContainer: {
    backgroundColor: "#fff",
    borderRadius: 8,
    elevation: 2,
    alignContent: "center",
    alignItems: "center",
    width: (width - 50) / 3


  }, headingText: { letterSpacing: 0, color: "#000", marginTop: 15, fontSize: 14, marginTop: 20 }
  ,questionLeft:{
    fontSize: 14,
   
    
    color: Constant.appColor() }
})

export default AppointmentDetails