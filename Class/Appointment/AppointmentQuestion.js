import React, { Component } from "react"
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  FlatList,
  Image,
  Platform,
  Animated,
  StatusBar,
  StyleSheet,
  Modal,
  Alert,
  AsyncStorage,
  ScrollView,
  TextInput,
  ImageBackground
} from "react-native"
import LinearGradient from "react-native-linear-gradient"
import ViewPager from "@react-native-community/viewpager"
import Constant from "./../BaseClass/Constant"
import BaseStyles from "../BaseClass/BaseStyles"
import SplashScreen from "react-native-splash-screen"
import images from "../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import language from "../BaseClass/language"
import urls from "../BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")


var Realm = require("realm")

class AppointmentQuestion extends Component {
  constructor(props) {
    super(props)

    this.state = {

      isCategories: true,
      isBestSelling: true,
      bestOffersArray: [],
      answersArray: ["", "", "", ""],

    }

    language.setLanguage("en")
  }

  async componentDidMount() {
    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
          this.forceUpdate()
        }
      )


    SplashScreen.hide()

  }

  render() {
    const { optionsArray } = this.state
    return (
      <SafeAreaView
        style={[
          styles.headerStyle,

        ]}
      >
        <ImageBackground style={{ height: "100%", width: "100%" }} source={images.bg}>
          <View >
            {this.headerView()}
            <View
              style={{

                width: "100%",
                paddingTop: 8,


              }}>
              <ScrollView >
                <View>
                  {this.appointmentui()}
                  {this.questionsui()}
                </View>
              </ScrollView>

              <View style={{ flex: 0, position: "absolute", bottom: 20, paddingHorizontal: 15, backgroundColor: "#fff", elevation: 12, width: "100%", marginTop: 15, flexDirection: "row", justifyContent: "flex-start" }}>
                <LinearGradient
                  start={{ x: 0, y: 1 }}
                  end={{ x: 1, y: 1 }}

                  colors={[
                    "#000",
                    "#000",

                  ]}
                  style={{ marginTop: 5, marginBottom: 10, borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: width * 0.4 }}>
                  <Text style={{ paddingHorizontal: 20, fontSize: 14, color: "#fff", alignContent: "center" }}>BACK</Text>
                </LinearGradient>
                <LinearGradient
                  start={{ x: 0, y: 1 }}
                  end={{ x: 1, y: 1 }}

                  colors={[
                    Constant.gradientColor1(),
                    Constant.gradientColor2(),

                  ]}
                  style={{ marginLeft: 30, marginTop: 5, marginBottom: 10, borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: width * 0.4 }}>
                  <Text style={{ fontSize: 14, color: "#000" }}>NEXT</Text>
                </LinearGradient>
              </View>
            </View>

          </View>
        </ImageBackground>




      </SafeAreaView >
    )
  }
  headerView() {
    const { drawerOpen } = this.state

    const userData = global.userData

    return (
      <SafeAreaView
        style={{ alignContent: "center", }}
      >
        <StatusBar
          translucent
          barStyle="dark-content"
          //  backgroundColor="rgba(0, 0, 0, 0.251)"
          backgroundColor={Constant.headerColor()}

        />

        <TouchableOpacity
          activeOpacity={2}
          style={styles.headerInnerView}
          onPress={() => {
            this.props.navigation.pop(1)
          }}
        >
          <View style={[BaseStyles.circle]}>
            <Image
              style={{ width: 10, height: 14, marginLeft: -2, position: 'absolute' }}
              source={images.backicon}
            />
          </View>
          <Text style={[BaseStyles.headerFontblack]}>
            Appointment Questions
          </Text>


        </TouchableOpacity>
        <View style={{ position: "absolute", right: 0, justifyContent: "flex-end", height: 60 }}>
          <Text style={[styles.questionLeft, BaseStyles.boldFont]}>
            3/4
          </Text>
          <Text style={[{ fontSize: 12 }, BaseStyles.regularFont]}>
            QUESTIONS LEFT
          </Text>
        </View>
      </SafeAreaView>
    )
  }

  appointmentui() {
    return (
      <View>

        <View style={[BaseStyles.cornerRadiusView,{marginTop:30}]}>
          <Image style={{ position: "absolute", bottom: 0, borderBottomLeftRadius: 28, borderBottomRightRadius: 28, width: "100%" }} source={images.appt_bottom_curve}></Image>

          <View style={{ flexDirection: "row", width: "100%", paddingLeft: 20 }}

          >

            <Image style={{ marginLeft: 15, marginVertical: 20, height: 66, width: 66, borderRadius: 15, }} source={images.manicon}></Image>
            <View>
              <Text style={[{ marginLeft: 20, marginTop: 12, color: "#000", fontSize: 16 }, BaseStyles.regularFont,]}> Appointment with
                <Text
                  style={[{ marginLeft: 5, marginTop: 12, color: Constant.appColor(), fontSize: 16 }, BaseStyles.regularFont,]}> David Nihn</Text>
              </Text>
              <View style={{ flexDirection: "row", marginTop: 15 }}

              >
                <View style={{ flexDirection: "row", marginLeft: 20 }}

                ><Image style={BaseStyles.smallIconStyle} source={images.calender}></Image>
                  <Text
                    style={[{ marginHorizontal: 10, }, BaseStyles.descFontblack,]}>Aug 4, 22</Text>

                </View>
                <View style={{ flexDirection: "row" }}

                ><Image style={[BaseStyles.smallIconStyle, { marginLeft: 10 }]} source={images.schedule}></Image>
                  <Text
                    style={[{ marginHorizontal: 10, }, BaseStyles.descFontblack,]}>5.30 PM to 6.30 PM</Text>

                </View>
              </View>

            </View>
          </View>


        </View>
      </View>
    )
  }

  questionsui() {
    return (
      <View>
        {this.questionTypeDesc()}
        {this.questionTypeSingleChoice()}

      </View>
    )
  }
  questionTypeDesc() {
    return (
      <View>
        <View style={[BaseStyles.cornerRadiusView, { minHeight: 300, paddingHorizontal: 20 ,marginTop:30}]}>

          <Text style={[styles.questionLeft, BaseStyles.boldFont]}>
            Describe your current situation and why do you need this call?
          </Text>
          <TextInput
            style={[
              { fontSize: 12, color: "#000", },
              BaseStyles.regularFont,
            ]}

            onChangeText={(text) => {
              this.setState({

              })

            }}
            placeholder="Write your answer here.."
          />
        </View>
      </View>)
  }
  questionTypeSingleChoice() {
    return (
      <View>
        <View style={[BaseStyles.cornerRadiusView, { minHeight: 300, paddingHorizontal: 20 ,marginTop:30}]}>

          <Text style={[styles.questionLeft, BaseStyles.boldFont]}>
            What’s your body temperature?
          </Text>

          {this.state.answersArray.map((value, i) => {
            // the _ just means we won't use that parameter
            return (
              <TouchableOpacity
                activeOpacity={2}
                onPress={() => {
                  this.setState({
                    // imageIndex: i,

                  })
                }}>
                <LinearGradient
                  start={{ x: 0, y: 1 }}
                  end={{ x: 1, y: 1 }}

                  colors={


                    [
                      "#1D90FF",
                      "#8307FF",

                    ]}
                  style={{ marginTop: 20, marginBottom: 10, borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: "100%" }}>
                  <Text style={{ fontSize: 14, color: "#fff" }}>98.5 F - 100 F</Text>
                </LinearGradient>
              </TouchableOpacity>
            )
          })}
        </View>
      </View>
    )
  }

}
const styles = StyleSheet.create({
  headerStyle: {
    flex: 0,
    backgroundColor: "#e5e5e5",





    justifyContent: "center",
  },
  headerInnerView: {
    height: 50,
    alignItems: "center",
    flexDirection: "row",
    alignContent: "center",
    paddingHorizontal: 20
  },

  homeTitle: {
    color: "#fff",
    fontSize: 16,
    fontFamily: "Roboto-bold",
    fontWeight: "200",
    left: 20,
  },
  header: {
    fontSize: 12,
    fontFamily: "Roboto-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
  patientContainer: {
    backgroundColor: "#fff",
    borderRadius: 8,
    elevation: 2,
    alignContent: "center",
    alignItems: "center",
    width: (width - 50) / 3


  }, headingText: { letterSpacing: 0, color: "#000", marginTop: 15, fontSize: 14, marginTop: 20 }
  , questionLeft: {
    fontSize: 14,


    color: Constant.appColor()
  }
})

export default AppointmentQuestion