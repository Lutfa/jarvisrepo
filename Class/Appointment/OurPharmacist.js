import React, { Component } from "react"
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  FlatList,
  Image,
  Platform,
  Animated,
  StatusBar,
  StyleSheet,
  Modal,
  Alert,
  AsyncStorage,
  ScrollView,
  TextInput,

} from "react-native"
import LinearGradient from "react-native-linear-gradient"

import Constant from "../BaseClass/Constant"
import BaseStyles from "../BaseClass/BaseStyles"
import SplashScreen from "react-native-splash-screen"
import images from "../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import language from "../BaseClass/language"
import urls from "../BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")
import Db from "../DB/Realm"
import ImageLoad from "../BaseClass/ImageLoader"
import moment from "moment"

import axios from "axios"
import { Calendar } from "react-native-calendars"
import StarRating from "react-native-star-rating"
var Realm = require("realm")

class OurPharmacist extends Component {
  constructor(props) {
    super(props)

    this.state = {

      isCategories: true,
      isBestSelling: true,
      pharmacistArray: [],
    }

    language.setLanguage("en")
  }

  async componentDidMount() {
    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
          this.forceUpdate()
        }
      )


    SplashScreen.hide()

    this.getAllPharmacist()
  }


  getAllPharmacist() {
    let params = {
      pharmacyId: 1,
      patientId: global.CustomerId,
      LastUpdatedTimeTicks: 0,
    }
    Constant.postMethod(
      urls.getPharmacist,
      params, (result) => {
console.log("pharmacist res "+JSON.stringify(result))

        if (result.success) {
          if (result.result.response != null) {
            this.setState ( {
              pharmacistArray:result.result.response.pharmacists
            })

          }
        }
      }
    )
  }

  render() {
    const {   pharmacistArray } = this.state
    return (
      <SafeAreaView
        style={[
          styles.headerStyle,

        ]}
      >
        <ImageBackground style={{ height: "100%", width: "100%" }} source={images.bg}>
          <View style={{ flex: 1 }}>
            {this.headerView()}
            <View style={[BaseStyles.cornerRadiusView,{marginTop:30}]}>
                
                <ImageBackground style={{borderTopLeftRadius:28,borderTopRightRadius:28,position:"absolute",top:0,width:width,height:100}} source={images.pharma_top_curve}></ImageBackground>

<Image style={{position:"absolute",bottom:0,borderBottomLeftRadius:28,borderBottomRightRadius:28, width:"100%",height:80}} source={images.pharma_bottom_curve}></Image>


                
                               <View style={{ flexDirection: "row",}}>
                        <View
                          style={{ marginLeft: 15 }}>

                          <Text
                            style={[{ fontSize: 14, color: "#000" }, BaseStyles.boldFont]}
                          >
                            Chat OR Book an Appointment
                          </Text>
                          <Text
                            style={[{ fontSize: 12, color: "#000" }, BaseStyles.regularFont]}
                          >
                            Connect with our pharmacists
                          </Text>
                        </View>
                       
                      </View>

                      <FlatList
                        style={{ marginTop: 10 ,paddingRight:10}}
                        data={pharmacistArray}
                        numColumns={2}
                        renderItem={({ item, index }) => (
                          <View style={{flex:1}}>
                            <TouchableOpacity


                              onPress={() => {
                                this.props.navigation.navigate(
                                    "Pharmacist",
                                    {data:item} 
                                )
                              }}

                              style={{ marginLeft: 12, marginTop: 12 }}>
                              <ImageBackground style={{ flex: 1, borderRadius: 10, height: 150,  }} source={images.manicon}>
                                <LinearGradient
                                  start={{ x: 0, y: 1 }}
                                  end={{ x: 0, y: 0 }}
                                  style={styles.linearGradient}
                                  colors={['rgba(0,0,0,0.60)', 'rgba(0,0,0,0.00)']} style={{ flex: 1, borderRadius: 10, }}>
                                  <View style={{ flex: 1, borderRadius: 8, height: 40, alignContent: "flex-end", marginLeft: 15, justifyContent: "flex-end", }}>
                                    <View style={{ flexDirection: "row", flex: 0 }}>
                                      <View>
                                        <Text style={{ fontSize: 14, color: "#fff", alignContent: "center" }}>{item?.name}</Text>
                                        <Text style={{ marginTop: 5, fontSize: 12, color: "#fff", alignContent: "center", marginBottom: 10 }}>{item?.age} {item?.pronuouns}</Text>
                                      </View>
                                      <View style={{ flexDirection: "row", position: "absolute", bottom: 10, right: 10, alignContent: "flex-end",justifyContent:"center",alignItems:"center" }}>
                                        <StarRating
                                          disabled={false}
                                          maxStars={1}
                                          rating={1}
                                          fullStarColor="#FFD500"
                                          starSize={15}></StarRating>
                                        <Text style={{ marginLeft: 4, fontSize: 14, color: "#fff", alignContent: "center" }}>{item?.rating}</Text>

                                      </View>
                                    </View>
                                  </View>
                                </LinearGradient>

                              </ImageBackground>






                            </TouchableOpacity>
                          </View>
                        )}
                      />
                    </View>



          </View>
        </ImageBackground>
      </SafeAreaView >
    )
  }
  headerView() {
    const { drawerOpen } = this.state

    const userData = global.userData

    return (
      <SafeAreaView
      style={{ paddingHorizontal: 15}}
      >
        <StatusBar
          translucent
          barStyle="dark-content"
          //  backgroundColor="rgba(0, 0, 0, 0.251)"
          backgroundColor={Constant.headerColor()}

        />

        <TouchableOpacity
          activeOpacity={2}
          style={styles.headerInnerView}
          onPress={() => {
            this.props.navigation.pop(1)
          }}
        >
         <View style={[BaseStyles.circle]}>
          <Image
            style={{ width: 10, height: 14,marginLeft:-2,position:'absolute'}}
            source={images.backicon}
          />
          </View>
          <Text style={[BaseStyles.headerFontblack]}>
            Our Phamacists
          </Text>


        </TouchableOpacity>
      </SafeAreaView>
    )
  }
}
const styles = StyleSheet.create({
  headerStyle: {
    flex: 0,

backgroundColor:"#fff",




    justifyContent: "center",
  },
  headerInnerView: {
    height: 20,
    alignItems: "center",
    flexDirection: "row",
    alignContent: "center"
  },

  homeTitle: {
    color: "#fff",
    fontSize: 16,
    fontFamily: "montserrant-semi-bold",
    fontWeight: "200",
    left: 20,
  },
  header: {
    fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
  patientContainer: {
    backgroundColor: "#fff",
    borderRadius: 8,
    elevation: 2,
    alignContent: "center",
    alignItems: "center",
    width: (width - 50) / 3


  }, headingText: { letterSpacing: 0, color: "#000", marginTop: 30, fontSize: 16, marginTop: 20}
})

export default  OurPharmacist 
