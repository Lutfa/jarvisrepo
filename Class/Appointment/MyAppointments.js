import React, { Component } from "react"
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  FlatList,
  Image,
  Platform,
  Animated,
  StatusBar,
  StyleSheet,
  Modal,
  Alert,
  AsyncStorage,
  ScrollView,
  TextInput,
  ImageBackground
} from "react-native"
import LinearGradient from "react-native-linear-gradient"
import ViewPager from "@react-native-community/viewpager"
import Constant from "../BaseClass/Constant"
import BaseStyles from "../BaseClass/BaseStyles"
import SplashScreen from "react-native-splash-screen"
import images from "../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import language from "../BaseClass/language"
import urls from "../BaseClass/ServiceUrls"
import StarRating from "react-native-star-rating"
const { width, height } = Dimensions.get("window")


var Realm = require("realm")

class MyAppointments extends Component {
  constructor(props) {
    super(props)

    this.state = {
      pageIndex: 0,
      isCategories: true,
      isBestSelling: true,
      previousArray: [],
      upcomingArray: [],
      appointmentArray: [],

    }
    this.viewPager = React.createRef()
    language.setLanguage("en")
  }

  async componentDidMount() {
    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
          this.forceUpdate()
        }
      )


    SplashScreen.hide()
    this.getAllAppointments()
  }

  render() {
    const { optionsArray, pageIndex, } = this.state
    return (
      <SafeAreaView
        style={[
          styles.headerStyle,

        ]}
      >
        <ImageBackground style={{ height: "100%", width: "100%" }} source={images.bg}>
          <View style={{ flex: 1 }}>
            {this.headerView()}
            <View
              style={{
                height: 50,
                width: "100%",
                paddingTop: 8,
                flexDirection: "row",
              }}>
              <TouchableOpacity
                onPress={() => {
                  this.viewPager.current.setPage(0)
                }}
                style={{
                  flex: 0.5,
                  alignItems: "center",
                  justifyContent: "center",
                  borderBottomColor:
                    Constant.selectedTextColor(),
                  borderBottomWidth:
                    pageIndex == 0 ? 2 : 0,
                }}>
                <Text
                  style={[
                    {
                      fontSize: 16,
                      color:
                        Constant.selectedTextColor(),
                    },
                    BaseStyles.regularFont,
                  ]}>
                  UPCOMMING
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  this.viewPager.current.setPage(1)
                }}
                style={{
                  flex: 0.5,
                  alignItems: "center",
                  justifyContent: "center",
                  borderBottomColor:
                    Constant.selectedTextColor(),
                  borderBottomWidth:
                    pageIndex == 1 ? 2 : 0,
                }}>
                <Text
                  style={[
                    {
                      fontSize: 16,
                      color:
                        Constant.selectedTextColor(),
                    },
                    BaseStyles.regularFont,
                  ]}>
                  PREVIOUS
                </Text>
              </TouchableOpacity>
            </View>
            <ViewPager
              style={{ flex: 1 }}
              ref={this.viewPager}
              scrollEnabled={false}
              initialPage={0}
              onPageSelected={this.onPageSelected}>
              <View key="1">{this.appointmentui()}</View>
              <View key="2">{this.prevappointmentui()}</View>
            </ViewPager>







          </View>

        </ImageBackground>
      </SafeAreaView >

    )
  }
  headerView() {
    const { drawerOpen } = this.state

    const userData = global.userData

    return (
      <SafeAreaView
      >
        <StatusBar
          translucent
          barStyle="dark-content"
          //  backgroundColor="rgba(0, 0, 0, 0.251)"
          backgroundColor={Constant.headerColor()}

        />

        <TouchableOpacity
          activeOpacity={2}
          style={styles.headerInnerView}
          onPress={() => {
            this.props.navigation.pop(1)
          }}
        >
          <View style={[BaseStyles.circle]}>
            <Image
              style={{ width: 10, height: 14, marginLeft: -2, position: 'absolute' }}
              source={images.backicon}
            />
          </View>
          <Text style={[BaseStyles.headerFontblack]}>
            My Appointments
          </Text>


        </TouchableOpacity>
      </SafeAreaView>
    )
  }

  appointmentui() {
    const { previousArray, upcomingArray } = this.state
    return (
      <View>

        <FlatList
          style={{ marginTop: 10 }}
          keyExtractor={item => item.id}
          data={previousArray}

          numColumns={1}
          renderItem={({ item, index }) => (
            <View style={[BaseStyles.cornerRadiusView, { marginTop: 30 }]}>
              <Image style={{ position: "absolute", bottom: 0, borderBottomLeftRadius: 28, borderBottomRightRadius: 28, width: "100%" }} source={images.appt_bottom_curve}></Image>

              <View style={{ flexDirection: "row", width: "100%", paddingLeft: 20 }}

              >

                <Image
                  style={{
                    marginLeft: 15, height: 50, width: 50, borderRadius: 14,
                  }}

                  source={{
                    uri: this.state.data.pharmacistPictureUrl,
                  }}

                />                                  <View>
                  <Text
                    style={[{ marginHorizontal: 20, color: Constant.appColor(), fontSize: 18 }, BaseStyles.boldFont,]}>{item.pharmacistName}</Text>
                  <View style={{ flexDirection: "row", marginTop: 15 }}

                  >
                    <View style={{ flexDirection: "row", marginLeft: 20 }}

                    ><Image style={BaseStyles.smallIconStyle} source={images.calender}></Image>
                      <Text
                        style={[{ marginHorizontal: 10, }, BaseStyles.descFontblack,]}>{this.getDate(item)}</Text>

                    </View>
                    <View style={{ flexDirection: "row" }}

                    ><Image style={[BaseStyles.smallIconStyle, { marginLeft: 10 }]} source={images.schedule}></Image>
                      <Text
                        style={[{ marginHorizontal: 10, }, BaseStyles.descFontblack,]}>{item.stot}</Text>

                    </View>
                  </View>

                </View>
              </View>

              <TouchableOpacity onPress={() => {
                this.props.navigation.navigate("AppointmentQuestion",
                {
                  data:item
                }
                )
              }}>
                <LinearGradient
                  start={{ x: 0, y: 1 }}
                  end={{ x: 1, y: 1 }}

                  colors={[
                    Constant.gradientCol1r1(),
                    Constant.gradientColor2(),

                  ]}
                  style={{ marginLeft: 35, marginRight: 35, marginTop: 20, marginBottom: 10, borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: width - 70 }}>
                  <Text style={{ fontSize: 14, color: "#000" }}>ANSWER QUESTIONS</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          )}
        />







      </View>
    )
  }
  prevappointmentui() {
    return (
      <View>
        <TouchableOpacity onPress={() => {
          this.props.navigation.navigate("AppointmentDetails")
        }}>

<FlatList
          style={{ marginTop: 10 }}
          keyExtractor={item => item.id}
          data={previousArray}

          numColumns={1}
          renderItem={({ item, index }) => (
            <View style={[BaseStyles.cornerRadiusView, { marginTop: 30 }]}>
            <Image style={{ position: "absolute", bottom: 0, borderBottomLeftRadius: 28, borderBottomRightRadius: 28, width: "100%" }} source={images.appt_bottom_curve}></Image>

            <View style={{ flexDirection: "row", width: "100%", paddingLeft: 20 }}

            >

<Image
                  style={{
                    marginLeft: 15, height: 50, width: 50, borderRadius: 14,
                  }}

                  source={{
                    uri: this.state.data.pharmacistPictureUrl,
                  }}

                />               <View>
                <View style={{ flexDirection: "row", }}>
                  <Text
                    style={[{ marginHorizontal: 20, color: Constant.appColor(), fontSize: 18 }, BaseStyles.boldFont,]}>{item.name}</Text>
                  <View style={{ alignItems: "center", alignContent: "center", justifyContent: "center", flexDirection: "row", height: 24, width: "18%", borderRadius: 7, backgroundColor: Constant.gradientColor2() }}>
                    <Text
                      style={[{ marginHorizontal: 5, color: "#000", fontSize: 16, fontWeight: 400 }, BaseStyles.regularFont,]}>{item?.pharmacistRating}</Text>

                    <StarRating
                      disabled={false}
                      maxStars={1}
                      rating={1}
                      fullStarColor="#000"
                      starSize={11}


                    ></StarRating>
                  </View>

                </View>
                <View style={{ flexDirection: "row", marginTop: 15 }}

                >
                  <View style={{ flexDirection: "row", marginLeft: 20 }}

                  ><Image style={BaseStyles.smallIconStyle} source={images.calender}></Image>
                    <Text
                      style={[{ marginHorizontal: 10, }, BaseStyles.descFontblack,]}>{this.getDate(item)}</Text>

                  </View>
                  <View style={{ flexDirection: "row" }}

                  ><Image style={[BaseStyles.smallIconStyle, { marginLeft: 10 }]} source={images.schedule}></Image>
                    <Text
                      style={[{ marginHorizontal: 10, }, BaseStyles.descFontblack,]}>{item.slot}</Text>

                  </View>
                </View>

              </View>
            </View>


          </View>
          )}
        />




         
        </TouchableOpacity>
      </View>
    )
  }
  getDate(data) {
    let date_moment = moment(data.dateUtc, "YYYY-MM-DDTHH:mm:ssZ");
    console.log(date_moment)
    let req_format = date_moment.format("DD MMM, yy");

    return req_format;
  }
  getAllAppointments() {
    console.log("global " + JSON.stringify(global.userData))

    let params = {
      pharmacyId: 1,
      patientId: global.CustomerId,
      LastUpdatedTimeTicks: 0,
    }
    Constant.getMethod(
      urls.getPharmacist + global.CustomerId,
      (result) => {
        console.log("appointmentts res " + JSON.stringify(result))

        if (result.success) {
          if (result.result.response != null) {
            this.setState({
              appointmentArray: result.result.response.appointment

            })
            this.separateItems()

          }
        }
      }
    )
  }
  separateItems() {
    const { previousArray, upcomingArray } = this.state
    exceedLimit = 0;
    this.state.separateItems.map((item) => {
      const dateLimit = moment(item.dateUtc, 'YYYY-MM-DDTHH:mm:ss.SSSZ');
      const now = moment()
      if (dateLimit.isValid() && now.isAfter(dateLimit)) {

        previousArray.push(item)

      } else {
        upcomingArray.push(item)
      }
    })
  }

  onPageSelected = (e) => {
    this.setState({
      pageIndex: e.nativeEvent.position,
    })
  }
}
const styles = StyleSheet.create({
  headerStyle: {
    flex: 0,
    backgroundColor: "#e5e5e5",





    justifyContent: "center",
  },
  headerInnerView: {
    height: 50,
    alignItems: "center",
    flexDirection: "row",
    alignContent: "center",
    paddingLeft: 20
  },

  homeTitle: {
    color: "#fff",
    fontSize: 16,
    fontFamily: "montserrant-semi-bold",
    fontWeight: "200",
    left: 20,
  },
  header: {
    fontSize: 12,
    fontFamily: "Roboto-Regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
  patientContainer: {
    backgroundColor: "#fff",
    borderRadius: 8,
    elevation: 2,
    alignContent: "center",
    alignItems: "center",
    width: (width - 50) / 3


  }, headingText: { letterSpacing: 0, color: "#000", marginTop: 15, fontSize: 14, marginTop: 20 }
})

export default MyAppointments