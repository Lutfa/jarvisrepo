import React, { Component } from "react"
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  FlatList,
  Image,
  Platform,
  Animated,
  StatusBar,
  StyleSheet,
  Modal,
  Alert,
  AsyncStorage,
  ScrollView,
  TextInput,

} from "react-native"
import LinearGradient from "react-native-linear-gradient"
import ImageLoad from "../BaseClass/ImageLoader"
import Constant from "../BaseClass/Constant"
import BaseStyles from "../BaseClass/BaseStyles"
import SplashScreen from "react-native-splash-screen"
import images from "../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import language from "../BaseClass/language"
const { width, height } = Dimensions.get("window")

import moment from "moment"
import BackButtonHeader from "../Profile/components/backButtonHeader"
var Realm = require("realm")

class AppointmentConfirmed extends Component {
  constructor(props) {
    super(props)

    this.state = {

    data:this.props.route.params.data
    }

    language.setLanguage("en")
  }

  async componentDidMount() {
    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
         // this.forceUpdate()
        }
      )


    SplashScreen.hide()

  }

  render() {
   const {data}=this.state
    return (
      <SafeAreaView  style={{backgroundColor:"#fff"}}>
        <ImageBackground style={{ height: "100%", width: "100%" }} source={images.bg}>
         
          {this.headerView()}


          <View
              style={{

                width: "100%",
                paddingTop: 8,


              }}>
             
                <View>
                  {this.appointmentui(data)}
                  
                </View>
                <LinearGradient
                    start={{ x: 0, y: 1 }}
                    end={{ x: 1, y: 1 }}

                    colors={[
                      "#FFFCFA",
                      "#FAFBFF",

                    ]}
                    style={{ marginTop:20, marginBottom: 10, borderRadius: 28, height: "70%", width: "100%" ,alignItems:"center",alignContent:"center",justifyContent:"center"}}>
                 
                 
                 <ImageBackground resizeMode="cover" style={{marginLeft:30, paddingHorizontal:10, height:120, width:"80%",}} source={require('../../assets/appointicon.png')}>

                 <Text style={{ marginTop:20,width:"70%", fontSize: 12, color: "#000", alignContent: "center" }}>Hi there! Your pharmacist will send you a health questionnaire shortly...</Text>

                 </ImageBackground>
                 <Image style={{height:160, width:"60%",marginHorizontal:30,alignContent:"center",alignItems:"center"}} source={require('../../assets/chatbg.png')}>

                 </Image>

                 <TouchableOpacity   onPress={() => {
             this.props.navigation.navigate("TabNavigationBar")
            }} style={{width:"80%", marginTop: 20}}>
                  
                  <LinearGradient
                    start={{ x: 0, y: 1 }}
                    end={{ x: 1, y: 1 }}

                    colors={[
                      Constant.gradientColor1(),
                      Constant.gradientColor2(),
  

                    ]}
                    style={{  marginTop: 20, borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: "100%" }}>
                    <Text style={{ fontSize: 12, color: "#000" }}>{language.gotoHome}</Text>
                  </LinearGradient>
                </TouchableOpacity>
                  </LinearGradient>

        
          
          
          </View>
        </ImageBackground>
        </SafeAreaView>
    )
  }
  headerView() {
    const { drawerOpen } = this.state

    const userData = global.userData

    return (
      <SafeAreaView
        
      >
        <StatusBar
          translucent
          barStyle="dark-content"
          //  backgroundColor="rgba(0, 0, 0, 0.251)"
          backgroundColor="transparent"

        />

        <BackButtonHeader navigation={this.props.navigation} titleHeader={"Appointment Confirmed"} />

      </SafeAreaView>
    )
  }
  appointmentui(data){
    return(
        <View>
          
            <View style={[BaseStyles.cornerRadiusView,{marginTop:30}]}>
            <Image style={{position:"absolute",bottom:0,borderBottomLeftRadius:28,borderBottomRightRadius:28, width:"100%"}} source={images.appt_bottom_curve}></Image>

                      <View style={{ flexDirection: "row", width: "100%" ,paddingLeft:20}}

                      >
 <Image
                  style={{
                    marginLeft:15,  height: 50, width: 50, borderRadius: 14,
                  }}
                  
                  source={{
                    uri: this.state.data.pharmacistPictureUrl,
                  }}
                 
                />
                <View>
                        <View style={{flexDirection:"row"}}>
                            <Text   style={[{ marginLeft:20, color:"#000",fontSize:16 }, BaseStyles.regularFont,]}> Appointment with</Text>
                          <Text 
                            style={[{ paddingLeft:5,color:Constant.appColor(),fontSize:16 }, BaseStyles.regularFont,]}>{ data.pharmacistName}</Text>
                         </View>
                          <View style={{ flexDirection: "row", marginTop: 15 }}

                          >
                            <View style={{ flexDirection: "row", marginLeft: 20 }}

                            ><Image style={{height:20,width:18}} source={images.calender}></Image>
                              <Text
                                style={[{ marginHorizontal: 10, }, BaseStyles.descFontblack,]}>{this.getDate()}</Text>

                            </View>
                            <View style={{ flexDirection: "row" }}

                            ><Image style={[BaseStyles.smallIconStyle, { marginLeft: 10 }]} source={images.schedule}></Image>
                              <Text
                                style={[{ marginHorizontal: 10, }, BaseStyles.descFontblack,]}>{data.slot}</Text>

                            </View>
                          </View>

                        </View>
                      </View>


                    </View>
        </View>
    )
}
getDate(){
  let date_moment = moment(this.state.data.dateUtc, "YYYY-MM-DDTHH:mm:ssZ");
  console.log(date_moment)
  let req_format = date_moment.format("DD MMM, yy");

return req_format;
}
}
const styles = StyleSheet.create({
  headerStyle: {
    flex: 0,

    backgroundColor: "#fff",




    justifyContent: "center",
  },
  headerInnerView: {
    height: 20,
    alignItems: "center",
    flexDirection: "row",
    alignContent: "center"
  },

  homeTitle: {
    color: "#fff",
    fontSize: 16,
    fontFamily: "montserrant-semi-bold",
    fontWeight: "200",
    left: 20,
  },
  header: {
    fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
  patientContainer: {
    backgroundColor: "#fff",
    borderRadius: 8,
    elevation: 2,
    alignContent: "center",
    alignItems: "center",
    width: (width - 50) / 3


  }, headingText: { letterSpacing: 0, color: "#000", marginTop: 30, fontSize: 16, marginTop: 20 }
})

export default AppointmentConfirmed