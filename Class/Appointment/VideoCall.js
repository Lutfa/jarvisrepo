import React, { Component } from 'react';
import {
  Dimensions,
  Image,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import RtcEngine, {
  RtcLocalView,
  RtcRemoteView,
  VideoRenderMode,
} from 'react-native-agora';

import { PermissionsAndroid } from 'react-native';

import styles from '../BaseClass/StyleSheet';
import Icon from 'react-native-vector-icons/FontAwesome5';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from "react-native-linear-gradient"

const {width, height} = Dimensions.get('window');

/**
 * @property peerIds Array for storing connected peers
 * @property appId
 * @property channelName Channel Name for the current session
 * @property joinSucceed State variable for storing success
 */


export default class VideoCall extends Component {
  _engine=RtcEngine;

  constructor(props) {
    super(props);
    this.state = {
      appId: 'f8c62367d35a44cab2185aca2aaba635',
      channelName: 'channel_jarvis',
      token:'006f8c62367d35a44cab2185aca2aaba635IADewJY8eOuBUIhqgGS1W6++j2IGthSE0xSPjDcFD7uvDYFl5a4AAAAAEACu7KwLiYLUYQEAAQCJgtRh',
      joinSucceed: false,
      peerIds: [6578889],
      toggleMic: true,
      toggleSoundType: true
    };
    if (Platform.OS === 'android') {
      // Request required permissions from Android
    this.  requestCameraAndAudioPermission().then(() => {
        console.log('requested!');
      });
    }
  }

  componentDidMount() {
    this.init();
  }

  /**
   * @name init
   * @description Function to initialize the Rtc Engine, attach event listeners and actions
   */
  init = async () => {
    const { appId } = this.state;
    this._engine = await RtcEngine.create(appId);
    await this._engine.enableVideo();

    this._engine.addListener('Warning', (warn) => {
      console.log('Warning', warn);
    });

    this._engine.addListener('Error', (err) => {
      console.log('Error', err);
    });

    this._engine.addListener('UserJoined', (uid, elapsed) => {
      console.log('UserJoined', uid, elapsed);
      // Get current peer IDs
      const { peerIds } = this.state;
      // If new user
      if (peerIds.indexOf(uid) === -1) {
        this.setState({
          // Add peer ID to state array
          peerIds: [...peerIds, uid],
        });
      }
    });

    this._engine.addListener('UserOffline', (uid, reason) => {
      console.log('UserOffline', uid, reason);
      const { peerIds } = this.state;
      this.setState({
        // Remove peer ID from state array
        peerIds: peerIds.filter((id) => id !== uid),
      });
    });

    // If Local user joins RTC channel
    this._engine.addListener('JoinChannelSuccess', (channel, uid, elapsed) => {
      console.log('JoinChannelSuccess', channel, uid, elapsed);
      // Set state variable to true
      this.setState({
        joinSucceed: true,
      });
    });
  };

  /**
   * @name startCall
   * @description Function to start the call
   */
  startCall = async () => {
    // Join Channel using null token and channel name
    await this._engine?.joinChannel(
      this.state.token,
      this.state.channelName,
      null,
      0
    );
  };

  /**
   * @name endCall
   * @description Function to end the call
   */
  endCall = async () => {
    await this._engine?.leaveChannel();
    this.setState({ peerIds: [], joinSucceed: false });
  };

  render() {
    return (
      <View style={styles.max}>
        <View style={styles.max}>
          {/* <View style={styles.buttonHolder}>
            <TouchableOpacity onPress={this.startCall} style={styles.button}>
              <Text style={styles.buttonText}> Start Call </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.endCall} style={styles.button}>
              <Text style={styles.buttonText}> End Call </Text>
            </TouchableOpacity>
          </View>*/}
          {this._renderVideos()}
          {this.headerVideoCall()}
          {this.infoCallLine()}
          {this.controlers()}
          <Image source={require('../../assets/frontCall.jpg')} style={{width: width, height: height/1.37, borderRadius: 15}}/>

          <View style={{position:'absolute', bottom: height/6, right: width/16, elevation: 10}}>
            <Image source={require('../../assets/womanCall.jpg')} style={{width: width/2.5, height: height/7, borderRadius: 15}}/>
          </View>
        </View>
        <StatusBar barStyle='light-content' translucent backgroundColor="transparent"/>
      </View>
    );
  }

  handleMicrophone() {
    this.setState({toggleMic: !this.state.toggleMic})
    // Put here the functionality of microphone control
  }

  handleSoundType() {
    this.setState({toggleSoundType: !this.state.toggleSoundType})
    // Put here the functionality of microphone control
  }

  infoCallLine() {
    return(
      <View style={{padding: 25, flexDirection:'row', alignItems:'center'}}>
        <Text style={{color:'white', fontSize:14, fontWeight: 'bold', marginRight: 15}}>Joshua Hysplop</Text>
        <Icon name="volume-up" size={14} color="#fff" style={{marginTop:2}}/>
      </View>
    );
  }
  headerVideoCall() {
    return(
      <View style={stylesHere.headerStyle}>
        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={stylesHere.goBackButton}>
          <Icon name="chevron-left" size={14} color="#fff" />
        </TouchableOpacity>
        
        <Text style={stylesHere.nameProfile}>Appointment Call</Text>

        <TouchableOpacity onPress={() => {}} style={[stylesHere.controlButtons, {marginLeft: width/4}]}>
          <MaterialIcon name="camera-retake" size={22} color="#fff" />
        </TouchableOpacity>
      </View>
      );
  }

  controlers() {
    return(
      <View style={{position:'absolute', bottom:0, padding: 30, flexDirection:'row'}}>
            {this.state.toggleSoundType ?
              <TouchableOpacity onPress={() => this.handleSoundType()} style={stylesHere.controlButtons}>
                <Icon name="phone-alt" size={18} color="#FFF" />
              </TouchableOpacity>
              :
              <TouchableOpacity onPress={() => this.handleSoundType()} style={stylesHere.controlButtons}>
                <Icon name="volume-up" size={18} color="#FFF" />
              </TouchableOpacity>
            }

            {this.state.toggleMic ? 
                <TouchableOpacity onPress={() => this.handleMicrophone()} style={[stylesHere.controlButtons, {marginLeft: 30}]}>
                  <Icon name="microphone" size={18} color="#FFF" />
                </TouchableOpacity>
                  :
                <TouchableOpacity onPress={() => this.handleMicrophone()} style={[stylesHere.controlButtons, {marginLeft: 30}]}>
                  <Icon name="microphone-slash" size={18} color="#FFF" />
                </TouchableOpacity>

            }

            <TouchableOpacity style={[stylesHere.controlButtons, {marginLeft: 30}]}>
              <Icon name="video" size={18} color="#FFF" />
            </TouchableOpacity>

            <TouchableOpacity style={{elevation: 20}}>
              <LinearGradient
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}

                colors={[
                  "#F12222",
                  "#B81818",

                ]}
                style={stylesHere.endCallButton}>
                <Text style={{ fontSize: 14, color: "#fff", fontWeight:'bold' }}>End Call</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
    );
  }

  _renderVideos = () => {
    const { joinSucceed } = this.state;
    return joinSucceed ? (
      <View style={styles.fullView}>
        <RtcLocalView.SurfaceView
          style={styles.max}
          channelId={this.state.channelName}
          renderMode={VideoRenderMode.Hidden}
        />
        {this._renderRemoteVideos()}
      </View>
    ) : null;
  };

  _renderRemoteVideos = () => {
    const { peerIds } = this.state;
    return (
      <ScrollView
        style={styles.remoteContainer}
        contentContainerStyle={{ paddingHorizontal: 2.5 }}
        horizontal={true}
      >
        {peerIds.map((value) => {
          return (
            <RtcRemoteView.SurfaceView
              style={styles.remote}
              uid={value}
              channelId={this.state.channelName}
              renderMode={VideoRenderMode.Hidden}
              zOrderMediaOverlay={true}
            />
          );
        })}
      </ScrollView>
    );
  };
 async requestCameraAndAudioPermission() {
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.CAMERA,
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      ]);
      if (
        granted['android.permission.RECORD_AUDIO'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
        granted['android.permission.CAMERA'] ===
          PermissionsAndroid.RESULTS.GRANTED
      ) {
        console.log('You can use the cameras & mic');
      } else {
        console.log('Permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }
}

const stylesHere = StyleSheet.create({
  controlButtons: {
    backgroundColor:'#434343', 
    elevation: 20,
    height: 45, 
    width: 45, 
    borderRadius: 40, 
    justifyContent:'center', 
    alignItems:'center'
  },
  endCallButton: {
    borderRadius: 40, 
    elevation: 20,
    height: 45, 
    alignContent: "center", 
    alignItems: "center", 
    justifyContent: "center", 
    width: width/4, 
    marginLeft: width/10
  },
  goBackButton: {
    marginLeft: width / 18, 
    width: width / 12, 
    height: 30, 
    justifyContent: 'center', 
    alignItems: 'center', 
    borderRadius: 50
  },
  nameProfile: {
    fontWeight: '500', 
    color: 'white',
    fontSize: 18, 
    marginLeft: 23
  },
  headerStyle: { 
    flexDirection: 'row', 
    alignItems: 'center',
    marginTop: height/16
  }
})

// import React, {useState} from 'react';
// import AgoraUIKit from 'agora-rn-uikit';
// import {
//     View,
//     Text,
   
//   } from "react-native"

// const VideoCall = () => {
//     console.log("video call")
//   const [videoCall, setVideoCall] = useState(true);
//   const rtcProps = {
//     appId: '088f1dcf1e914c10a9b17b8e32eeb974',
//     channel: 'test',
//     token:'0835841a3dd14984a8cf5f254950ce7b'
//   };
//   const callbacks = {
//     EndCall: () => setVideoCall(false),
//   };
//   return videoCall ? (
//     <AgoraUIKit rtcProps={rtcProps} callbacks={callbacks} />
//   ) : (
//     <Text onPress={()=>setVideoCall(true)}>Start Call</Text>
//   );
// };

// export default VideoCall;