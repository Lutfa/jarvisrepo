import React, { Component } from "react"
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  FlatList,
  Image,
  Platform,
  Animated,
  StatusBar,
  StyleSheet,
  Modal,
  Alert,
  AsyncStorage,
  ScrollView,
  TextInput,

} from "react-native"
import LinearGradient from "react-native-linear-gradient"

import Constant from "../BaseClass/Constant"
import BaseStyles from "../BaseClass/BaseStyles"
import SplashScreen from "react-native-splash-screen"
import images from "../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import language from "../BaseClass/language"
import urls from "../BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")
import Db from "../DB/Realm"
import ImageLoad from "../BaseClass/ImageLoader"
import moment from "moment"
import Toast from "react-native-simple-toast"
import axios from "axios"
import { Calendar } from "react-native-calendars"
import BackButtonHeader from "../Profile/components/backButtonHeader"
import { getBootloader } from "react-native-device-info"
var Realm = require("realm")

class BookAppointment extends Component {
  constructor(props) {
    super(props)

    this.state = {

      selectedItem: props.route.params.data,

      dateUtc: "",
      dateUtcLocal: "",
      isCategories: true,
      isBestSelling: true,
      selectedDay: {},
      selectedSlot: "",
      dateString: "",
      optionsArray: [

      ],
      selectedColor:"#F3F3F3"
    }

    language.setLanguage("en")
  }

  async componentDidMount() {
    this._unsubscribe =
      this.props.navigation.addListener(
        "focus",
        () => {
          this.forceUpdate()
        }
      )
    console.log("moment date" + moment().format("YYYY-MM-DD"))
    this.getSlots(moment().format("YYYY-MM-DD"))
    SplashScreen.hide()

  }

  render() {
    const { selectedDay, dateString, optionsArray, selectedSlot,selectColor } = this.state
    return (
      <SafeAreaView style={{ backgroundColor: "#fff" }}>
        <ImageBackground style={{ height: "100%", width: "100%" }} source={images.bg}>
          <View style={{ height: "100%", width: "100%" }}>
            
            {this.headerView()}
            <View style={{ marginTop: 30, backgroundColor: "#fff", borderTopLeftRadius: 28, borderTopRightRadius: 15, width: "100%", paddingTop: 20, paddingBottom: 45 }}>


              <ScrollView >
                <View style={{ paddingHorizontal: 20, paddingBottom: height / 12 }}>

                  <Text style={[styles.headingText], BaseStyles.boldFont}>Date on which you want appointment</Text>
                  <View style={{ marginTop: 15 }}>
                    <Calendar
                      style={{ backgroundColor: "#ECFFF7", borderRadius: 20, paddingBottom: 10 }}
                      enableSwipeMonths={true}

                      onDayPress={day => {
                        console.log('selected day ' + JSON.stringify(day));

                        console.log('selected dayname ' + this.getDayName(day.dateString));

                        this.getSlots(day.dateString)
                      }}
                      markedDates={{ dateString: { selected: true, marked: true, selectedColor: 'blue' }, }}
                      marking={true}


                    >



                    </Calendar>
                  </View>
                  <View style={{ marginTop: 35 }}>
                    <Text style={[styles.headingText], BaseStyles.boldFont}>Pick a time slot</Text>
                  </View>
                  <FlatList
                    style={{ marginTop: 10 }}
                    keyExtractor={item => item.slotTime}
                    data={this.getdata()}

                    numColumns={3}
                    renderItem={({ item, index }) => (
                      <TouchableOpacity

                        onPress={() => {
                          this.handleClick(item.slotTime)

                        }}

                        style={{ marginHorizontal: 7, marginVertical: 8, backgroundColor:selectedSlot == item.slotTime?"#000":"#F3F3F3", borderRadius: 44, height: 36, alignContent: "center", alignItems: "center", justifyContent: "space-evenly", width: "30%" }}
                      >
                        <Text style={{marginHorizontal:5, fontSize: 10, color:selectedSlot == item.slotTime?"#fff": "#000", alignContent: "center" }}>{item?.slotTime}</Text>



                      </TouchableOpacity>
                    )}
                  />

                  <View style={{ width: "100%", justifyContent: "center", alignContent: "center", alignItems: "center" }}>
                    {this.state.optionsArray.length == 0 && (<Text style={{ fontSize: 14, color: "#000", alignContent: "center" }}>No slots available!</Text>
                    )}
                  </View>

                </View>

                <View style={{  paddingHorizontal: 15,height:60, backgroundColor: "#fff", elevation: 12, width: "100%", flexDirection: "row", justifyContent: "space-evenly" }}>
              <LinearGradient
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}

                colors={[
                  "#000",
                  "#000",

                ]}
                style={{ marginTop: 5, marginBottom: 10, borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: width * 0.4 }}>
                <Text style={{ paddingHorizontal: 20, fontSize: 14, color: "#000", alignContent: "center" }}>BACK</Text>
              </LinearGradient>
              <TouchableOpacity
                onPress={() => {
                  this.serviceCall();

                }} >
                  
                <LinearGradient
                  start={{ x: 0, y: 1 }}
                  end={{ x: 1, y: 1 }}

                  colors={[
                    Constant.gradientColor1(),
                    Constant.gradientColor2(),

                  ]}
                  style={{ marginLeft: 30, marginTop: 5, marginBottom: 10, borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: width * 0.4 }}>
                  <Text style={{ fontSize: 14, color: "#000" }}>NEXT</Text>
                </LinearGradient>
               






                
              </TouchableOpacity>
            </View>


              </ScrollView>


            </View>
          </View>
        </ImageBackground>
      </SafeAreaView>
    )
  }
  getColor(slotTime) {
    const { selectedSlot } = this.state
    console.log("selectedSlot" + selectedSlot + " ")

    if (
      selectedSlot == slotTime) {
      return "#000"
    } else {
      return "#F3F3F3"
    }
    
  }
  handleClick(slotTime) {
    this.state.selectedSlot = slotTime;
    console.log("selectedSlot" + this.state.selectedSlot + " ")
this.forceUpdate()

  }
  getdata() {
    console.log("slots changed" + JSON.stringify(this.state.optionsArray))

    return this.state.optionsArray;

  }

  getSlots(dateString) {
    const { optionsArray } = this.state
    this.setState = { dateString: dateString }
    console.log("availability " + JSON.stringify(this.state.selectedItem.availability))

    this.state.selectedItem.availability.map((item, index) => {
      if (this.getDayName(dateString) == item.day) {
        this.state.optionsArray = item.slots;
        console.log("slots " + JSON.stringify(this.state.optionsArray))

      } else {
        this.state.optionsArray = []
        this.state.selectedSlot=""
      }
      this.state.selectedColor="#000000"
      this.forceUpdate()
    })
  }
  getDayName(dateString) {
    console.log(dateString)
    let date_moment = moment(dateString, "YYYY-MM-DD");
    console.log(date_moment)
    let req_format = date_moment.format("ddd");

    this.state.dateUtc = date_moment.utc().format("YYYY-MM-DDTHH:mm:ssZ"),
      this.state.dateUtcLocal = date_moment.format("YYYY-MM-DDTHH:mm:ssZ"),

      console.log(req_format)

    return req_format
  }
  headerView() {
    const { drawerOpen } = this.state

    const userData = global.userData

    return (
      <SafeAreaView

      >
        <StatusBar
          translucent
          barStyle="dark-content"
          //  backgroundColor="rgba(0, 0, 0, 0.251)"
          backgroundColor="transparent"

        />

        <BackButtonHeader navigation={this.props.navigation} titleHeader={"Book an Appointment"} />

      </SafeAreaView>
    )
  }

  serviceCall() {
    const { selectedDay, selectedSlot,selectedItem } = this.state
    const userData = global.userData;
    console.log(
      "userData",
      JSON.stringify(userData)
    )
    let params = {
      id: 0,
      pharmacyId: Constant.pharmacyId(),
      pharmacistId: selectedItem.id,
      pharmacistPictureId: selectedItem.pictureId,
      pharmacistPictureUrl:selectedItem.pictureUrl,
      pharmacistName: selectedItem.name,
      patientId: userData.id,
      patientPictureUrl: userData.pictureUrl,
      patientName: userData.dob,
      patientSex: userData.sex,
      dateUtc: this.state.dateUtc,
      localDateUtc: this.state.dateUtcLocal,
      slot: this.state.selectedSlot,
      createdOnUtc: moment().utc().format("YYYY-MM-DDTHH:mm:ssZ"),
      isPharmacistAttended: false,
      isPatientAttended: false,
      isCompleted: false,



    }
    console.log(
      "params",
      JSON.stringify(params)
    )

    Constant.postMethod(
      urls.upsertAppointment,
      params,
      (result) => {
        //this.setState({ isLoading: false })
       // this.loadingButton.showLoading(false)
        console.log(
          "servicecall",
          JSON.stringify(result)
        )
        if (result.success) {
          let response = result.result

          if (
            response != null &&
            response.status != "Fail"
          ) {
           

            this.props.navigation.navigate("AppointmentConfirmed", {
data:params
            })

         
          } else {
            Toast.show("Something went wrong")
          }
        } else {
        }
      }
    )


  }

   
  
   
  
}


const styles = StyleSheet.create({
  headerStyle: {
    flex: 0,

    backgroundColor: "#fff",




    justifyContent: "center",
  },
  headerInnerView: {
    height: 20,
    alignItems: "center",
    flexDirection: "row",
    alignContent: "center"
  },

  homeTitle: {
    color: "#fff",
    fontSize: 16,
    fontFamily: "montserrant-semi-bold",
    fontWeight: "200",
    left: 20,
  },
  header: {
    fontSize: 12,
    fontFamily: "TitilliumWeb-regular",
    lineHeight: 16,
    color: Constant.textColor(),
  },
  patientContainer: {
    backgroundColor: "#fff",
    borderRadius: 8,
    elevation: 2,
    alignContent: "center",
    alignItems: "center",
    width: (width - 50) / 3


  }, headingText: { letterSpacing: 0, color: "#000", marginTop: 30, fontSize: 16, marginTop: 20 }
})

export default BookAppointment