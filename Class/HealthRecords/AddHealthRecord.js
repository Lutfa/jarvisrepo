import React, { Component } from "react"
import {
  View,
  Keyboard,
  Modal,
  ImageBackground,
  Text,
  Animated,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  StatusBar,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import SplashScreen from "react-native-splash-screen"
import {
  FlatList,
  TextInput,
} from "react-native-gesture-handler"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import urls from "./../BaseClass/ServiceUrls"

const { width, height } = Dimensions.get("window")
import moment from "moment"
import PickImage from "./../BaseClass/PickImage"
import { KeyboardAvoidingScrollView } from "react-native-keyboard-avoiding-scroll-view"
import AnimateLoadingButton from "./../ExtraClass/LoadingButton"
import Toast from "react-native-simple-toast"
import axios from "axios"
import language from "./../BaseClass/language"
import {
  TextField,
  OutlinedTextField,
} from "react-native-material-textfield"
import CartDb from "./../DB/CartDb"
import BackButtonHeader from "../Profile/components/backButtonHeader";
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from "react-native-linear-gradient"

class AddHealthRecord extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: false,
      imagesArray: ["", "", "", "", ""],
      position: 0,
      showOptions: false,
      showBottom: false,
      optionsArray: [
        "Lab Report",
        "Prescription",
        "Diagnostic Record",
        "Others",
      ],
      previousImages: [],
      reportType:
        props.route.params != null
          ? props.route.params.data.HealthRecordType
          : "Select Report Type",
      imageIndex: 0,
      reportSummary:
        props.route.params != null
          ? props.route.params.data.Description
          : "",
      patient:
        props.route.params != null
          ? props.route.params.data
            .HealthRecordPatient
          : "",
      patientError: false,
      doctorError: false,
      id:
        props.route.params != null
          ? props.route.params.data.Id
          : 0,
      doctor:
        props.route.params != null
          ? props.route.params.data
            .HealthRecordDoctor
          : "",
      reportName:
        props.route.params != null
          ? props.route.params.data.Title
          : "",
      reportNameError: false,
      isuploadedImages: false,
      isEdit:
        props.route.params != null ? true : false,
      uploadImagesArray: [],
      HealthRecordImages: [],
      typeIndex: -1,
      uploadingText: "",
    }
  }

  componentDidMount() {
    SplashScreen.hide()

    if (this.props.route.params != null) {
      let oldImages = this.props.route.params.data
        .HealthRecordImages

      let NewDic = oldImages.filter((item) => {
        let dic = {
          PictureUrl: item.PictureUrl,
          PictureId: item.PictureId,
        }
        return dic
      })

      let newData = this.state.imagesArray.map(
        (item, index) => {
          return oldImages.length > index
            ? oldImages[index].PictureUrl
            : item
        }
      )

      let type = -1

      if (
        this.props.route.params.data
          .HealthRecordType == "Prescription"
      ) {
        type = 0
      } else if (
        this.props.route.params.data
          .HealthRecordType == "Diagnostic report"
      ) {
        type = 1
      } else {
        type = 2
      }

      this.setState({
        imagesArray: newData,
        HealthRecordImages: NewDic,
        typeIndex: type,
        previousImages: oldImages
      })
    }
  }
  
  
  render() {
    const {
      isLoading,
      typeIndex,
      uploadingText,
      reportType,
      showOptions,
      imagesArray,
      position,
      showBottom,
    } = this.state

    
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        <ImageBackground style={{ height: "100%", width: "100%", position: 'absolute' }} source={images.bg} />
        {isLoading
          ? Constant.showLoader(uploadingText)
          : null
        }

        <BackButtonHeader navigation={this.props.navigation} titleHeader={language.healthcarerecords} />

        <KeyboardAvoidingScrollView
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          stickyFooter={
            <TouchableOpacity
              style={{ marginBottom: 10 }}>
              <AnimateLoadingButton
                ref={(c) =>
                  (this.loadingButton = c)
                }
                width={width - 30}
                height={45}
                title={
                  this.state.isEdit
                    ? language.update.toUpperCase()
                    : language.save.toUpperCase()
                }
                titleFontSize={16}
                titleWeight={"100"}
                titleColor="#121212"
                backgroundColor={"#45f6e3"}
                borderRadius={25}
                onPress={() => {
                  this.serverCall()
                }}
              />
            </TouchableOpacity>
          }>
          <View style={{ flex: 1 }}>
            <View style={{ marginTop: 10 }}>
              <ScrollView
                horizontal={true}
                pagingEnabled={true}
                scrollEventThrottle={16}
                showsHorizontalScrollIndicator={
                  false
                }
                onMomentumScrollEnd={
                  this._onMomentumScrollEnd
                }>
                {imagesArray.map((url, i) => {
                  // the _ just means we won't use that parameter
                  return (
                    <TouchableOpacity
                      activeOpacity={2}
                      onPress={() => {
                        this.setState({
                          showOptions: true,
                          imageIndex: i,
                        })
                      }}
                      style={[styles.uploadView]}>
                      {url != null && url != "" ? (
                        <Image
                          style={{
                            height: "100%",
                            width: "100%",
                          }}
                          source={{ uri: url }}
                        />
                      ) : (
                        <View
                          style={{
                            alignItems: "center",
                            justifyContent: "center",
                            maxWidth: width / 2
                          }}>
                          <MaterialIcon name="camera-plus-outline" size={35} color="#C0C0C0" />
                          <Text
                            style={[
                              {
                                fontSize: 14,
                                textAlign: "center",
                                marginTop: 10,
                                color: "#121212",
                              },
                              BaseStyle.regularFont,
                            ]}>
                            {language.uploadreport.toLocaleUpperCase()}
                          </Text>
                        </View>
                      )}
                    </TouchableOpacity>
                  )
                })}
              </ScrollView>

              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  position: "absolute",
                  bottom: 5,
                  left: 0,
                  right: 0,
                }} // this will layout our dots horizontally (row) instead of vertically (column)
              >
                <View
                  style={{ flexDirection: "row" }}>
                  {imagesArray.map((_, i) => {
                    // the _ just means we won't use that parameter

                    return (
                      <Animated.View
                        key={i}
                        style={{
                          height: 6,
                          width:
                            i == position ? 25 : 15,
                          backgroundColor:
                            i == position
                              ? '#8050FF'
                              : "#EBEAF2",
                          margin: 4,
                          borderRadius: 5,
                        }}
                      />
                    )
                  })}
                </View>
              </View>
            </View>

            <View
              style={{
                paddingTop: 0,
                paddingRight: 8,
                marginRight: 10,
              }}>

              <View style={{ backgroundColor: '#fff', width: width, paddingLeft: 20, borderRadius: 30, marginVertical: 30 }}>
                <Text
                  style={[
                    {
                      fontSize: 18,
                      marginTop: 15,
                      fontWeight: 700,
                      color: "#FF0055",
                    },
                    BaseStyle.regularFont,
                  ]}>
                  {language.selectreporttype}
                </Text>

                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 18,
                    marginBottom: 18,
                    height: 100,
                    width: "100%",
                    justifyContent: "space-between",
                    paddingLeft: 0,
                    paddingRight: 20,
                  }}>
                  <TouchableOpacity
                    activeOpacity={2}
                    onPress={() => {
                      this.setState({
                        typeIndex: 0,
                        reportType: "Prescription",
                      })
                    }}
                    style={typeIndex === 0 ? styles.reportTypeSelected : styles.reportType}>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 1 }}
                      colors={[
                        "#E9FFE5",
                        "#C7FCD9",
                        "#D3D2FF"

                      ]}
                      style={styles.itemImage}
                    >
                      <MaterialIcon name="file-document-edit" size={25} color="#46ad35" />
                    </LinearGradient>

                    <Text
                      style={[typeIndex === 0 ? styles.textReportTypeSelected : styles.textReportType, BaseStyle.regularFont]}>
                      {language.perscription}
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    activeOpacity={2}
                    onPress={() => {
                      this.setState({
                        typeIndex: 1,
                        reportType:
                          "Diagnostic report",
                      })
                    }}
                    style={typeIndex === 1 ? styles.reportTypeSelected : styles.reportType}>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 1 }}
                      colors={[
                        "#E9FFE5",
                        "#C7FCD9",
                        "#D3D2FF"

                      ]}
                      style={styles.itemImage}
                    >
                      <MaterialIcon name="file-chart-outline" size={25} color="#46ad35" />
                    </LinearGradient>

                    <Text
                      style={[typeIndex === 1 ? styles.textReportTypeSelected : styles.textReportType, BaseStyle.regularFont]}>
                      {language.diagnosticReport}
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    activeOpacity={2}
                    onPress={() => {
                      this.setState({
                        typeIndex: 2,
                        reportType: "Medical claims",
                      })
                    }}
                    style={typeIndex === 2 ? styles.reportTypeSelected : styles.reportType}>
                    <LinearGradient
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 1 }}
                      colors={[
                        "#E9FFE5",
                        "#C7FCD9",
                        "#D3D2FF"

                      ]}
                      style={styles.itemImage}
                    >
                      <MaterialIcon name="file-alert-outline" size={25} color="#46ad35" />
                    </LinearGradient>

                    <Text
                      style={[typeIndex === 2 ? styles.textReportTypeSelected : styles.textReportType, BaseStyle.regularFont]}>
                      {language.medicalClaims}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={{paddingHorizontal: width/18, backgroundColor: "#fff", borderRadius: 30}}>
                <TextField
                  label={language.patientName}
                  value={this.state.patient}
                  lineWidth={1}
                  fontSize={14}
                  labelFontSize={13}
                  labelTextStyle={[
                    { paddingTop: 5 },
                    BaseStyle.regularFont,
                  ]}
                  baseColor={Constant.textColor()}
                  tintColor={Constant.textColor()}
                  textColor={"#000"}
                  onChangeText={(text) =>
                    this.setState({
                      patient: text,
                      patientError: "",
                    })
                  }
                  error={this.state.patientError}
                />

                <TextField
                  label={language.doctorname}
                  value={this.state.doctor}
                  lineWidth={1}
                  fontSize={14}
                  labelFontSize={13}
                  labelTextStyle={[
                    { paddingTop: 5 },
                    BaseStyle.regularFont,
                  ]}
                  baseColor={Constant.textColor()}
                  tintColor={Constant.textColor()}
                  textColor={"#000"}
                  onChangeText={(text) =>
                    this.setState({
                      doctor: text,
                      doctorError: "",
                    })
                  }
                  error={this.state.doctorError}
                />

                {/* <TouchableOpacity
        onPress = {()=>{
          this.setState({showBottom:true})
        }}
         style = {{height:60,marginBottom:-10, marginTop:30,width:'100%',borderBottomColor:'#d3d3d3',borderBottomWidth:1}}>
        <Text style = {[{fontSize:12,marginTop:2,color:Constant.appColorAlpha()},BaseStyle.regularFont]}>UPLOAD REPORT</Text>
       
       <View style = {{paddingTop:10,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
        <Text style = {[{fontSize:14,marginLeft:25,flex:1},BaseStyle.regularFont]}>{reportType}</Text>
        <Image style = {{width:15,height:15}} source = {images.downArrow}/>
        </View>
        </TouchableOpacity> */}

                <TextField
                  label={language.reportname}
                  value={this.state.reportName}
                  lineWidth={1}
                  fontSize={14}
                  labelFontSize={13}
                  labelTextStyle={[
                    { paddingTop: 5 },
                    BaseStyle.regularFont,
                  ]}
                  baseColor={Constant.textColor()}
                  tintColor={Constant.textColor()}
                  textColor={"#000"}
                  onChangeText={(text) =>
                    this.setState({
                      reportName: text,
                      reportNameError: "",
                    })
                  }
                  error={this.state.reportNameError}
                />

                <View style={{ height: 5 }} />

                {/* <TextField
          inputStyle = {[{height:50,color:'#000',fontSize:14}]}
          label={language.reportname.toUpperCase()}
         textFocusColor={this.state.reportNameError ? 'red' : '#c0c0c0'}
         value={this.state.reportName}
          highlightColor={this.state.reportNameError ? 'red' : Constant.appColorAlpha()}
          onChangeText={(text)=>{
            this.setState({reportName:text,reportNameError:false})
          }}
          onFocus={()=>{
            // this.bottomSheet[1].snapTo(height/2)

          }}
        /> */}

                <View style={{ marginTop: 23 }}>
                  <Text
                    style={[
                      {
                        fontSize: 12,
                        marginTop: 2,
                        color: Constant.textColor(),
                      },
                      BaseStyle.regularFont,
                    ]}>
                    {language.reportsummary}
                  </Text>

                  <TextInput
                    multiline
                    onChangeText={(text) => {
                      this.setState({
                        reportSummary: text,
                      })
                    }}
                    value={this.state.reportSummary}
                    style={[
                      {
                        height: 100,
                        marginTop: 5,
                        width: "100%",
                        padding:10,
                        borderRadius: 10,
                        backgroundColor: "#F2F7F4",
                      },
                      BaseStyle.regularFont,
                    ]}
                    placeholder={
                      language.reportsummary
                    }
                  />
                </View>
              </View>
              <View
                style={{
                  height: 20,
                  width: 300,
                }}></View>
            </View>
          </View>
        </KeyboardAvoidingScrollView>

        {showOptions ? (
          <PickImage
            selectedImage={this.selectedImage}
            onBack={this.hidePicker}
          />
        ) : null}

        {showBottom ? this.showBottomSheet() : null}
        <StatusBar barStyle="dark-content" translucent backgroundColor="transparent" />
      </SafeAreaView>
    )
  }

  serverCall() {
    console.log("healthRecordImages" + JSON.stringify(this.state.HealthRecordImages))
    if (this.validate()) {
      Keyboard.dismiss()

      if (this.state.isuploadedImages) {
        let oldRecords = this.state.HealthRecordImages.filter(
          (item) => {
            return item != ""
          }
        )

        let filterArray = this.state.imagesArray.filter(
          (item) => {
            return (
              item != "" && !item.includes("http")
            )
          }
        )

        this.setState(
          {
            uploadImagesArray: filterArray,
            isLoading: true,
            HealthRecordImages: oldRecords,
          },
          () => {
            if (
              this.state.uploadImagesArray.length >
              0
            ) {
              // console.log(this.state.uploadImagesArray)

              this.uploadImage(
                this.state.uploadImagesArray[0],
                0
              )
            }
          }
        )
      } else {
        const data = Array.from(
          this.state.HealthRecordImages,
          (item) => ({ ...item })
        )
        console.log("data healthRecordImages" + JSON.stringify(data))


        this.setState(
          {
            // HealthRecordImages: data,
            HealthRecordImages: this.state.HealthRecordImages,
          },
          () => {
            this.createRecord()
          }
        )
      }
    }
  }

  createRecord() {
    this.setState({
      uploadingText: "",
      isLoading: true,
    })

    const {
      HealthRecordImages,
      patient,
      doctor,
      reportType,
      reportName,
      reportSummary,
      isEdit,
      id,
    } = this.state

    // props.route.params != null
    //       ? props.route.params.data.HealthRecordType
    //console.log("pre Images",JSON.stringify(previousImages))
    //console.log("pre Images this",JSON.stringify(this.previousImages))

    // HealthRecordImages?HealthRecordImages!=null:previousImages
    let params = {
      Id: id,
      pharmacyId: Constant.pharmacyId(),
      patientId: global.CustomerId,
      Description: reportSummary,
      HealthRecordDoctor: doctor,
      HealthRecordPatient: patient,
      HealthRecordType: reportType,
      Title: reportName,
      HealthRecordImages: HealthRecordImages,
      lastModifiedDate: moment().utc().format("YYYY-MM-DDTHH:mm:ssZ"),
      createdOnUtc: moment().utc().format("YYYY-MM-DDTHH:mm:ssZ"),
    }

    console.log("healthRecord params" + JSON.stringify(params))

    if (isEdit) {
      Constant.postMethod(
        urls.updateHealthRecord,
        params,
        (result) => {
          console.log("update res" + JSON.stringify(result))
          this.setState({
            uploadingText: "",
            isLoading: false,
          })

          if (result.success) {
            if (result.result != null) {
              CartDb.addHealthRecordsData(
                result.result
              )
              Toast.show(
                "Health Record updated successfully"
              )
              this.props.navigation.pop(1)
            }
          }
        }
      )
    } else {
      Constant.postMethod(
        urls.addHealthRecord,
        params,
        (result) => {
          this.setState({
            uploadingText: "",
            isLoading: false,
          })

          if (result.success) {
            if (result.result != null) {
              CartDb.addHealthRecordsData(
                result.result
              )
              Toast.show(
                "Health Record added successfully"
              )
              this.props.navigation.pop(1)
            }
          }
        }
      )
    }
  }

  validate() {
    const {
      patient,
      patientError,
      doctor,
      doctorError,
      reportName,
      reportNameError,
      isuploadedImages,
      isEdit,
      reportType,
    } = this.state
    let returnValue = true
    if (patient == "") {
      this.setState({
        patientError: "Please enter patient name",
      })
      returnValue = false
    }

    if (doctor == "") {
      this.setState({
        doctorError: "Please enter doctor name",
      })
      returnValue = false
    }

    if (reportName == "") {
      this.setState({
        reportNameError: "Please enter report name",
      })
      returnValue = false
    }

    if (
      reportType == "" ||
      reportType == "Select Report Type"
    ) {
      Toast.show("Please select report type")
      returnValue = false
    }

    if (!isuploadedImages && !isEdit) {
      Toast.show("Please select report")
      returnValue = false
    }

    return returnValue
  }

  showBottomSheet() {
    const { optionsArray } = this.state
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={true}
        onRequestClose={() => {
          this.setState({ showBottom: false })
        }}>
        <TouchableOpacity
          activeOpacity={2}
          onPress={() => {
            this.setState({ showBottom: false })
          }}
          style={{
            flex: 1,
            backgroundColor: "rgba(0,0,0,0.5)",
            position: "absolute",
            bottom: 0,
            top: 0,
            left: 0,
            right: 0,
          }}
        />
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "flex-end",
          }}>
          <View
            style={{
              height: null,
              width: "100%",
              backgroundColor: "#fff",
              padding: 15,
            }}>
            {/* <Text
        onPress={()=>{
          alert(name)
        }}
         style = {[{color:Constant.appColorAlpha(),height:30, fontSize:14},BaseStyle.mediumFont]}>Select Report Type</Text> */}

            {optionsArray.map((name, i) => {
              // the _ just means we won't use that parameter
              return (
                <Text
                  onPress={() => {
                    this.setState({
                      reportType: name,
                      showBottom: false,
                    })
                  }}
                  style={[
                    { height: 40, fontSize: 14 },
                    BaseStyle.regularFont,
                  ]}>
                  {name}
                </Text>
              )
            })}
          </View>
        </View>
      </Modal>
    )
  }

  hidePicker = () => {
    this.setState({ showOptions: false })
  }
  selectedImage = (url) => {
    const {
      imageIndex,
      imagesArray,
      HealthRecordImages,
    } = this.state
    let images = imagesArray.map((item, index) => {
      return index == imageIndex ? url : item
    })

    if (HealthRecordImages.length > imageIndex) {
      let newHelthRecords = HealthRecordImages.map(
        (item, index) => {
          return index == imageIndex ? "" : item
        }
      )
      this.setState({
        HealthRecordImages: newHelthRecords,
      })
    }

    this.setState({
      showOptions: false,
      imagesArray: images,
      isuploadedImages: true,
    })
  }

  uploadImage(url, count) {
    this.setState({
      uploadingText: `Uploading records ${count + 1
        }/${this.state.uploadImagesArray.length}`,
    })
    Constant.uploadImageToServer(
      urls.mediaUpload + Constant.pharmacyId(),
      url,
      (result) => {
        if (result.success) {
          console.log("image res " + JSON.stringify(result))
          let dic = {
            pictureUrl: result.url.response.pictureUrl,
            pictureId:
              result.url.response.pictureId,
          }

          this.setState(
            {
              HealthRecordImages: [
                ...this.state.HealthRecordImages,
                dic,
              ],
            },
            () => {
              if (
                this.state.uploadImagesArray
                  .length >
                count + 1
              ) {
                this.uploadImage(
                  this.state.uploadImagesArray[
                  count + 1
                  ],
                  count + 1
                )
              } else {
                this.createRecord()
              }
            }
          )
        }
        this.setState({ showLoader: false })
        return
      }
    )
  }
  // var photo = {
  //   uri:url,
  //   type: 'image/jpeg',
  //   name: 'photo.jpg',
  // };

  // const formData = new FormData();
  // formData.append('file', photo);

  // axios({
  //     method: 'post',
  //     headers: {
  //         'Content-Type': 'multipart/form-data',
  //     },
  //     data: formData,
  //     url: urls.mediaUpload,
  //     onUploadProgress: (ev: ProgressEvent) => {
  //         const progress = ev.loaded / ev.total * 100;
  //         console.log(Math.round(progress))
  //     },
  // })
  // .then((resp) => {
  //     // our mocked response will always return true
  //     // in practice, you would want to use the actual response object
  //     alert(JSON.stringify(resp))
  // })
  // .catch((err) => {
  //   alert(JSON.stringify(err))
  // });


  _onMomentumScrollEnd = ({ nativeEvent }: any) => {
    const index = Math.round(
      nativeEvent.contentOffset.x / width
    )

    if (index !== this.state.position) {
      this.setState({ position: index })
    }
  }
}

const styles = StyleSheet.create({
  mainView: {
    marginTop: 10,
    marginBottom: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  reportType: {
    flex: 0.28,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#F3F2EE",
    borderRadius: 15,
  },
  textReportType: {
    color: "#121212",
    fontSize: 12,
    marginTop: 5,
    maxWidth: width / 6,
    textAlign: 'center'
  },
  textReportTypeSelected: {
    color: "#fff",
    fontSize: 12,
    marginTop: 5,
    maxWidth: width / 6,
    textAlign: 'center'
  },
  reportTypeSelected: {
    flex: 0.28,
    alignItems: "center",
    justifyContent: "center",
    elevation: 10,
    backgroundColor: "#8050FF",
    borderRadius: 15,
  },
  backView: {
    marginLeft: 15,
    height: 35,
    width: 35,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.4)",
  },
  backBtn: { height: 20, width: 20, left: 0 },
  uploadView: {
    marginTop: 0,
    height: 300,
    width: width,
    backgroundColor: "#fff",
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
  },
  itemImage: {
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    width: 40,
    borderRadius: 40,
  },
})

export default AddHealthRecord
