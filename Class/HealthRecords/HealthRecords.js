import React, { Component } from "react"
import {
  View,
  Text,
  ScrollView,
  Image,
  AsyncStorage,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import SplashScreen from "react-native-splash-screen"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import urls from "./../BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")
import moment from "moment"
import language from "./../BaseClass/language"
import CartDb from "./../DB/CartDb"
import Db from "./../DB/Realm"
import { isBatteryChargingSync } from "react-native-device-info"

class HealthRecords extends Component {
  constructor(props) {
    super(props)
    this.state = {
      recordsArray: [],
      isLoading: false,
      isGoBack:
        props.route.params != null
          ? props.route.params.isGoBack
          : false,
    }
  }

  componentDidMount() {
    SplashScreen.hide()
    this._unsubscribe = this.props.navigation.addListener(
      "focus",
      () => {
        let recordsData = Db.objects(
          "HealthRecord"
        ).sorted("Id", true)
        this.setState({ recordsArray: recordsData })
      }
    )

    AsyncStorage.getItem(
      "HealthRecordsTimeTicks"
    ).then((asyncStorageRes) => {
      if (
        asyncStorageRes != null &&
        asyncStorageRes != ""
      ) {
        this.getRecords(asyncStorageRes)
      } else {
        this.getRecords("0")
      }
    })
  }
  render() {
    const { isLoading, AddressArray } = this.state

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        {isLoading ? Constant.showLoader() : null}

        <View
          style={{
            backgroundColor: Constant.headerColor(),
            elevation: 4,
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 15,
              backgroundColor: Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack()
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFontwhite,
              ]}>
              {language.healthrecords.toUpperCase()}
            </Text>
          </View>
        </View>

        <FlatList
          style={{ marginTop: 10 }}
          data={this.state.recordsArray}
          keyExtractor={(item) => item.Id}
          extraData={this.state}
          renderItem={({ item, index }) => (
            
          

            <View style={styles.item}>
              <TouchableOpacity
                activeOpacity={2}
                onPress={() => {
                  if (this.state.isGoBack) {
                    this.props.navigation.goBack()
                    this.props.route.params.onSelect(
                      item
                    )
                  } else {
                    this.props.navigation.navigate(
                      "HealthRecordDetails",
                      { selectedItem: item }
                    )
                  }
                }}
                style={styles.itemInner}>
                <View style={styles.itemTopView}>
                  <View style={styles.itemImage}>
                   
                    <Image
                      resizeMode={"contain"}
                      style={{
                        width: 24,
                        height: 24,
                      }}

                      source={this.getTypeImage(item.HealthRecordType) }
                    />
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text
                      style={[
                        styles.itemMainText,
                        BaseStyle.boldFont,
                      ]}>
                      {item.Title}
                    </Text>
                    <Text
                      style={[
                        styles.itemSubtext,
                        BaseStyle.regularFont,
                      ]}>
                      {item.HealthRecordType} |{" "}
                      {moment(
                        !!item.CreatedOn
                          ? item.CreatedOn
                          : item.LastModifiedDate
                      ).format("DD/MM/YY")}{" "}
                      |{" "}
                      {moment(
                        !!item.CreatedOn
                          ? item.CreatedOn
                          : item.LastModifiedDate
                      ).format("HH:mm a")}
                    </Text>

                    <View
                      style={styles.itemBottomView}>
                      <View
                        style={
                          styles.itemBottomSubview
                        }>
                        <Text
                         numberOfLines={2}
                         ellipsizeMode="tail"
                          style={[
                            styles.patientName,
                            BaseStyle.regularFont,
                          ]}>
                          Patient Name :
                          <Text
                         
                            style={[
                              styles.patientName,

                              {
                                color: Constant.appColorAlpha(),
                                
                               
                              },
                              BaseStyle.regularFont,
                            ]}>{` ${item.HealthRecordPatient}`}</Text>
                        </Text>
                      </View>

                      <View
                        style={
                          styles.itemBottomSubview
                        }>
                        <Text
                          numberOfLines={2}
                          ellipsizeMode="tail"
                          style={[
                            styles.patientName,
                            BaseStyle.regularFonts,
                          ]}>
                          Doctor Name :
                          <Text
                         
                            style={[
                              styles.patientName,
                              {
                                color: Constant.appColorAlpha(),
                              },
                              BaseStyle.regularFont,
                            ]}>{` ${item.HealthRecordDoctor}`}</Text>
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}
          ListEmptyComponent={Constant.noDataView(
            isLoading
          )}
        />

        <View
          style={{
            position: "absolute",
            right: 30,
            bottom: 30,
          }}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate(
                "AddHealthRecord"
              )
            }}
            style={styles.addBtn}>
            <Image
              tintColor={Constant.appColorAlpha()}
              style={styles.addImage}
              source={images.plus}
            />
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    )
  }
  getTypeImage(type){
    var image;
    if(type=="Prescription"){
      image=images.uReport
    }else if(type=="Diagnostic Report"){
      image=images.uploadPrescription
    }else{
      image=images.uClaims

    }
    return image
  }
  getRecords(time) {
    this.setState({
      isLoading: time == "0" ? true : false,
    })
    let params = {
      pharmacyId:Constant.pharmacyId(),
      patientId: global.CustomerId,
      lastUpdatedTimeTicks: time,
    }
    Constant.postMethod(
      urls.getAllHealthRecords,
      params,
      (result) => {
        this.setState({ isLoading: false })

        if (result.success) {
          if (result.result.response != null) {
            // this.setState({recordsArray:result.result.Response.HealthRecords})

            result.result.response.healthRecords.map(
              (item) => {
                CartDb.addHealthRecordsData(item)
              }
            )
          } else {
            //    Toast.show(result.result.Response.Message)
          }
        } else {
          //   this.setState({isNumberCorrect:false})
        }
        AsyncStorage.setItem(
          "HealthRecordsTimeTicks",
          JSON.stringify(
            result.result
              .lastUpdatedTimeTicks
          )
        )
        let recordsData = Db.objects(
          "HealthRecord"
        ).sorted("Id", true)
        this.setState({ recordsArray: recordsData })
      }
    )
  }
}

const styles = StyleSheet.create({
  mainView: {
    marginTop: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  backView: {
    marginLeft: 15,
    height: 35,
    width: 35,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.4)",
  },
  backBtn: { height: 20, width: 20, left: 0 },
  item: {
    width: "100%",
    padding: 10,
    backgroundColor: "#fff",
  },
  itemInner: {
    elevation: 2,
    backgroundColor: "#fff",
    padding: 8,
    borderRadius: 5,
  },
  itemTopView: {
    flexDirection: "row",
    marginBottom: 5,
  },
  itemImage: {
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    elevation: 4,
    width: 40,
    backgroundColor: Constant.appLightColor(),
    borderRadius: 6,
  },
  itemMainText: {
    fontSize: 16,
    flex: 1,
    marginLeft: 10,
    color: "#000",
  },
  itemline: {
    height: 1,
    width: "100%",
    backgroundColor: "rgba(60,111,251,0.4)",
    marginTop: 6,
    marginBottom: 10,
  },
  itemSubtext: {
    fontSize: 11,
    flex: 1,
    marginLeft: 10,
    marginTop: 4,
    color: Constant.textAlpha(),
  },
  itemBottomView: {
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    height: 34,
    width: "90%",
  },
  itemBottomSubview: {
    flex: 0.46,
    marginLeft: 10,
  },
  patientName: {
    fontSize: 11,
    color: Constant.textAlpha(),
  },
  addBtn: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    width: 50,
    borderRadius: 25,
    elevation: 5,
    backgroundColor: "#fff",
  },
  addImage: {
    borderRadius: 20,
    width: 25,
    height: 25,
  },
})

export default HealthRecords
