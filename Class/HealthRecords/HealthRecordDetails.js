import React, { Component } from "react"
import {
  View,
  Modal,
  Alert,
  ImageBackground,
  Text,
  Animated,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import SplashScreen from "react-native-splash-screen"
import {
  FlatList,
  TextInput,
} from "react-native-gesture-handler"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import urls from "./../BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")
import moment from "moment"
import PickImage from "./../BaseClass/PickImage"
import TextField from "./../Login/lib/TextField"
import { KeyboardAvoidingScrollView } from "react-native-keyboard-avoiding-scroll-view"
import AnimateLoadingButton from "./../ExtraClass/LoadingButton"
import Toast from "react-native-simple-toast"
import axios from "axios"
import ImageLoad from "./../BaseClass/ImageLoader"
import ImageViewer from "react-native-image-zoom-viewer"
import language from "./../BaseClass/language"
import Db from "./../DB/Realm"
import CartDb from "./../DB/CartDb"
class HealthRecordDetails extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: false,
      imagesArray:
        props.route.params.selectedItem
          .HealthRecordImages,
      position: 0,
      selectedItem: props.route.params.selectedItem,
      showImages: [],
      isShow: false,
    }
  }

  componentDidMount() {
    SplashScreen.hide()

    if (this.props.route.params != null) {
      this._unsubscribe = this.props.navigation.addListener(
        "focus",
        () => {
          let selected = Db.objectForPrimaryKey(
            "HealthRecord",
            this.props.route.params.selectedItem.Id
          )
          this.setState({ recordsArray: selected })
        }
      )
    }
  }
  render() {
    const {
      selectedItem,
      isShow,
      imagesArray,
      position,
      isLoading,
    } = this.state

    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: "#fff",
        }}>
        {isLoading ? Constant.showLoader() : null}

        <View
          style={{
            backgroundColor: Constant.headerColor(),
            elevation: 4,
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 15,
              backgroundColor: Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.pop(1)
              }}
              style={{
                marginLeft: 15,
                height: 35,
                borderRadius: 15,
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image
                tintColor="#fff"
                style={{
                  height: 20,
                  width: 20,
                  left: 0,
                }}
                source={images.back}
              />
            </TouchableOpacity>
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              {language.healthrecords.toUpperCase()}
            </Text>

            <TouchableOpacity
              onPress={() => {
                this.deletePopUP(selectedItem.Id)
              }}
              style={{
                marginRight: 20,
                height: 25,
                width: 25,
              }}>
              <Image
                tintColor={"#fff"}
                style={styles.backBtn}
                source={images.delete}

              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate(
                  "AddHealthRecord",
                  { data: selectedItem }
                )
              }}
              style={{
                marginRight: 20,
                height: 25,
                width: 25,
              }}>
              <Image
                tintColor={"#fff"}
                style={styles.backBtn}
                source={images.edit}
              />
            </TouchableOpacity>
          </View>
        </View>

        <ScrollView>
          <View style={{ flex: 1 }}>
            <View>
              <ScrollView
                horizontal={true}
                pagingEnabled={true}
                scrollEventThrottle={16}
                showsHorizontalScrollIndicator={
                  false
                }
                onMomentumScrollEnd={
                  this._onMomentumScrollEnd
                }>
                {imagesArray.map((item, i) => {
                  // the _ just means we won't use that parameter
                  return (
                    <TouchableOpacity
                      activeOpacity={2}
                      onPress={() => {
                        const images = [
                          { url: item.PictureUrl },
                        ]
                        this.setState({
                          showImages: images,
                          imageIndex: i,
                          isShow: true,
                        })
                      }}
                      style={[styles.uploadView]}>
                      {item != null &&
                      item != "" ? (
                        //  <Image style = {{height:'100%',width:'100%'}} source={{uri:item.PictureUrl}}/>
                        <View
                          style={{
                            backgroundColor: "#fff",
                            width: "100%",
                            height: "100%",
                            elevation: 5,
                            padding: 0,
                            overflow: "hidden",
                            borderRadius: 8,
                          }}>
                          <ImageLoad
                            style={{
                              width: "100%",
                              height: "100%",
                            }}
                            loadingStyle={{
                              size: "large",
                              color: "blue",
                            }}
                            source={{
                              uri: item.PictureUrl,
                            }}
                          />
                        </View>
                      ) : null}
                    </TouchableOpacity>
                  )
                })}
              </ScrollView>

              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  position: "absolute",
                  bottom: 25,
                  left: 0,
                  right: 0,
                }} // this will layout our dots horizontally (row) instead of vertically (column)
              >
                <View
                  style={{ flexDirection: "row" }}>
                  {imagesArray.map((_, i) => {
                    // the _ just means we won't use that parameter

                    return (
                      <Animated.View
                        key={i}
                        style={{
                          height: 6,
                          width:
                            i == position ? 25 : 15,
                          backgroundColor:
                            i == position
                              ? Constant.appColorAlpha()
                              : Constant.appLightColor(),
                          margin: 4,
                          borderRadius: 5,
                        }}
                      />
                    )
                  })}
                </View>
              </View>
            </View>

            <View
              style={{
                width: "100%",
                padding: 15,
                flexDirection: "row",
              }}>
              <View style={styles.itemImage}>
                <Image
                  style={{ width: 24, height: 24 }}
                  source={images.blood}
                />
              </View>
              <View style={{ flex: 1 }}>
                <Text
                  style={[
                    styles.itemMainText,
                    BaseStyle.boldFont,
                  ]}>
                  {selectedItem.Title}
                </Text>
                <Text
                  style={[
                    styles.itemSubtext,
                    BaseStyle.regularFont,
                  ]}>
                  {selectedItem.HealthRecordType}
                </Text>

                <View style={styles.itemBottomView}>
                  <View
                    style={
                      styles.itemBottomSubview
                    }>
                    <Image
                      tintColor={Constant.appColorAlpha()}
                      style={{
                        marginRight: 5,
                        width: 16,
                        height: 16,
                      }}
                      source={images.calendar}
                    />

                    <Text
                      style={[
                        styles.patientName,
                        {
                          color: Constant.appColorAlpha(),
                        },
                        BaseStyle.regularFont,
                      ]}>
                      {moment(
                        !!selectedItem.CreatedOn
                          ? selectedItem.CreatedOn
                          : selectedItem.LastModifiedDate
                      ).format("DD/MM/YY")}
                    </Text>
                  </View>

                  <View
                    style={[
                      styles.itemBottomSubview,
                      { marginLeft: 20 },
                    ]}>
                    <Image
                      resizeMode={"contain"}
                      tintColor={Constant.appColorAlpha()}
                      style={{
                        marginRight: 5,
                        width: 16,
                        height: 16,
                      }}
                      source={images.clock}
                    />

                    <Text
                      style={[
                        styles.patientName,
                        {
                          color: Constant.appColorAlpha(),
                        },
                        BaseStyle.regularFont,
                      ]}>
                      {moment(
                        selectedItem.CreatedOn
                      ).format("HH:mm a")}
                    </Text>
                  </View>
                </View>

                <View></View>
              </View>
            </View>
            <View style={styles.itemline}></View>

            <View
              style={{
                width: "100%",
                padding: 15,
                paddingTop: 5,
                flexDirection: "row",
              }}>
              <View style={{ flex: 0.5 }}>
                <Text
                  style={[
                    styles.subHeader,
                    BaseStyle.regularFont,
                  ]}>
                  {language.patient}
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    flex: 1,
                    paddingTop: 8,
                    alignItems: "center",
                  }}>
                  <Image
                    style={{
                      marginRight: 5,
                      width: 40,
                      height: 40,
                    }}
                    source={images.user}
                  />
                  <Text
                    style={[
                      styles.patientName,
                      {
                        color: Constant.textAlpha(),
                        fontSize: 16,
                      },
                      BaseStyle.boldFont,
                    ]}>
                    {
                      selectedItem.HealthRecordPatient
                    }
                  </Text>
                </View>
              </View>

              <View style={{ flex: 0.5 }}>
                <Text
                  style={[
                    styles.subHeader,
                    BaseStyle.regularFont,
                  ]}>
                  {language.doctor}
                </Text>
                <View
                  style={{
                    flexDirection: "row",
                    flex: 1,
                    paddingTop: 8,
                    alignItems: "center",
                  }}>
                  <Image
                    style={{
                      marginRight: 5,
                      width: 40,
                      height: 40,
                    }}
                    source={images.user}
                  />
                  <Text
                    style={[
                      styles.patientName,
                      {
                        color: Constant.textAlpha(),
                        fontSize: 16,
                      },
                      BaseStyle.boldFont,
                    ]}>
                    {
                      selectedItem.HealthRecordDoctor
                    }
                  </Text>
                </View>
              </View>
            </View>

            <View
              style={{
                flex: 1,
                padding: 15,
                paddingTop: 5,
              }}>
              <Text
                style={[
                  styles.subHeader,
                  BaseStyle.boldFont,
                ]}>
                {language.summary}
              </Text>
              <Text
                style={[
                  styles.patientName,
                  { marginTop: 7, marginLeft: 2 },
                  BaseStyle.regularFont,
                ]}>
                {selectedItem.Description}
              </Text>
            </View>
          </View>
        </ScrollView>

        {isShow ? this.showImage() : null}
      </SafeAreaView>
    )
  }

  deletePopUP(id) {
    Alert.alert(
      "",
      "Do you want to delete this Health Record?",
      [
        { text: "CANCEL" },
        {
          text: "DELETE",
          onPress: () => this.deleteRecord(id),
        },
      ]
      // { cancelable: true }
    )
  }

  deleteRecord(id) {
    this.setState({ isLoading: true })

    Constant.deleteMethod(
      urls.deleteHealthRecord + id,
      {},
      (result) => {
        this.setState({ isLoading: false })

        // alert(JSON.stringify(result))
        if (result.success) {
          CartDb.deleteHealthRecord(id)

          this.props.navigation.pop(1)
        } else {
          //   this.setState({isNumberCorrect:false})
        }
      }
    )
  }

  showImage() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Modal
          visible={this.state.isShow}
          transparent={true}
          onRequestClose={() =>
            this.setState({ isShow: false })
          }>
          <ImageViewer
            enableSwipeDown={true}
            onSwipeDown={() => {
              console.log("down")
              this.setState({ isShow: false })
            }}
            imageUrls={this.state.showImages}
          />

          <TouchableOpacity
            style={{
              position: "absolute",
              top: 30,
              right: 30,
            }}
            onPress={() => {
              this.setState({ isShow: false })
            }}>
            <Image
              source={images.close}
              style={{
                height: 25,
                width: 25,
                tintColor: "#fff",
              }}
            />
          </TouchableOpacity>
        </Modal>
      </SafeAreaView>
    )
  }

  _onMomentumScrollEnd = ({ nativeEvent }: any) => {
    const index = Math.round(
      nativeEvent.contentOffset.x / width
    )

    if (index !== this.state.position) {
      this.setState({ position: index })
    }
  }
}

const styles = StyleSheet.create({
  mainView: {
    marginTop: 10,
    marginBottom: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  backView: {
    marginLeft: 15,
    height: 35,
    width: 35,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.4)",
  },
  backBtn: { height: 20, width: 20, left: 0 },
  uploadView: {
    marginTop: 0,
    padding: 15,
    height: 330,
    width: width,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    resizeMode:"contain"
  },
  itemImage: {
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    elevation: 4,
    width: 40,
    backgroundColor: Constant.appLightColor(),
    borderRadius: 6,
  },
  itemMainText: {
    fontSize: 18,
    flex: 1,
    marginLeft: 10,
    color: "#000",
  },
  itemline: {
    height: 1,
    backgroundColor: "#D0D1D2",
    marginTop: 6,
    marginBottom: 10,
    marginLeft: 20,
    marginRight: 20,
  },
  itemSubtext: {
    fontSize: 12,
    flex: 1,
    marginLeft: 10,
    marginTop: 4,
    color: "#828282",
  },

  itemBottomSubview: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  patientName: { fontSize: 12, color: "#000" },
  itemBottomView: {
    left: 10,
    alignItems: "center",
    flexDirection: "row",
    height: 20,
    marginTop: 10,
    width: "100%",
  },
  subHeader: { fontSize: 14, color: "#000" },
})

export default HealthRecordDetails
