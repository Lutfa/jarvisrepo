import React, { Component } from "react"
import {
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    FlatList,
    Image,
    Platform,
    Animated,
    StatusBar,
    StyleSheet,
    Modal,
    Alert,
    AsyncStorage,
    ScrollView,
    
} from "react-native"

import Constant from "../BaseClass/Constant"
import BaseStyles from "../BaseClass/BaseStyles"
import SplashScreen from "react-native-splash-screen"
import images from "../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import language from "../BaseClass/language"
import urls from "../BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")
import Db from "../DB/Realm"
import {
    TextField,
    
} from "react-native-material-textfield"

import moment from "moment"
import CartDb from "../DB/CartDb"
import Toast from "react-native-simple-toast"
import { CommonActions, NavigationContainer } from "@react-navigation/native"
import axios from "axios"
import LinearGradient from "react-native-linear-gradient"
import CountryPicker from "react-native-country-picker-modal"
var Realm = require("realm")

export default class GetHelp extends Component {
    constructor(props) {
        super(props)
    }
    render(){
        return(
<View style={{ flex: 1, backgroundColor: "#fff" }}>
                <View style={{ flex: 1, paddingHorizontal: 20 }}>
                    {this.headerView()}
                    <View

                    >
                        <Text style={[styles.headerTextStyle,]}>Contact us and we will get back to you shortly</Text>
                        <View style={styles.numberContainer}>
                        <Text style={[styles.headerTextStyle,{fontSize:24,color:"#000"}]}>customercare@jarvis.com</Text>

                        </View>
                        <View style={styles.numberContainer}>
                        <Text style={[styles.headerTextStyle,{fontSize:24,color:"#000"}]}>+1-236-456-4451</Text>

                        </View>
                        </View>
                        </View>
                        </View>

        )
    }
    headerView() {
        


        return (
            <SafeAreaView
                style={[
                    styles.headerStyle,
                    
                ]}>
                <StatusBar
                    translucent
                    barStyle="light-content"
                    //  backgroundColor="rgba(0, 0, 0, 0.251)"
                    backgroundColor={Constant.headerColor()}

                />
                <TouchableOpacity
                    style={styles.headerInnerView}>
                    <TouchableOpacity
                        activeOpacity={2}
                        style={styles.menuStyle}
                    >

                        <Image
                            resizeMode={"contain"}
                            style={{
                                height: 50,
                                width: 120,
                            }}
                            source={images.icon}
                        />
                    </TouchableOpacity>

                    <View
                        style={{
                            flexDirection: "row",
                            alignItems: "center",
                            marginRight: 15,
                        }}>


                       


                    </View>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
     
}
const styles = StyleSheet.create({
    headerStyle: {
        flex: 0,


        paddingTop: 10,
        justifyContent: "center",
    },
    headerInnerView: {
        height: 50,
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    headerTextStyle: { letterSpacing: 0, color: Constant.appColor(), fontSize: 20, fontFamily: 'Roboto-Regular', fontWeight: "500" }
    ,
    numberContainer: {
       
    
        height: "20%",
        width: "100%",
        borderRadius: 9,
        marginTop:30,
        backgroundColor: "#FFE7E4",
    
    borderColor:"#B3FFD4",
    borderWidth:1,
        justifyContent: "center",
        alignItems: "center",
      },
}) 
