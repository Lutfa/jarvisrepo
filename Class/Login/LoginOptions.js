import React, { Component } from "react"
import {
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    FlatList,
    Image,
    Platform,
    Animated,
    StatusBar,
    StyleSheet,
    Modal,
    Alert,
    AsyncStorage,
    ScrollView,
    
} from "react-native"

import Constant from "../BaseClass/Constant"
import BaseStyles from "../BaseClass/BaseStyles"
import SplashScreen from "react-native-splash-screen"
import images from "../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import language from "../BaseClass/language"
import urls from "../BaseClass/ServiceUrls"
const { width, height } = Dimensions.get("window")
import Db from "../DB/Realm"
import {
    TextField,
    
} from "react-native-material-textfield"

import moment from "moment"
import CartDb from "../DB/CartDb"
import Toast from "react-native-simple-toast"
import { CommonActions, NavigationContainer } from "@react-navigation/native"
import axios from "axios"
import LinearGradient from "react-native-linear-gradient"
import CountryPicker from "react-native-country-picker-modal"


//Import some screens


var Realm = require("realm")

class LoginOptions extends Component {
    constructor(props) {
        super(props)

        this.state = {
            countryName: "IN",
            countryCode: "+91",
            showEnterPin: false,
            isNumberCorrect: "",
            showConfirmPin: false,
            confirmPin: "",
            enterPin: "",
            isConfirm: false,
            isEmailCorrect: "",
            email: "",
            pinColor: "",
            name: "",
            nameError: "",
            phoneNumber: "",
            enterPinError: "",
            confirmPinError: "",

        }

        language.setLanguage("en")
    }

    async componentDidMount() {
        this._unsubscribe =
            this.props.navigation.addListener(
                "focus",
                () => {
                    this.forceUpdate()
                }
            )


        SplashScreen.hide()

    }





    render() {
        const {
            name,
            nameError,
            enterPinError,
            confirmPinError,
            showEnterPin,
            isNumberCorrect,
            email,
            enterPin,
            isEmailCorrect,
            pinColor,
            phoneNumber,
            showConfirmPin,
            confirmPin,
            isConfirm,
            countryCode,
            
        } = this.state

        return (
            <View style={{ flex: 1, backgroundColor: "#fff" }}>
                <View style={{ flex: 1, paddingHorizontal: 20 }}>
                    {this.headerView()}
                    <View

                    >
                        <ScrollView

                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={false}
                            ref="mainScroll">
                            <View>
                                <View style={{ marginTop: 10, flexDirection: "row", alignItems: "center", alignContent: "center" }}>

                                    <Text style={[styles.headerTextStyle,]}>Login</Text>
                                    <Text style={[styles.headerTextStyle, { marginLeft: 5, fontSize: 16, color: Constant.textGreyColor() }]}>or</Text>
                                    <Text style={[styles.headerTextStyle, { marginLeft: 5 }]}>SignUp</Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: "row",
                                        justifyContent: "center",
                                        alignItems: "center", marginTop: 20
                                    }}>
                                    <View
                                        style={{
                                            justifyContent: "center",
                                            alignItems: "center",
                                            marginTop: 0,
                                            height: 56,
                                            borderBottomWidth: 1,
                                            borderBottomColor:
                                                Constant.appColorAlpha(),
                                        }}>
                                        <CountryPicker
                                            style={{
                                                height: 40,
                                                width: 60,
                                            }}
                                            countryCode={
                                                this.state.countryName
                                            }
                                            translation={"ita"}
                                            withCallingCodeButton
                                            withAlphaFilter
                                            withFilter
                                            visible={false}
                                            onClose={() => {
                                                this.setState({
                                                    showCountry: false,
                                                })
                                            }}
                                            onSelect={(country) => {
                                                this.setState({
                                                    countryName: country.cca2,
                                                    countryCode:
                                                        country.callingCode,
                                                })
                                            }}
                                        />
                                    </View>
                                    <View
                                        style={{
                                            width: width - 120,
                                            paddingLeft: 10,
                                        }}>
                                        <TextField
                                            label={language.phonenumber}
                                            value={phoneNumber}
                                            lineWidth={1}
                                            fontSize={15}
                                            labelFontSize={13}
                                            labelTextStyle={[
                                                { paddingTop: 5 },
                                                BaseStyles.regularFont,
                                            ]}
                                            keyboardType="numeric"
                                            baseColor={Constant.appFullColor()}
                                            tintColor={Constant.appFullColor()}
                                            textColor={"#000"}
                                            // error={isNumberCorrect}
                                            // onBlur={() => {
                                            //   if (
                                            //     Constant.isValidPhone(
                                            //       phoneNumber
                                            //     )
                                            //   ) {
                                            //     this.isPhoneAvailable()
                                            //   } else {
                                            //     this.setState({
                                            //       isNumberCorrect:
                                            //         "Enter valid phone number",
                                            //     })
                                            //   }
                                            // }}
                                            onChangeText={(text) => {
                                                let number = text.replace(
                                                    /[^0-9]/g,
                                                    ""
                                                )
                                                this.setState({
                                                    phoneNumber: number,
                                                    isNumberCorrect: "",
                                                })
                                            }}
                                        />
                                    </View>
                                </View>

                                <TouchableOpacity


                                    onPress={() => {

                                        this.props.navigation.push("Otp", {
                                            phoneNumber: phoneNumber,
                                            countryCode: countryCode,
                                    
                                            parameters: "",
                                        })

                                        // this.props.navigation.push('Otp',{number:'9959814490'})


                                    }}>
                                    <LinearGradient
                                        start={{ x: 0, y: 1 }}
                                        end={{ x: 1, y: 1 }}

                                        colors={[
                                            Constant.gradientColor1(),
                    Constant.gradientColor2(),


                                        ]}
                                        style={{ marginTop: 30, marginBottom: 10, borderRadius: 8, height: 40, alignContent: "center", alignItems: "center", justifyContent: "center", width: width - 40 }}>
                                        <Text style={{ fontSize: 14, color: "#000" }}>SEND OTP</Text>
                                    </LinearGradient>
                                </TouchableOpacity>

                                <View style={{ height: 30, width: "100%", marginTop: 50, alignContent: "center", alignItems: "center" }}>

                                    <View style={{ marginTop: 10, flexDirection: "row", alignItems: "center", alignContent: "center" }}>

                                        <View style={{ height: 0.5, width: "45%", backgroundColor: "#909090" }}></View>
                                        <Text style={[styles.headerTextStyle, { marginTop: 2, marginHorizontal: 10, fontSize: 18, color: Constant.textGreyColor() }]}>or</Text>
                                        <View style={{ height: 0.5, width: "45%", backgroundColor: "#909090" }}></View>
                                    </View>
                                </View>

                                <View style={[styles.loginEmailBg, { flexDirection: "row", alignItems: "center", alignContent: "center", marginTop: 30 }]}>
                                    <Image style={{ height: 20, width: 20, resizeMode: "contain", marginLeft: 30 }} source={require('../../assets/googleLogo.png')}></Image>

                                    <Text style={[styles.headerTextStyle, { marginHorizontal: 20, fontSize: 14, color: "#000" }]}>CONTINUE WITH Google</Text>

                                </View>
                                <TouchableOpacity onPress={() => {
                                    this.props.navigation.navigate("LoginWithEmail")
                                }}>
                                    <View style={[styles.loginEmailBg, { flexDirection: "row", alignItems: "center", alignContent: "center", marginTop: 20 }]}>
                                        <Image style={{ height: 20, width: 20, resizeMode: "contain", marginLeft: 30 }} source={images.email}></Image>

                                        <Text style={[styles.headerTextStyle, { marginHorizontal: 20, fontSize: 14, color: "#000" }]}>CONTINUE WITH Email</Text>

                                    </View>
                                </TouchableOpacity>
                                <View style={[styles.loginEmailBg, { flexDirection: "row", alignItems: "center", alignContent: "center", marginTop: 20 }]}>
                                    <Image style={{ height: 20, width: 20, resizeMode: "contain", marginLeft: 30 }} source={require('../../assets/facebookLogo.png')}></Image>

                                    <Text style={[styles.headerTextStyle, { marginHorizontal: 20, fontSize: 14, color: "#000" }]}>CONTINUE WITH Facebook</Text>

                                </View>

                                <View style={{ marginTop: 30, flexDirection: "row", alignItems: "center", alignContent: "center" }}>


                                    <Text style={[styles.headerTextStyle, { marginLeft: 5, fontSize: 14, color: Constant.textGreyColor() }]}>Having trouble logging in? </Text>
                                   <TouchableOpacity  onPress={() => {
                      this.props.navigation.navigate("GetHelp")
                    }}> 
                                   <Text style={[styles.headerTextStyle, { fontSize: 14, }]}>Get help</Text>
                               
                               </TouchableOpacity>
                                </View>
                                <Text style={[styles.headerTextStyle, { marginLeft: 5, marginTop: 20, fontSize: 12, color: Constant.textGreyColor() }]}>By continuing, I agree to the Terms of Use & Privacy Policy </Text>



                                <View style={{ height: 20 }} />
                            </View>
                        </ScrollView>
                    </View>

                </View>

            </View>


        )
    }

    headerView() {
        const { drawerOpen } = this.state

        const userData = global.userData

        return (
            <SafeAreaView
                style={[
                    styles.headerStyle,
                    {
                        borderTopLeftRadius: drawerOpen ? 8 : 0,
                    },
                ]}>
                <StatusBar
                    translucent
                    barStyle="light-content"
                    //  backgroundColor="rgba(0, 0, 0, 0.251)"
                    backgroundColor={Constant.headerColor()}

                />
                <TouchableOpacity
                    style={styles.headerInnerView}>
                    <TouchableOpacity
                        activeOpacity={2}
                        style={styles.menuStyle}
                    >

                        <Image
                            resizeMode={"contain"}
                            style={{
                                height: 50,
                                width: 120,
                            }}
                            source={images.icon}
                        />
                    </TouchableOpacity>

                    <View
                        style={{
                            flexDirection: "row",
                            alignItems: "center",
                            marginRight: 15,
                        }}>


                        <TouchableOpacity
                            onPress={() => {
                              this.props.navigation.pop(1
                                
                              )
                            }}
                            style={{


                                backgroundColor: "#fff",
                                borderRadius: 15,
                                height: 25, width: 25, elevation: 6, justifyContent: "center", alignContent: "center", alignItems: "center"
                            }}>
                            <Image

                                style={{ width: 20, height: 120, resizeMode: "center" }}
                                source={images.backicon}
                            />

                        </TouchableOpacity>


                    </View>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }


    //Serivce calls
















}


const styles = StyleSheet.create({
    headerStyle: {
        flex: 0,


        paddingTop: 10,
        justifyContent: "center",
    },
    headerInnerView: {
        height: 50,
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between",
    },

    loginEmailBg: {
        height: 40, width: width - 40, backgroundColor: "#F6F6F6",
        borderRadius: 9,
        elevation: 8,
    },
    menuStyle: {

        height: 0,
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
    },
    mainView: {
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
        marginTop: -110,
        zIndex: 101,
    },
    topBox: {
        height: 100,
        paddingRight: 5,
        paddingLeft: 5,
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-between",
    },
    topInnerBox: {
        flex: 0.2,

        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 6,
        elevation: 2,
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowColor: "#d3d3d3",
        shadowRadius: 5,
        shadowOpacity: 0.6,
    },

    categoriesBox: {
        width: width / 4 - 16,
        marginRight: 10,

        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 6,
        elevation: 2,
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowColor: "#d3d3d3",
        shadowRadius: 5,
        shadowOpacity: 0.6,
    },
    subMenu: { fontSize: 16, marginRight: 15 },
    viewAll: {
        fontSize: 13,
        color: Constant.homeNonSelected(),
    },
    topInnerBox2: {
        width: 40,
        height: 40,
        borderRadius: 6,
        elevation: 2,
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowColor: "#d3d3d3",
        shadowRadius: 5,
        shadowOpacity: 0.6,
        backgroundColor: Constant.appLightColor(),
        alignItems: "center",
        justifyContent: "center",
    },
    topInnerText: {
        color: Constant.textColor(),
        paddingLeft: 5,
        paddingRight: 5,
        marginTop: 10,
        fontSize: 11,
        textAlign: "center",
        lineHeight: 13,
        height: 26,
    },

    boxTitle: {
        height: 40,
        width: "100%",
        backgroundColor: "#fff",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingRight: 5,
        paddingLeft: 5,
    },
    bestOffersMain: {
        height: 120,
        width: "100%",
        borderRadius: 6,
        padding: 4,
        paddingLeft: 2,
        justifyContent: "center",
        alignItems: "center",
    },
    bestOffersInner: {
        width: "100%",
        borderRadius: 6,
        backgroundColor: "#fff",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowColor: "#d3d3d3",
        shadowRadius: 5,
        shadowOpacity: 0.6,
        elevation: 2,
        marginRight: 2,
        paddingTop: 6,
        alignItems: "center",
        justifyContent: "center",
    },
    bestOffersText: {
        width: "100%",
        backgroundColor: Constant.appColorAlpha(),
        color: Constant.selectedTextColor(),
        textAlign: "center",
        padding: 2,
        fontSize: 12,
        borderBottomLeftRadius: 6,
        borderBottomRightRadius: 6,
    },
    bannerMain: {
        marginTop: 20,
        backgroundColor: Constant.appColorAlpha(),
        borderRadius: 6,
    },
    banneritem: {
        width: width - 30,
        height: 200,
        borderRadius: 6,
        backgroundColor: Constant.appColorAlpha(),
        overflow: "hidden",
    },
    bannerTitle: {
        color: Constant.selectedTextColor(),
        fontSize: 16,
    },
    bannerSub: {
        color: Constant.selectedTextColor(),
        fontSize: 13,
        marginTop: 10,
    },
    bannerBuy: {
        marginTop: 15,
        width: 100,
        borderRadius: 20,
        elevation: 2,
        alignItems: "center",
        justifyContent: "center",
        height: 40,
        backgroundColor: Constant.appFullColor(),
    },
    bannerDotView: {
        flexDirection: "row",
        paddingBottom: 0,
        justifyContent: "center",
        alignItems: "center",
        position: "absolute",
        bottom: 5,
        left: 0,
        right: 0,
    },
    bannerDot: {
        height: 9,
        width: 9,
        elevation: 2,
        backgroundColor: Constant.appColorAlpha(),
        margin: 3,
        borderRadius: 4.5,
    },
    searchMainView: {
        width: "100%",
        marginTop: 10,
        paddingRight: 5,
        paddingLeft: 5,
        height: 50,
        marginBottom: 20,
        alignItems: "center",
        justifyContent: "center",
        elevation: 8
    },

    searchInner: {
        marginRight: 5,
        marginLeft: 5,
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowColor: "#d3d3d3",
        shadowRadius: 5,
        shadowOpacity: 0.6,
        elevation: 0,
        height: 50,
        backgroundColor: "#fff",
        borderRadius: 10,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    searchIcon: {
        marginLeft: 0,
        color: Constant.textColor(),
        textAlign: "left",
        left: 5,
        fontSize: 12,
        flex: 1,
    },


    linearGradient: {

        width: '100%',
        height: 40,
        borderRadius: 20
    }, patientContainer: {
        backgroundColor: "#fff",
        borderRadius: 8,
        elevation: 2,
        alignContent: "center",
        alignItems: "center",
        width: (width - 50) / 2


    }, headerTextStyle: { letterSpacing: 0, color: Constant.appColor(), fontSize: 20, fontFamily: 'Roboto-Regular', fontWeight: "500" }
    , circle: {


        marginRight: 15,
        backgroundColor: "#fff",
        borderRadius: 15,
        height: 30, width: 30, elevation: 6, justifyContent: "center", alignContent: "center", alignItems: "center"

    },

})
export default LoginOptions


