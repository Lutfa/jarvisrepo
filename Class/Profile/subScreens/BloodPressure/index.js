import React, { Component } from 'react'
import { Text, StyleSheet, View, Dimensions, Image, StatusBar, TouchableOpacity } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import BackButtonHeader from '../../components/backButtonHeader'
import BaseStyle from "./../../../BaseClass/BaseStyles";
import Constant from "./../../../BaseClass/Constant";
import {
  TextField,
} from "react-native-material-textfield"
import LinearGradient from "react-native-linear-gradient"
import urls from '../../../BaseClass/ServiceUrls';

const { width, height } = Dimensions.get('window');

export default class BloodPressure extends Component {
  constructor(props) {
    super(props)
    this.state = {
      sysBlood: null,
      diaBlood: null,
      pulseBlood: null,
      isLoading: false
    }
  }

  render() {
    const {
      sysBlood,
      diaBlood,
      pulseBlood,
      isLoading
    } = this.state;

    return (
      <SafeAreaView style={{ flex: 1 ,backgroundColor:"#e5e5e5"}}>
        {isLoading ? Constant.showLoader() : null}

        <View style={{ marginTop: 10 }}>
          <BackButtonHeader navigation={this.props.navigation} titleHeader={"Blood Pressure"} />
        </View>

        <View style={styles.blankView}>

          <View style={{ alignItems: 'center' }}>
            <View style={styles.heartView}>
              <Image source={require('../../../../assets/heart.png')} style={{ width: 40, height: 40 }} />
            </View>

            <View style={{ marginTop: height / 30, alignItems: "center", maxWidth: width / 2 }}>
              <Text style={styles.textPressureRelated}>Blood Pressure Related</Text>
              <Text style={{ textAlign: 'center' }}>Share you recent recordings for best experiences</Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row' }}>
            <TextField
              label="Sys"
              value={sysBlood}
              editable={true}
              lineWidth={1}
              fontSize={15}
              onChangeText={(text) =>
                this.setState({
                  sysBlood: text,
                })
              }
              containerStyle={styles.containerField}
              keyboardType='number-pad'
              labelFontSize={13}
              labelTextStyle={[
                { paddingTop: 5 },
                BaseStyle.regularFont,
              ]}
              baseColor={"#000"}
              tintColor={"#000"}
              textColor={"#1F94BC"}
            />

            <TextField
              label="Dia"
              value={diaBlood}
              editable={true}
              lineWidth={1}
              fontSize={15}
              onChangeText={(text) =>
                this.setState({
                  diaBlood: text,
                })
              }
              containerStyle={styles.containerField}
              keyboardType='number-pad'
              labelFontSize={13}
              labelTextStyle={[
                { paddingTop: 5 },
                BaseStyle.regularFont,
              ]}
              baseColor={"#000"}
              tintColor={"#000"}
              textColor={"#1F94BC"}
            />


          </View>

          <TextField
            label="Pulse"
            value={pulseBlood}
            editable={true}
            lineWidth={1}
            fontSize={15}
            onChangeText={(text) =>
              this.setState({
                pulseBlood: text,
              })
            }
            containerStyle={styles.containerField}
            keyboardType='number-pad'
            labelFontSize={13}
            labelTextStyle={[
              { paddingTop: 5 },
              BaseStyle.regularFont,
            ]}
            baseColor={"#000"}
            tintColor={"#000"}
            textColor={"#1F94BC"}
          />
        </View>



        <StatusBar barStyle="dark-content" translucent backgroundColor="white" />
        <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: 0, width: width, height: height / 12, backgroundColor: '#ebebeb', elevation: 10 }}>
          <TouchableOpacity onPress={() => this.saveBloodInfo()}
          >
             <LinearGradient
                              start={{ x: 0, y: 1 }}
                              end={{ x: 1, y: 1 }}

                              colors={[
                                Constant.gradientColor1(),
                                Constant.gradientColor2(),

                              ]}
                              style={{ marginHorizontal: 10, marginTop: 5, marginBottom: 10, borderRadius: 111,width: width / 1.1, height: height / 20, alignContent: "center", alignItems: "center", justifyContent: "center" }}>
                              <Text style={{ fontSize: 14, color: "#000" }}>SAVE CHANGES</Text>
                            </LinearGradient>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    )
  }

  saveBloodInfo() {
    const { sysBlood,
      diaBlood,
      pulseBlood,}=this.state
   let  parameters=global.userData
  
   console.log("param",JSON.stringify(parameters))
   
    // parameters = {
    //       ...parameters,
    //       ...{ bloodPressureSys: sysBlood },
    //      }
     
//     if (sysBlood != "") {
//         parameters.bloodPressureSys= sysBlood;
//       }
   
//   if (diaBlood != "") {
//     parameters.bloodPressureDia= diaBlood;
 
// }
// if (pulseBlood != "") {
//     parameters.pulse= pulseBlood;
  

// }

console.log("profile param",JSON.stringify(parameters))

Constant.postWithTokenMethod(
  urls.profileUpdate,
  parameters,
  (result) => {
    this.setState({ isLoading: false })

    if (result.success) {
      let response = result.result

      if (response.status != "Fail") {
        console.log("profile res",JSON.stringify(response))
        let userId =
          response.response.patientData.id


        let userData = response.response.patientData
        global.userData = userData
        this.setState({ isLoading: false })
        // this.saveUserData(response.Response)

        try {
          Db.write(() => {
            Db.create(
              "User",
              {
                
                id: userData.id,
                name: userData.name,
                email: userData.email,
                phoneNumber:
                  userData.phoneNumber,
             deviceType: userData?.deviceType,
               // Latitude: userData.Latitude,
               // Longitude: userData.Longitude,
                // IsPhoneVerified:
                //   userData.isPhoneVerified,
                countryCode:
                  userData.countryCode,
                  pictureUrl:
                  userData.pictureUrl,

                  age: userData.age,
                  sex: userData.sex,
                  temparature: userData.temparature,
                 
                  fastingBloodSugar: userData.fastingBloodSugar,
                  bloodSugar:  userData.bloodSugar,
                  bloodPressureDia: userData.bloodPressureDia,
                  bloodPressureSys: userData.bloodPressureSys,
                  pulse: userData.pulse,
                  deviceUniqueId: userData.deviceUniqueId,
                  token: global.userToken,

            
                // Dob: userData.Dob,
                // Gender: userData.Gender,
                // HealthIssues:
                //   userData.HealthIssues,
                // SCProof: userData.SCProof,
                // SCVerified: userData.SCVerified,
                // PHCDocument:
                //   userData.PHCDocument,
                // PHCVerified:
                //   userData.PHCVerified,
                // FirstName: userData.FirstName,
                // LastName: userData.LastName,
                // DeviceId: userData.deviceId,
                // DeviceUniqueId:
                //   userData.deviceUniqueId,
                // AuthToken: userData.AuthToken,
                // ReferralCode:
                //   userData.ReferralCode,
                // Version: userData.version,
                // DeviceModel:
                //   userData.deviceModel,
                // DeviceBrand:
                //   userData.deviceBrand,
                // DeviceVersion:
                //   userData.deviceVersion,
              },
              "modified"
            )

            AsyncStorage.setItem(
              "userLoggedIn",
              "true"
            )
            const value =  AsyncStorage.getItem(
              "userLoggedIn"
            )
           
            this.props.navigation.pop(1)
            
          })
        } catch (e) {
          console.log("Error on creation"+e)
          Toast.show("Unable to save data")
        }
      } else {
        Toast.show("Something went wrong")
      }
    }
  }
)
}
}
const styles = StyleSheet.create({
  containerField: {
    marginLeft: 22,
    width: width / 2.5
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  textPressureRelated: {
    color: '#FE3C28',
    fontSize: 18,
    fontWeight: '500'
  },
  blankView: {
    backgroundColor: 'white',
    width: width,
    height: height / 2,
    marginTop: height / 19,
    borderRadius: 30
  },
  heartView: {
    marginTop: height / 30,
    backgroundColor: 'white',
    justifyContent: "center",
    alignItems: 'center',
    borderRadius: 50,
    width: 70,
    height: 70,
    elevation: 10
  }
})