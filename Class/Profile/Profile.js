import React, { Component } from "react"
import {
  View,
  TextInput,
  ImageBackground,
  AppState,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  StatusBar,
} from "react-native"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import SplashScreen from "react-native-splash-screen"
import Icon from 'react-native-vector-icons/FontAwesome5';
import BackButtonHeader from "./components/backButtonHeader"
import ImageLoad from "../BaseClass/ImageLoader"



const { width } = Dimensions.get("window")
const heightScreen = Dimensions.get("window").height

// const MAX_POINTS = 90

class Profile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      categoryIndex: 0,
      points: 80,
      isNew: true,
    }
  }

  componentDidMount() {
    SplashScreen.hide()
    this._unsubscribe = this.props.navigation.addListener(
      "focus",
      () => {
        this.setState({ isNew: false })
      }
    )
  }

  render() {
    // const fill =
    //   (this.state.points / MAX_POINTS) * 100
    // const { categoryIndex } = this.state
     const userData = global.userData

    // var height = parseInt(userData.Height)
    // height = height > 0 ? height : 0

    // var weight = parseInt(userData.Weight)
    // weight = weight > 0 ? weight : 0

    // var bmi = weight / ((height / 100) * (height / 100))

    // if (!!bmi) {
    //   bmi = bmi.toFixed(2)
    // }

    // const healthIssueArray =
    //   userData.HealthIssues != null &&
    //     userData.HealthIssues != ""
    //     ? userData.HealthIssues.split(",")
    //     : []

    return (
      <SafeAreaView style={{flex:1,backgroundColor:"#fff"}}>
        <ImageBackground style={{ height: "70%", width: "100%", position:'absolute'}} source={images.bg}/>
        <ImageBackground style={{ height: "80%", width: "100%", position:'absolute'}} source={images.pharma_top_curve}/>
        <ImageBackground style={{ height: "30%", width: "100%", position:'absolute', top: heightScreen/3}} source={images.background_vectors}/>

        <BackButtonHeader navigation={this.props.navigation} titleHeader={"Marinell Anastacia"}/>
       
         <Image source={{uri: userData?.pictureUrl}} style={styles.photoProfile} /> 
        <TouchableOpacity onPress={() => this.props.navigation.navigate('EditProfile')} style={styles.editProfile}>
          <Icon name="pencil-alt" size={14} color="#fff" />
        </TouchableOpacity>

        <View style={styles.blankView}>
          {/* Blank VIEW */}
          <View style={{marginTop: heightScreen/8, paddingHorizontal: 20}}>
            {/* Infor VIEW */}
            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-evenly'}}>
              {/* Name, age and gender VIEW */}
              <Text style={{fontSize: 18, color:'#FE3C28'}}>{userData.name}</Text>
              <Text style={{fontSize: 16, color:'#000'}}>28 Her/She</Text>
            </View>

           
           
           
            <View style={styles.boxInfo}>
              {/* Box Input VIEW */}
              <Image source={require("../../assets/thermometer.png")} style={{width: width/14, height: heightScreen/26}}/>
              
              <View style={{flexDirection:'column', marginLeft:10}}>
                {/* Text Column VIEW */}
                <Text style={styles.boldTxt}>Temperature</Text>
                <Text style={{color:'#FE3C28'}}>102 F</Text>
              </View>

              <View style={{ paddingLeft: width/2.6, flexDirection: 'row', alignItems: 'center' }}>
                <TouchableOpacity onPress={() => {}} style={{ marginLeft: width / 18, backgroundColor: '#fff', width: width / 12, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 40, elevation: 3 }}>
                  <Icon name="chevron-right" size={14} color="#FE3C28" />
                </TouchableOpacity>
              </View>
            </View>




            <View style={styles.boxInfo}>
              {/* Box Input VIEW */}
              <Image source={require("../../assets/bloodBag.png")} style={{width: width/14, height: heightScreen/26}}/>
              
              <View style={{flexDirection:'column', marginLeft:10}}>
                {/* Text Column VIEW */}
                <Text style={styles.boldTxt}>Blood Sugar</Text>
                <View style={{flexDirection:'row'}}>
                  <Text style={{color:'#000'}}>Fasting</Text>
                  <Text style={{color:'#FE3C28', marginLeft: 5}}>126mg/dL</Text>
                </View>
              </View>

              <View style={{ paddingLeft: width/3.4, flexDirection: 'row', alignItems: 'center' }}>
                <TouchableOpacity onPress={() => {}} style={{ marginLeft: width / 18, backgroundColor: '#fff', width: width / 12, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 40, elevation: 3 }}>
                  <Icon name="chevron-right" size={14} color="#FE3C28" />
                </TouchableOpacity>
              </View>
            </View>




            <View style={{ flexDirection:'row', marginTop: heightScreen/28, elevation: 5, padding:15, alignItems:'center', borderRadius:10, backgroundColor:'white', width: width/1.12, height: heightScreen/7}}>
              {/* Box Input VIEW */}
              <Image source={require("../../assets/heart.png")} style={{width: width/14, height: heightScreen/26}}/>
              
              <View style={{flexDirection:'column', marginLeft:10}}>
                {/* Text Column VIEW */}
                <Text style={[{marginBottom: 10}, styles.boldTxt]}>Blood Pressure</Text>

                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                  {/* Sys and Dia VIEW */}
                  <Text style={{color:'#000'}}>Sys</Text>
                  <Text style={{color:'#FE3C28'}}>116</Text>
                </View>

                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                  {/* Sys and Dia VIEW */}
                  <Text style={{color:'#000'}}>Dia</Text>
                  <Text style={{color:'#FE3C28'}}>116</Text>
                </View>

                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                  {/* Sys and Dia VIEW */}
                  <Text style={{color:'#000'}}>Pulse</Text>
                  <Text style={{color:'#FE3C28'}}>69</Text>
                </View>
              </View>

              <View style={{ paddingLeft: width/2.9, flexDirection: 'row', alignItems: 'center'}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('BloodPressure')} style={{ marginLeft: width / 18, backgroundColor: '#fff', width: width / 12, height: 30, justifyContent: 'center', alignItems: 'center', borderRadius: 40, elevation: 3 }}>
                  <Icon name="chevron-right" size={14} color="#FE3C28" />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>


          {/* PROFILE */}
          <StatusBar barStyle="dark-content" translucent backgroundColor="transparent" />
      </SafeAreaView>
    )
  }

  healthStatus(bmi) {
    var txt = "";
    if (bmi > 24.9) {
      txt = "Over weight"
    } else if (bmi < 18.5) {
      txt = "Under weight"
    } else {
      txt = "Normal"
    }
    return txt
  }
}

const styles = StyleSheet.create({
  boldTxt: {
    fontWeight: 'bold'
  },
  boxInfo: {
    flexDirection:'row', 
    marginTop: heightScreen/28, 
    elevation: 5, 
    padding:15, 
    alignItems:'center', 
    borderRadius:10, 
    backgroundColor:'white', 
    width: width/1.12, 
    height: heightScreen/12
  },
  blankView: {
    position:'absolute', 
    width: width, 
    height: heightScreen/1.45, 
    bottom:0, 
    backgroundColor: '#fff', 
    borderTopLeftRadius: 40, 
    borderTopRightRadius: 40
  },
  nameProfile: {
    fontWeight: 'bold', 
    fontSize: 18, 
    marginLeft: 23
  },
  photoProfile: {
    elevation:1, marginHorizontal: 20, 
    width: "100%" , 
    height: heightScreen/3, 
    maxHeight: heightScreen/3, 
    marginVertical: heightScreen/23, 
    borderRadius: 14, 
    resizeMode: "contain" 
  },
  editProfile: {
    elevation:1, justifyContent:'center', 
    alignItems:'center', 
    backgroundColor: 'red', 
    position: 'absolute', 
    right: width / 10, 
    bottom: heightScreen/1.20, 
    width: width/12, 
    height: heightScreen/20, 
    borderRadius:8
  }
})

export default Profile
