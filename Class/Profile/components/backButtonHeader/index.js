import React, { Component } from 'react'
import { Text, StyleSheet, View, Dimensions } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';

const { width } = Dimensions.get("window")

class BackButtonHeader extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={styles.headerStyle}>
        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.goBackButton}>
          <Icon name="chevron-left" size={14} color="#C4122F" />
        </TouchableOpacity>
        <Text style={styles.nameProfile}>{this.props.titleHeader}</Text>
      </View>
    )
  }
}

export default BackButtonHeader;

const styles = StyleSheet.create({
  goBackButton: {
    marginLeft: width / 18, 
    backgroundColor: '#fff', 
    width: width / 12, 
    height: 30, 
    justifyContent: 'center', 
    alignItems: 'center', 
    borderRadius: 50, 
    elevation: 3 
  },
  nameProfile: {
    fontWeight: 'bold', 
    fontSize: 18, 
    marginLeft: 23
  },
  headerStyle: { 
    flexDirection: 'row', 
    alignItems: 'center',
  }
})
