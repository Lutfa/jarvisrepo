import React, { Component } from "react"
import {
  View,
  TextInput,
  Modal,
  ImageBackground,
  AppState,
  Text,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  StatusBar,
  AsyncStorage,
} from "react-native"
import BaseStyle from "./../BaseClass/BaseStyles"
import Constant from "./../BaseClass/Constant"
import images from "./../BaseClass/Images"
import { SafeAreaView } from "react-native-safe-area-context"
import SwipeListView from "./../ExtraClass/SwipeList/SwipeListView"
import SplashScreen from "react-native-splash-screen"
import MapView from "react-native-maps"
import { FlatList } from "react-native-gesture-handler"
import LinearGradient from "react-native-linear-gradient"
import {
  TextField,
  OutlinedTextField,
} from "react-native-material-textfield"
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"
import DateTimePicker from "@react-native-community/datetimepicker"
import moment from "moment"
import PickImage from "./../BaseClass/PickImage"
import urls from "./../BaseClass/ServiceUrls"
import Db from "./../DB/Realm"

const { width, height } = Dimensions.get("window")
const heightScreen = Dimensions.get("window").height;

const MAX_POINTS = 90

import { CommonActions } from "@react-navigation/native"
import language from "./../BaseClass/language"
import BackButtonHeader from "./components/backButtonHeader"
import Icon from 'react-native-vector-icons/FontAwesome5';

class EditProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      categoryIndex: 0,
      healthIssueArray: [],
      points: 80,
      name: global.userData.name,
     // height: `${global.userData.Height}`,
      //weight: `${global.userData.Weight}`,
      gender:
        global.userData.sex == null
          ? "Select"
          : global.userData.sex,
      bloodGroup:
        global.userData?.bloodGroup == null
          ? "Select"
          : global.userData.bloodGroup,
      email: "",
      nameError: "",
      emailError: "",
      healthIssue: "",
      showDate: false,
      dob:
        global.userData.dob == null
          ? "Select"
          : moment(global.userData.dob).format('DD-MM-YYYY'),
      selectedDate: "",
      showMenu: false,
      genderArray: ["Him/His", "Her/She"],
      menuType: "",
      menuArray: [],
      ProfilePictureId: 0,
      SCProofImageId: 0,
      PHCDocumentImageId: 0,
      bloodArray: [
        "O+",
        "O-",
        "A+",
        "A-",
        "B+",
        "B-",
        "AB+",
        "AB-",
      ],
      showPicker: false,
      imageType: 1, //1=profile, 2=sc, 3=hd
      // scUrl:
      //   global.userData.SCProof == null
      //     ? "null"
      //     : global.userData.SCProof,
      // hdUrl:
      //   global.userData.PHCDocument == null
      //     ? "null"
      //     : global.userData.PHCDocument,
      profileUrl:
        global.userData.pictureUrl == null
          ? "null"
          : global.userData.pictureUrl,
      isHome:
        props.route.params != null
          ? props.route.params.isHome
          : false,
    }
  }

  componentDidMount() {
    SplashScreen.hide()

    const userData = global.userData
    this.setState({
      healthIssueArray:
        userData.HealthIssues != null &&
          userData.HealthIssues != ""
          ? userData.HealthIssues.split(",")
          : [],
    })
  }

  render() {
    const fill =
      (this.state.points / MAX_POINTS) * 100
    const {
      name,
      isLoading,
      isHome,
      email,
      profileUrl,
      imageType,
      scUrl,
      hdUrl,
      showPicker,
      healthIssueArray,
      menuType,
      bloodArray,
      menuArray,
      nameError,
      dob,
      genderArray,
      showDate,
      emailError,
      gender,
      healthIssue,
      height,
      weight,
      bloodGroup,
    } = this.state

    const userData = global.userData

    return (
      <SafeAreaView style={{ flex: 1,backgroundColor:"#e5e5e5" }}>
        {isLoading ? Constant.showLoader() : null}
        <View style={{ marginTop: 10, flexDirection: 'row' }}>
          <BackButtonHeader navigation={this.props.navigation} titleHeader={"Edit Account Details"} />
        </View>

        <View style={{ alignItems: 'center', position: 'absolute', bottom: 0, width: width, height: heightScreen / 1.09, backgroundColor: '#fff', borderTopLeftRadius: 30, borderTopRightRadius: 30 }}>
          {/* Blank VIEW */}
          <View style={{ marginTop: heightScreen / 18, borderRadius: 30, width: width / 3, height: heightScreen / 6 }}>
            {profileUrl == "" ||
                  profileUrl == "null" ? (
                    <Image
                      resizeMode={"contain"}
                      style={{
                        borderRadius: 30, 
                        width: width / 3, 
                        height: heightScreen / 6
                      }}
                      source={images.user}
                    />
                  ) : (
                    <Image
                      style={{
                        borderRadius: 30, 
                        width: width / 3, 
                        height: heightScreen / 6
                      }}
                      source={{ uri: profileUrl }}
                    />
                  )}
          </View>

          <TouchableOpacity style={{position:'absolute', top: heightScreen/5}} onPress={() => {
            this.setState({
              showPicker: true,
              imageType: 1,
            })
          }}>
            <Text style={{ color: "#FE3C28", fontWeight: 'bold', marginTop: heightScreen / 40 }}>CHANGE PROFILE PHOTO</Text>
          </TouchableOpacity>


          <KeyboardAwareScrollView style={{ marginTop: heightScreen / 55 }}>
            <ScrollView style={{ width: width / 1.1 }}>
              <TextField
                label={language.name}
                value={name}
                lineWidth={1}
                fontSize={15}
                labelFontSize={13}
                labelTextStyle={[
                  { paddingTop: 5 },
                  BaseStyle.regularFont,
                ]}
                baseColor={"#000"}
                tintColor={"#000"}
                textColor={"#000"}
                onChangeText={(text) =>
                  this.setState({
                    name: text,
                    nameError: "",
                  })
                }
                error={nameError}
              />


              <TouchableOpacity
                activeOpacity={2}
                onPress={() => {
                  this.setState({
                    showDate: true,
                  })
                }}
                style={{
                  height: 50,
                  width: "100%",
                  marginTop: 20,
                }}>
                <Text
                  style={[
                    {
                      fontSize: 12,
                      color:
                        "#62646A",
                    },
                    BaseStyle.regularFont,
                  ]}>
                  {language.birthdate}
                </Text>
                <View
                  style={{
                    marginTop: 7,
                    paddingHorizontal: 20,
                    width: "100%",
                    alignItems: "center",
                    justifyContent:
                      "space-between",
                    flexDirection: "row",
                  }}>
                  <Text
                    style={[
                      {
                        fontSize: 14,
                        color:
                          "#8307FF",
                      },
                      BaseStyle.regularFont,
                    ]}>
                    {dob}
                  </Text>
                  <Icon name="calendar-day" size={14} color="#000" />
                </View>
                <View
                  style={{
                    backgroundColor:
                      "#000",
                    marginTop: 4,
                    height: 1,
                    width: "100%",
                  }}
                />
              </TouchableOpacity>



              <TouchableOpacity
                activeOpacity={2}
                onPress={() => {
                  this.setState({
                    menuType: "Gender",
                    showMenu: true,
                    menuArray: genderArray,
                  })
                }}
                style={{
                  height: 50,
                  width: "100%",
                  marginTop: 15,
                }}>
                <Text
                  style={[
                    {
                      fontSize: 12,
                      color:
                        "#62646A",
                    },
                    BaseStyle.regularFont,
                  ]}>
                  {language.gender}
                </Text>
                <View
                  style={{
                    marginTop: 7,
                    width: "100%",
                    paddingHorizontal: 20,
                    alignItems: "center",
                    justifyContent:
                      "space-between",
                    flexDirection: "row",
                  }}>
                  <Text
                    style={[
                      {
                        fontSize: 14,
                        color:
                          "#8307FF",
                      },
                      BaseStyle.regularFont,
                    ]}>
                    {gender}
                  </Text>
                  <Icon name="chevron-down" size={14} color="#000" />
                </View>
                <View
                  style={{
                    backgroundColor:
                      "#000",
                    marginTop: 4,
                    height: 1,
                    width: "100%",
                  }}
                />
              </TouchableOpacity>


              <TextField
                label={language.email}
                value={userData.email}
                editable={false}
                lineWidth={1}
                fontSize={15}
                labelFontSize={13}
                labelTextStyle={[
                  { paddingTop: 5 },
                  BaseStyle.regularFont,
                ]}
                baseColor={"#000"}
                tintColor={"#000"}
                textColor={"#000"}
              />

              <TextField
                label={language.phonenumber}
                value={userData.countryCode+"-"+userData.phoneNumber}
                 editable={false}
                lineWidth={1}
                fontSize={15}
                labelFontSize={13}
                labelTextStyle={[
                  { paddingTop: 5 },
                  BaseStyle.regularFont,
                ]}
                baseColor={"#000"}
                tintColor={"#000"}
                textColor={"#000"}
              />



              <View
                style={{
                  height: 60,
                  width: "38%",
                  marginTop: 15,
                }}>
                <Text
                  style={[
                    {
                      fontSize: 12,
                      color:
                        "#62646A",
                    },
                    BaseStyle.regularFont,
                  ]}>
                  {language.linkedAccount}
                </Text>
                <View
                  style={{
                    marginTop: 7,
                    width: "100%",
                    alignItems: "center",
                    justifyContent:
                      "space-between",
                    flexDirection: "row",
                  }}>

                  <Image source={require('../../assets/googleLogo.png')} style={{ width: 20, height: 20 }} />

                  {/* Facebook ICON down below */}
                  {/* <Image source={require('../../assets/facebookLogo.png')} style={{width: 20, height: 20}}/> */}
                  <Text
                    style={[
                      {
                        fontSize: 14,
                        color:
                          "#000",
                      },
                      BaseStyle.regularFont,
                    ]}>
                    xyz@gmail.com
                  </Text>
                </View>
              </View>
            </ScrollView>
          </KeyboardAwareScrollView>

          <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: 0, width: width, height: heightScreen / 12, backgroundColor: '#ebebeb', elevation: 10 }}>
            
            
            <TouchableOpacity onPress={() => { isLoading? null: this.checkServiceCall() }} >
                <LinearGradient
                              start={{ x: 0, y: 1 }}
                              end={{ x: 1, y: 1 }}

                              colors={[
                                Constant.gradientColor1(),
                                Constant.gradientColor2(),

                              ]}
                              style={{ marginHorizontal: 10, marginTop: 5, marginBottom: 10, borderRadius: 111,width: width / 1.1, height: heightScreen / 20, alignContent: "center", alignItems: "center", justifyContent: "center" }}>
                              <Text style={{ fontSize: 14, color: "#000" }}>SAVE CHANGES</Text>
                            </LinearGradient>
            </TouchableOpacity>
          </View>

        </View>

        {/* <View
          style={{
            backgroundColor: Constant.headerColor(),
            borderBottomLeftRadius: 30,
            borderBottomRightRadius: 30,
            paddingBottom: 50,
          }}>
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 15,
              backgroundColor:
                Constant.headerColor(),
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderBottomLeftRadius: 30,
              borderBottomRightRadius: 30,
            }}>
            {isHome ? null : (
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.pop(1)
                }}
                style={{
                  marginLeft: 15,
                  height: 35,
                  borderRadius: 15,
                  alignItems: "center",
                  justifyContent: "center",
                }}>
                <Image
                  tintColor="#fff"
                  style={{
                    height: 20,
                    width: 20,
                    left: 0,
                  }}
                  source={images.back}
                />
              </TouchableOpacity>
            )}
            <Text
              style={[
                { flex: 1 },
                BaseStyle.headerFont,
              ]}>
              {language.profile.toUpperCase()}
            </Text>

            <TouchableOpacity
              onPress={() => {
                isLoading
                  ? null
                  : this.checkServiceCall()
              }}
              style={{
                height: 34,
                width: 60,
                backgroundColor: "#fff",
                borderRadius: 17,
                marginRight: 15,
                justifyContent: "center",
                alignItems: "center",
              }}>
              <Text
                style={[
                  {
                    color: Constant.appColorAlpha(),
                  },
                  BaseStyle.boldFont,
                ]}>
                {language.save}
              </Text>
            </TouchableOpacity>
          </View>
        </View> */}

        {/* PROFILE */}

        {/* <KeyboardAwareScrollView
          style={{ marginTop: -55 }}>
          <ScrollView>
            <View>
              <View
                style={{
                  flexDirection: "row",
                  paddingLeft: 15,
                  paddingRight: 15,
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                <TouchableOpacity
                  activeOpacity={2}
                  onPress={() => {
                    this.setState({
                      showPicker: true,
                      imageType: 1,
                    })
                  }}
                  style={{
                    padding: 2,
                    backgroundColor: "#fff",
                    elevation: 4,
                    borderRadius: 47,
                    height: 94,
                    width: 94,
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  {profileUrl == "" ||
                  profileUrl == "null" ? (
                    <Image
                      resizeMode={"contain"}
                      style={{
                        height: 90,
                        width: 90,
                        borderRadius: 45,
                      }}
                      source={images.user}
                    />
                  ) : (
                    <Image
                      style={{
                        height: 90,
                        width: 90,
                        borderRadius: 45,
                      }}
                      source={{ uri: profileUrl }}
                    />
                  )}
                  <View
                    style={{
                      height: 24,
                      width: 24,
                      alignItems: "center",
                      justifyContent: "center",
                      position: "absolute",
                      bottom: 10,
                      right: 10,
                      backgroundColor:
                        Constant.appColorAlpha(),
                      borderWidth: 2,
                      borderColor: "#fff",
                      borderRadius: 12,
                      overflow: "hidden",
                      padding: 2,
                    }}>
                    <Image
                      tintColor={"#fff"}
                      resizeMode={"contain"}
                      style={{
                        height: 12,
                        width: 12,
                      }}
                      source={images.pencil}
                    />
                  </View>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  padding: 15,
                  marginTop: 5,
                  paddingTop: 0,
                  backgroundColor: "#fff",
                }}>
                <TextField
                  label={language.name}
                  value={name}
                  lineWidth={1}
                  fontSize={15}
                  labelFontSize={13}
                  labelTextStyle={[
                    { paddingTop: 5 },
                    BaseStyle.regularFont,
                  ]}
                  baseColor={Constant.appFullColor()}
                  tintColor={Constant.appFullColor()}
                  textColor={Constant.textColor()}
                  onChangeText={(text) =>
                    this.setState({
                      name: text,
                      nameError: "",
                    })
                  }
                  error={nameError}
                />

                <TextField
                  label={language.email}
                  value={userData.Email}
                  editable={false}
                  lineWidth={1}
                  fontSize={15}
                  labelFontSize={13}
                  labelTextStyle={[
                    { paddingTop: 5 },
                    BaseStyle.regularFont,
                  ]}
                  baseColor={Constant.appFullColor()}
                  tintColor={Constant.appFullColor()}
                  textColor={Constant.textColor()}
                />

                <TextField
                  label={language.phonenumber}
                  value={userData.PhoneNumber}
                  // editable={false}
                  lineWidth={1}
                  fontSize={15}
                  labelFontSize={13}
                  labelTextStyle={[
                    { paddingTop: 5 },
                    BaseStyle.regularFont,
                  ]}
                  baseColor={Constant.appFullColor()}
                  tintColor={Constant.appFullColor()}
                  textColor={Constant.textColor()}
                />

                <TouchableOpacity
                  activeOpacity={2}
                  onPress={() => {
                    this.setState({
                      menuType: "Gender",
                      showMenu: true,
                      menuArray: genderArray,
                    })
                  }}
                  style={{
                    height: 50,
                    width: "100%",
                    marginTop: 15,
                  }}>
                  <Text
                    style={[
                      {
                        fontSize: 12,
                        color:
                          Constant.appFullColor(),
                      },
                      BaseStyle.regularFont,
                    ]}>
                    {language.gender}
                  </Text>
                  <View
                    style={{
                      marginTop: 7,
                      width: "100%",
                      alignItems: "center",
                      justifyContent:
                        "space-between",
                      flexDirection: "row",
                    }}>
                    <Text
                      style={[
                        {
                          fontSize: 14,
                          color:
                            Constant.textColor(),
                        },
                        BaseStyle.regularFont,
                      ]}>
                      {gender}
                    </Text>
                    <Image
                      resizeMode={"contain"}
                      style={{
                        height: 12,
                        width: 12,
                      }}
                      source={images.leftarrow}
                    />
                  </View>
                  <View
                    style={{
                      backgroundColor:
                        Constant.appColorAlpha(),
                      marginTop: 4,
                      height: 1,
                      width: "100%",
                    }}
                  />
                </TouchableOpacity>

                <TouchableOpacity
                  activeOpacity={2}
                  onPress={() => {
                    this.setState({
                      showDate: true,
                    })
                  }}
                  style={{
                    height: 50,
                    width: "100%",
                    marginTop: 20,
                  }}>
                  <Text
                    style={[
                      {
                        fontSize: 12,
                        color:
                          Constant.appFullColor(),
                      },
                      BaseStyle.regularFont,
                    ]}>
                    {language.birthdate}
                  </Text>
                  <View
                    style={{
                      marginTop: 7,
                      width: "100%",
                      alignItems: "center",
                      justifyContent:
                        "space-between",
                      flexDirection: "row",
                    }}>
                    <Text
                      style={[
                        {
                          fontSize: 14,
                          color:
                            Constant.textColor(),
                        },
                        BaseStyle.regularFont,
                      ]}>
                      {dob}
                    </Text>
                    <Image
                      resizeMode={"contain"}
                      style={{
                        height: 12,
                        width: 12,
                      }}
                      source={images.leftarrow}
                    />
                  </View>
                  <View
                    style={{
                      backgroundColor:
                        Constant.appColorAlpha(),
                      marginTop: 4,
                      height: 1,
                      width: "100%",
                    }}
                  />
                </TouchableOpacity>

                <TextField
                  label={language.height + "(Cm)"}
                  value={height}
                  lineWidth={1}
                  fontSize={15}
                  keyboardType="numeric"
                  labelFontSize={13}
                  labelTextStyle={[
                    { paddingTop: 5 },
                    BaseStyle.regularFont,
                  ]}
                  baseColor={Constant.appFullColor()}
                  tintColor={Constant.appFullColor()}
                  textColor={Constant.textColor()}
                  formatText={this.formatText}
                  onChangeText={(text) => {
                    this.setState({ height: text })
                  }}
                />

                <TouchableOpacity
                  activeOpacity={2}
                  onPress={() => {
                    this.setState({
                      menuType: "Blood Group",
                      showMenu: true,
                      menuArray: bloodArray,
                    })
                  }}
                  style={{
                    height: 50,
                    width: "100%",
                    marginTop: 20,
                  }}>
                  <Text
                    style={[
                      {
                        fontSize: 12,
                        color:
                          Constant.appFullColor(),
                      },
                      BaseStyle.regularFont,
                    ]}>
                    {language.bloodGroup}
                  </Text>
                  <View
                    style={{
                      marginTop: 7,
                      width: "100%",
                      alignItems: "center",
                      justifyContent:
                        "space-between",
                      flexDirection: "row",
                    }}>
                    <Text
                      style={[
                        {
                          fontSize: 14,
                          color:
                            Constant.textColor(),
                        },
                        BaseStyle.regularFont,
                      ]}>
                      {bloodGroup}
                    </Text>
                    <Image
                      resizeMode={"contain"}
                      style={{
                        height: 12,
                        width: 12,
                      }}
                      source={images.leftarrow}
                    />
                  </View>
                  <View
                    style={{
                      backgroundColor:
                        Constant.appColorAlpha(),
                      marginTop: 4,
                      height: 1,
                      width: "100%",
                    }}
                  />
                </TouchableOpacity>

                <TextField
                  label={language.weight + "(Kg)"}
                  value={weight}
                  lineWidth={1}
                  fontSize={15}
                  keyboardType="numeric"
                  labelFontSize={13}
                  labelTextStyle={[
                    { paddingTop: 5 },
                    BaseStyle.regularFont,
                  ]}
                  baseColor={Constant.appFullColor()}
                  tintColor={Constant.appFullColor()}
                  textColor={Constant.textColor()}
                  onChangeText={(text) =>
                    this.setState({ weight: text })
                  }
                />

                {healthIssueArray.length > 0 ? (
                  <View
                    style={{
                      padding: 0,
                      paddingTop: 10,
                    }}>
                    <FlatList
                      data={healthIssueArray}
                      horizontal={true}
                      renderItem={({
                        item,
                        index,
                      }) => (
                        <TouchableOpacity
                          onPress={() => {
                            const newArray =
                              healthIssueArray.filter(
                                (
                                  item,
                                  newIndex
                                ) => {
                                  return (
                                    index !=
                                    newIndex
                                  )
                                }
                              )
                            this.setState({
                              healthIssueArray:
                                newArray,
                            })
                          }}
                          style={{
                            height: 36,
                            flexDirection: "row",
                            padding: 5,
                            marginRight: 6,
                            alignItems: "center",
                            justifyContent:
                              "center",
                            backgroundColor:
                              Constant.headerColor(),
                            borderColor:
                              Constant.headerColor(),
                            borderRadius: 18,
                          }}>
                          <Text
                            numberOfLines={1}
                            style={[
                              {
                                fontSize: 14,
                                color: "#fff",
                                textAlign: "center",
                              },
                              BaseStyle.boldFont,
                            ]}>
                            {item}
                          </Text>

                          <Image
                            tintColor={"#fff"}
                            style={{
                              height: 12,
                              width: 12,
                              marginLeft: 6,
                            }}
                            source={images.close}
                          />
                        </TouchableOpacity>
                      )}
                    />
                  </View>
                ) : null}

                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                  }}>
                  <View style={{ flex: 1 }}>
                    <TextField
                      label={language.healthissues}
                      value={healthIssue}
                      lineWidth={1}
                      ref={(ref) =>
                        (this.healthTextField = ref)
                      }
                      fontSize={15}
                      labelFontSize={13}
                      labelTextStyle={[
                        { paddingTop: 5 },
                        BaseStyle.regularFont,
                      ]}
                      baseColor={Constant.appFullColor()}
                      tintColor={Constant.appFullColor()}
                      textColor={Constant.textColor()}
                      onChangeText={(text) =>
                        this.setState({
                          healthIssue: text,
                        })
                      }
                    />
                  </View>

                  {healthIssue.length > 0 ? (
                    <View
                      style={{
                        width: 80,
                        alignItems: "center",
                        justifyContent: "center",
                      }}>
                      <TouchableOpacity
                        style={{
                          alignItems: "center",
                          borderRadius: 6,
                          justifyContent: "center",
                          padding: 10,
                          backgroundColor:
                            Constant.headerColor(),
                        }}
                        onPress={() => {
                          this.healthTextField.clear()
                          this.setState({
                            healthIssueArray: [
                              ...this.state
                                .healthIssueArray,
                              healthIssue,
                            ],
                            healthIssue: "",
                          })
                        }}>
                        <Text
                          style={[
                            BaseStyle.boldFont,
                            {
                              color: "#fff",
                              width: 50,
                              textAlign: "center",
                            },
                          ]}>
                          {language.Add}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  ) : null}
                </View>

                <View
                  style={{
                    width: "100%",
                    marginTop: 20,
                  }}>
                  <Text
                    style={[
                      {
                        fontSize: 12,
                        color:
                          Constant.appFullColor(),
                      },
                      BaseStyle.regularFont,
                    ]}>
                    {language.seniorDocument}
                  </Text>
                  <TouchableOpacity
                    activeOpacity={2}
                    onPress={() => {
                      this.setState({
                        showPicker: true,
                        imageType: 2,
                      })
                    }}
                    style={{
                      marginTop: 7,
                      height: 40,
                      padding: 5,
                      paddingRight: 12,
                      paddingLeft: 12,
                      borderRadius: 6,
                      width: "50%",
                      backgroundColor:
                        Constant.headerColor(),
                      alignItems: "center",
                      justifyContent:
                        "space-between",
                      flexDirection: "row",
                    }}>
                    <Text
                      style={[
                        {
                          fontSize: 14,
                          color: "#fff",
                        },
                        BaseStyle.boldFont,
                      ]}>
                      {language.browseDocument}
                    </Text>
                    <Image
                      style={{
                        height: 20,
                        width: 20,
                      }}
                      source={images.download}
                    />
                  </TouchableOpacity>

                  {scUrl != "" &&
                  scUrl != "null" ? (
                    <Image
                      style={{
                        height: 200,
                        width: "100%",
                        marginTop: 10,
                      }}
                      source={{ uri: scUrl }}
                    />
                  ) : null}
                </View>

                <View
                  style={{
                    width: "100%",
                    marginTop: 20,
                  }}>
                  <Text
                    style={[
                      {
                        fontSize: 12,
                        color:
                          Constant.appFullColor(),
                      },
                      BaseStyle.regularFont,
                    ]}>
                    {language.handicappedDocument}
                  </Text>
                  <TouchableOpacity
                    activeOpacity={2}
                    onPress={() => {
                      this.setState({
                        showPicker: true,
                        imageType: 3,
                      })
                    }}
                    style={{
                      marginTop: 7,
                      height: 40,
                      padding: 5,
                      paddingRight: 12,
                      paddingLeft: 12,
                      borderRadius: 6,
                      width: "50%",
                      backgroundColor:
                        Constant.headerColor(),
                      alignItems: "center",
                      justifyContent:
                        "space-between",
                      flexDirection: "row",
                    }}>
                    <Text
                      style={[
                        {
                          fontSize: 14,
                          color: "#fff",
                        },
                        BaseStyle.boldFont,
                      ]}>
                      {language.browseDocument}
                    </Text>
                    <Image
                      style={{
                        height: 20,
                        width: 20,
                      }}
                      source={images.download}
                    />
                  </TouchableOpacity>
                  {hdUrl != "" &&
                  hdUrl != "null" ? (
                    <Image
                      style={{
                        height: 200,
                        width: "100%",
                        marginTop: 10,
                      }}
                      source={{ uri: hdUrl }}
                    />
                  ) : null}
                </View>
              </View>
            </View>
          </ScrollView>
        </KeyboardAwareScrollView> */}

        {showDate ? (
          <DateTimePicker
            testID="dateTimePicker"
            value={new Date()}
            mode={"date"}
            is24Hour={true}
            maximumDate={new Date()}
            display="default"
            onChange={(event, selectedDate) => {
              this.setState({
                originalDate:
                  selectedDate != null
                    ? selectedDate
                    : "",
                dob:
                  selectedDate != null
                    ? moment(selectedDate).format(
                      "DD/MM/YYYY"
                    )
                    : "Select",
                showDate: false,
              })
            }}
          />
        ) : null}

        {showPicker ? (
          <PickImage
            selectedImage={this.selectedImage}
            profile={
              this.state.imageType == 1
                ? true
                : false
            }
            onBack={this.hidePicker}
          />
        ) : null}

        {this.showBottomMenu(menuType, menuArray)}

        <StatusBar barStyle="dark-content" translucent backgroundColor="white" />
      </SafeAreaView>
    )
  }

  selectedImage = (url) => {
    if (this.state.imageType == 1) {
      this.setState({
        profileUrl: url,
        showPicker: false,
      })
    } else if (this.state.imageType == 2) {
      this.setState({
        scUrl: url,
        showPicker: false,
      })
    } else {
      this.setState({
        hdUrl: url,
        showPicker: false,
      })
    }
  }

  formatText = (text) => {
    let newText = ""
    let numbers = ".0123456789"

    for (var i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        newText = newText + text[i]
      } else {
      }
    }

    return newText
  }

  hidePicker = () => {
    this.setState({ showPicker: false })
  }
  setMenuData(header, index) {
    this.setState({ showMenu: false })
    if (header == "Gender") {
      this.setState({
        gender: this.state.genderArray[index],
      })
    } else {
      this.setState({
        bloodGroup: this.state.bloodArray[index],
      })
    }
  }

  showBottomMenu(header, data) {
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={this.state.showMenu}
        onRequestClose={() => { }}>
        <TouchableOpacity
          style={{
            flex: 1,
            height: height,
            width: width,
            position: "absolute",
            bottom: 0,
            backgroundColor: "rgba(0,0,0,0.5)",
          }}
          onPress={() => {
            this.setState({ showMenu: false })
          }}></TouchableOpacity>
        <View
          style={{
            width: width,
            bottom: 0,
            position: "absolute",
            backgroundColor: "#fff",
          }}>
          <View
            style={{
              width: width,
              padding: 10,
              flexDirection: "column",
            }}>
            <View
              style={{
                height: 50,
                justifyContent: "center",
                width: "100%",
                borderBottomWidth: 0.5,
                borderBottomColor:
                  Constant.appColorAlpha(),
              }}>
              <Text style={[BaseStyle.regularFont]}>
                {header}
              </Text>
            </View>

            {data.map((item, index) => {
              return (
                <TouchableOpacity
                  style={{
                    height: 40,
                    justifyContent: "center",
                    width: width,
                  }}
                  onPress={() => {
                    this.setMenuData(header, index)
                  }}>
                  <Text
                    style={[BaseStyle.regularFont]}>
                    {item}
                  </Text>
                </TouchableOpacity>
              )
            })}
          </View>
        </View>
      </Modal>
    )
  }

  uploadImage(url, count) {


    Constant.uploadImageToServer(
      urls.mediaUpload+Constant.pharmacyId(),
      url,
      (result) => {
        if (result.success) {
          console.log("image res "+JSON.stringify(result))
          let dic = {
            pictureUrl: result.url.response.pictureUrl,
            pictureId:
              result.url.response.pictureId,
          }

          this.setState(
            {
              profileUrl: dic.pictureUrl,
              ProfilePictureId: dic.pictureId,
            }),
            this.serviceCall()
            
          }
        
      })

    // Constant.uploadImageToServer(
    //   urls.mediaUpload,
    //   url,
    //   (result) => {
    //     if (result.success) {
    //       const PictureUrl = result.url.Response.Url
    //       const id = result.url.Response.PictureId
    //       const { profileUrl, scUrl, hdUrl } =
    //         this.state
    //       const userData = global.userData

    //       if (count == 1) {
    //         //profile
    //         this.setState(
    //           {
    //             profileUrl: PictureUrl,
    //             ProfilePictureId: id,
    //           },
    //           function () {
    //             if (scUrl != userData.SCProof) {
    //               this.uploadImage(scUrl, 2)
    //             } else if (
    //               hdUrl != userData.PHCDocument
    //             ) {
    //               this.uploadImage(hdUrl, 3)
    //             } else {
    //               this.serviceCall()
    //             }
    //           }
    //         )
    //       } else if (count == 2) {
    //         this.setState(
    //           {
    //             scUrl: PictureUrl,
    //             SCProofImageId: id,
    //           },
    //           function () {
    //             if (hdUrl != userData.PHCDocument) {
    //               this.uploadImage(hdUrl, 3)
    //             } else {
    //               this.serviceCall()
    //             }
    //           }
    //         )
    //       } else {
    //         this.setState(
    //           {
    //             hdUrl: PictureUrl,
    //             PHCDocumentImageId: id,
    //           },
    //           function () {
    //             this.serviceCall()
    //           }
    //         )
    //       }
    //     }
    //   }
    // )
  }

  checkServiceCall() {
    const { profileUrl, scUrl, hdUrl } = this.state
    const userData = global.userData

    this.setState({ isLoading: true })

    if (
      profileUrl != userData.ProfilePicture &&
      profileUrl != "null"
    ) {
      console.log("image uploading")

      this.uploadImage(profileUrl, 1)
    } else if (
      scUrl != userData.SCProof &&
      scUrl != "null"
    ) {
      this.uploadImage(scUrl, 2)
    } else if (
      hdUrl != userData.PHCDocument &&
      hdUrl != "null"
    ) {
      this.uploadImage(hdUrl, 3)
    } else {
      console.log("Service call")

      this.serviceCall()
    }
  }

  serviceCall() {
    console.log("servicecall")
    const {
      name,
      profileUrl,
      scUrl,
      hdUrl,
      dob,
      gender,
      healthIssueArray,
      healthIssue,
      height,
      weight,
      bloodGroup,
      ProfilePictureId,
      SCProofImageId,
      PHCDocumentImageId,
     
    } = this.state
    const userData = global.userData
    // var healthIssueString = ""
    // if (healthIssueArray.length > 0) {
    //   healthIssueArray.map((item) => {
    //     healthIssueString =
    //       healthIssueString == ""
    //         ? item
    //         : healthIssueString + "," + item
    //   })
    // }
    var parameters = {
      id: userData.id,
      name: name,
      pictureUrl: profileUrl,
      pharmacyId: Constant.pharmacyId(),
      phoneNumber:userData.phoneNumber,
      countryCode:userData.countryCode,
      // BloodGroup:
      //   bloodGroup == "Select" ? "" : bloodGroup,
      // Height: height,
      // Weight: weight,
      sex: gender == "Select" ? "" : gender,
     // HealthIssues: healthIssueString,
     // SCProof: scUrl,
     // PHCDocument: hdUrl,
      IsPhoneVerified: true,
    }
    if (dob != "Select" && dob != "Invalid date") {
      parameters = {
        ...parameters,
        ...{ age: dob },
      }
    }
    if (ProfilePictureId != 0) {
      parameters = {
        ...parameters,
        ...{ pictureId: ProfilePictureId },
      }
    }
    // if (SCProofImageId != 0) {
    //   parameters = {
    //     ...parameters,
    //     ...{ SCProofImageId: SCProofImageId },
    //   }
    // }
    // if (PHCDocumentImageId != 0) {
    //   parameters = {
    //     ...parameters,
    //     ...{
    //       PHCDocumentImageId: PHCDocumentImageId,
    //     },
    //   }
    // }
    console.log("log", parameters)
    Constant.postWithTokenMethod(
      urls.profileUpdate,
      parameters,
      (result) => {
        this.setState({ isLoading: false })

        if (result.success) {
          let response = result.result

          if (response.status != "Fail") {
            console.log("profile res",JSON.stringify(response))
            let userId =
              response.response.patientData.id


            let userData = response.response.patientData
            global.userData = userData
            this.setState({ isLoading: false })
            // this.saveUserData(response.Response)

            try {
              Db.write(() => {
                Db.create(
                  "User",
                  {
                    
                    id: userData.id,
                    name: userData.name,
                    email: userData.email,
                    phoneNumber:
                      userData.phoneNumber,
                 deviceType: userData?.deviceType,
                   // Latitude: userData.Latitude,
                   // Longitude: userData.Longitude,
                    // IsPhoneVerified:
                    //   userData.isPhoneVerified,
                    countryCode:
                      userData.countryCode,
                      pictureUrl:
                      userData.pictureUrl,

                      age: userData.age,
                      sex: userData.sex,
                      temparature: userData.temparature,
                     
                      fastingBloodSugar: userData.fastingBloodSugar,
                      bloodSugar:  userData.bloodSugar,
                      bloodPressureDia: userData.bloodPressureDia,
                      bloodPressureSys: userData.bloodPressureSys,
                      pulse: userData.pulse,
                      deviceUniqueId: userData.deviceUniqueId,
                      token: global.userToken,

                
                    // Dob: userData.Dob,
                    // Gender: userData.Gender,
                    // HealthIssues:
                    //   userData.HealthIssues,
                    // SCProof: userData.SCProof,
                    // SCVerified: userData.SCVerified,
                    // PHCDocument:
                    //   userData.PHCDocument,
                    // PHCVerified:
                    //   userData.PHCVerified,
                    // FirstName: userData.FirstName,
                    // LastName: userData.LastName,
                    // DeviceId: userData.deviceId,
                    // DeviceUniqueId:
                    //   userData.deviceUniqueId,
                    // AuthToken: userData.AuthToken,
                    // ReferralCode:
                    //   userData.ReferralCode,
                    // Version: userData.version,
                    // DeviceModel:
                    //   userData.deviceModel,
                    // DeviceBrand:
                    //   userData.deviceBrand,
                    // DeviceVersion:
                    //   userData.deviceVersion,
                  },
                  "modified"
                )

                AsyncStorage.setItem(
                  "userLoggedIn",
                  "true"
                )
                const value =  AsyncStorage.getItem(
                  "userLoggedIn"
                )
               
                this.props.navigation.pop(1)
                
              })
            } catch (e) {
              console.log("Error on creation"+e)
              Toast.show("Unable to save data")
            }
          } else {
            Toast.show("Something went wrong")
          }
        }
      }
    )
  }


  async   profileUpdate() {
    const {
      name,
      profileUrl,
      scUrl,
      hdUrl,
      dob,
      gender,
      healthIssueArray,
      healthIssue,
      height,
      weight,
      bloodGroup,
      ProfilePictureId,
      SCProofImageId,
      PHCDocumentImageId,
    } = this.state


    var parameters = {
      
      id: global.CustomerId,
      pharmacyId: Constant.pharmacyId(),
      name: name,

      sex: gender == "Select" ? "" : gender,
      email: email,
      countryCode: countryCode,
      phoneNumber: phoneNumber,


      isPhoneVerified: true,


    }



    console.log("log", parameters)
    Constant.postWithTokenMethod(
      urls.profileUpdate,
      parameters,
      (result) => {
        console.log("profileupdate", JSON.stringify(result))
        this.setState({ isLoading: false })

        if (result.success) {
          let response = result.result

          if (response.status != "Fail") {
            let userId =
              response.response.patientData.id


            let userData = response.response.patientData
            global.userData = userData

            // this.saveUserData(response.Response)

            try {
              Db.write(() => {
                Db.create(
                  "User",
                  {
                    
                    id: userData.id,
                    name: userData.name,
                    email: userData.email,
                    phoneNumber:
                      userData.phoneNumber,
                 deviceType: userData?.deviceType,
                   // Latitude: userData.Latitude,
                   // Longitude: userData.Longitude,
                    // IsPhoneVerified:
                    //   userData.isPhoneVerified,
                    countryCode:
                      userData.countryCode,
                      pictureUrl:
                      userData.pictureUrl,

                      age: userData.age,
                      sex: userData.sex,
                      temparature: userData.temparature,
                     
                      fastingBloodSugar: userData.fastingBloodSugar,
                      bloodSugar:  userData.bloodSugar,
                      bloodPressureDia: userData.bloodPressureDia,
                      bloodPressureSys: userData.bloodPressureSys,
                      pulse: userData.pulse,
                      deviceUniqueId: userData.deviceUniqueId,
                      token: global.userToken,

                
                    // Dob: userData.Dob,
                    // Gender: userData.Gender,
                    // HealthIssues:
                    //   userData.HealthIssues,
                    // SCProof: userData.SCProof,
                    // SCVerified: userData.SCVerified,
                    // PHCDocument:
                    //   userData.PHCDocument,
                    // PHCVerified:
                    //   userData.PHCVerified,
                    // FirstName: userData.FirstName,
                    // LastName: userData.LastName,
                    // DeviceId: userData.deviceId,
                    // DeviceUniqueId:
                    //   userData.deviceUniqueId,
                    // AuthToken: userData.AuthToken,
                    // ReferralCode:
                    //   userData.ReferralCode,
                    // Version: userData.version,
                    // DeviceModel:
                    //   userData.deviceModel,
                    // DeviceBrand:
                    //   userData.deviceBrand,
                    // DeviceVersion:
                    //   userData.deviceVersion,
                  },
                  "modified"
                )

                AsyncStorage.setItem(
                  "userLoggedIn",
                  "true"
                )
                const value =  AsyncStorage.getItem(
                  "userLoggedIn"
                )
                console.log
                if( global.userData.name!=""){
                  this.props.navigation.pop(
                   1
                  )
                }else{
                  this.props.navigation.push("Register",{
                   
                    number: this.state.phoneNumber,
                    countryCode: this.state.countryCode,

                  }
                  )
                }
                
              })
            }catch (e) {
              console.log("Error on creation")
              Toast.show("Unable to save data")
            }
          } else {
            Toast.show("Something went wrong")
          }
        }
      }
    )
  }
}

export default EditProfile
