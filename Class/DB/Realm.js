var Realm = require("realm")



const DiscountSchema = {
  name: "BestDiscount",
  primaryKey: "Id",
  properties: {
    Id: "int?",
    Name: "string?",
    DiscountTypeId: "int?",
    UsePercentage: "bool?",
    DiscountPercentage: "int?",
    DiscountAmount: "int?",
    MaximumDiscountAmount: "int?",
    StartDateUtc: "string?",
    EndDateUtc: "date?",
    RequiresCouponCode: "bool?",
    CouponCode: "string?",
    IsCumulative: "bool?",
    LimitationTimes: "int?",
    MaximumDiscountedQuantity: "int?",
    MinOrderTotal: "int?",
    Description: "string?",
    CreatedOnUtc: "string?",
    UpdatedOnUtc: "string?",
    PictureId: "int?",
    PictureUrl: "string?",
  },
}



const ImageSchema = {
  name: "ImageSchemas",
  properties: {
    Id: "int?",
    PictureId: "int?",
    DisplayOrder: "int?",
    Src: "string?",
  },
}

const IdSchema = {
  name: "IdSchemas",
  properties: {
    id: "int?",
  },
}

const PersonSchema = {
  name: "User",
  primaryKey: "id",
  properties: {


   

    id: "int", // required property
    iname: "string?", // optional property
    email: "string?",
    phoneNumber: "string?",
    deviceType: "string?",
    latitude: "string?",
    longitude: "string?",
    isPhoneVerified: "bool?",
    countryCode: "string?",
    pictureUrl: "string?",
    bloodGroup: "string?",
    height: "string?",
    weight: "string?",
    age: "string?",
    sex: "string?",
    temparature: "string?",
    SCVerified: "bool?",
    fastingBloodSugar: "string?",
    bloodSugar: "bool?",
    bloodPressureDia: "string?",
    bloodPressureSys: "string?",
    pulse: "string?",
    DeviceUniqueId: "string?",
    token: "string?",
    allergies: "string?",
    version: "string?",
    deviceModel: "string?",
    deviceBrand: "string?",
    deviceVersion: "string?",
  },
}




const BannersSchema = {
  name: "Banners",
  primaryKey: "PictureId",
  properties: {
    BannerTypeId: "int?",
    BannerPositionId: "int?",
    ImageUrl: "string?",
    RedirectUrl: "string?",
    IsActive: "bool?",
    FromDate: "string?",
    ToDate: "string?",
    DisplayOrder: "int?",
    CategoryId: "int?",
    PictureId: "int?",
    NavigationId: "int?",
    SelectedCategoryId: "int?",
    SelectedDiscountId: "int?",
    BannerTypeName: "string?",
    Id: "int?",
    BannerPositionName: "string?",
  },
}





const articlesSchema = {
  name: "Articles",
  primaryKey: "Id",
  properties: {
    HasLikedIt: "bool?",
    HasBookmarkedIt: "bool?",
    Id: "int?",
    Title: "string?",
    Description: "string?",
    Image: "string?",
    CreatedOnUtc: "date?",
    LikesCount: "int?",
    BookmarksCount: "int?",
    IsActive: "bool?",
    PictureId: "int?",
    CategoryIds: "IdSchemas[]",
  },
}

const addressSchema = {
  name: "Address",
  primaryKey: "AddressId",
  properties: {
    FirstName: "string?",
    LastName: "string?",
    Email: "string?",
    Company: "string?",
    CountryId: "int?",
    StateProvinceId: "int?",
    County: "string?",
    City: "string?",
    Address1: "string?",
    Address2: "string?",
    ZipPostalCode: "string?",
    PhoneNumber: "string?",
    FaxNumber: "string?",
    CustomAttributes: "string?",
    CreatedOnUtc: "string?",
    AddressCategory: "string?",
    Longitude: "string?",
    Latitude: "string?",
    LocationName: "string?",
    AddressId: "int?",
    CustomerId: "int?",
    DeviceUniqueId: "string?",
    Landmark: "string?",
  },
}



const healthRecordSchema = {
  name: "HealthRecord",
  primaryKey: "Id",
  properties: {
    Id: "int?",
    CustomerId: "int?",
    Title: "string?",
    Description: "string?",
    HealthRecordType: "string?",
    HealthRecordDoctor: "string?",
    HealthRecordPatient: "string?",
    LastModifiedDate: "string?",
    CreatedOn: "string?",
    HealthRecordImages: "RecordImage[]",
  },
}

const RecordImageSchema = {
  name: "RecordImage",
  properties: {
    HealthRecordId: "int?",
    PictureUrl: "string?",
    Id: "int?",
    PictureId: "int?",
  },
}

const SearchSchema = {
  name: "SearchHistory",
  primaryKey: "Id",
  properties: {
    Id: "int?",
    Name: "string?",
    ShortDescription: "string?",
    AdminComment: "string?",
    Price: "float?",
    OldPrice: "float?",
    PriceWithDiscount: "float?",
    Sku: "string?",
    ColorCode: "string?",
    PriceValue: "string?",
    ManufactureName: "string?",
    GenericName: "string?",
    SCorPHCDiscount: "float?",
    PriceWithSCorPHCDiscount: "float?",
    Images: "ImageSchemas[]",
  },
}

const pillReminderSchema = {
  name: "PillReminder",
  primaryKey: "Id",
  properties: {
    Id: "int?",
    CustomerId: "int?",
    PillName: "string?",
    PillType: "string?",
    ShapeType: "string?",
    ColorCode: "string?",
    Dosage: "string?",
    DoseTimes: "string?",
    Image: "string?",
    Alarm: "string?",
    CreatedOnUtc: "string?",
    IsDeleted: "bool?",
    IsCompleted: "bool?",
    IsModulerView: "bool?",
    IsRecurring: "bool?",
    IsSelectAll: "bool?",
    RecurringValue: "string?",
    Brand: "string?",
    Generic: "string?",
    PillReminderSlots: "Pill[]",
  },
}

const PillSchema = {
  name: "Pill",
  primaryKey: "Id",
  properties: {
    Id: "int?",
    PillReminderId: "int?",
    Day: "date?",
    Time: "string?",
    IsSkipped: "bool?",
    IsTaken: "bool?",
    CreatedOnUtc: "string?",
    UpdatedOnUtc: "string?",
  },
}










let realm = new Realm({
  schema: [
   
    PersonSchema,
    DiscountSchema,
    ImageSchema,
    
    IdSchema,
    BannersSchema,
    
   
    articlesSchema,
    addressSchema,
  
    RecordImageSchema,
    healthRecordSchema,
    SearchSchema,
    PillSchema,
    pillReminderSchema,
    
   
  ],
})

export default realm
