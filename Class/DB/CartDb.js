import Db from "./Realm"
import moment from "moment"
class CartDB {
  async addData(item) {
    try {
      await Db.write(() => {
        let newProduct = Db.create(
          "CartDB",
          {
            Id: item.Id,
            Name: item.Name,
            ShortDescription: item.ShortDescription,
            DisplayOrder: item.DisplayOrder,
            Price: parseFloat(item.Price),
            OldPrice: parseFloat(item.OldPrice),
            PriceWithDiscount:
              item.PriceWithDiscount != null
                ? parseFloat(item.PriceWithDiscount)
                : 0,
            Sku: item.Sku,
            PriceValue: item.PriceValue,
            ProductCost: item.ProductCost,
            OrderMinimumQuantity:
              item.OrderMinimumQuantity,
            OrderMaximumQuantity:
              item.OrderMaximumQuantity,
            DisplayOrder: item.DisplayOrder,
            IsTaxExempt: item.IsTaxExempt,
            TaxCategoryId: item.TaxCategoryId,
            Published: item.Published,
            Deleted: item.Deleted,
            CreatedOnUtc: item.CreatedOnUtc,
            UpdatedOnUtc: item.UpdatedOnUtc,
            ColorCode: item.ColorCode,
            GenericCode: item.GenericCode,
            PackageDescription:
              item.PackageDescription,
            PackageInNumbers: item.PackageInNumbers,
            PricePerUnit: item.PricePerUnit,
            TherapeuticName: item.TherapeuticName,
            GenericName: item.GenericName,
            AdultDosage: item.AdultDosage,
            ChildrenDosage: item.ChildrenDosage,
            Indication: item.Indication,
            ContraIndication: item.ContraIndication,
            SideEffectsOrAdverseEffects:
              item.SideEffectsOrAdverseEffects,
            PrecautionOrWarning:
              item.PrecautionOrWarning,
            DrugInteractions: item.DrugInteractions,
            RecomandAdultDosage:
              item.RecomandAdultDosage,
            RecomandChildrenDosage:
              item.RecomandChildrenDosage,
            PregnancyAlert: item.PregnancyAlert,
            LactationAlert: item.LactationAlert,
            DrivingAlert: item.DrivingAlert,
            LiverAlert: item.LiverAlert,
            KidneyAlert: item.KidneyAlert,
            ManufacturerName: item.ManufacturerName,
            SymptomsAndOverdoseTreatment:
              item.SymptomsAndOverdoseTreatment,
            StorageCondition: item.StorageCondition,
            AvailabiltyPackage:
              item.AvailabiltyPackage,
            ManufacturerId: item.ManufacturerId,
            IsPriceIncludingTax:
              item.IsPriceIncludingTax,
            TaxId: item.TaxId,
            DiscountIds: item.DiscountIds,
            ManufacturerIds:
              item.ManufacturerIds != null
                ? item.ManufacturerIds
                : [],
            Images: item.Images,
          },
          "modified"
        )
        return true
      })
    } catch (e) {
      console.log("Error on creation")
      console.log(e)

      return false
    }
  }

  async delete(id) {
    try {
      await Db.write(() => {
        Db.delete(
          Db.objectForPrimaryKey("CartDB", id)
        )

        return true
      })
    } catch (e) {
      console.log("Error on creatione")
      console.log(JSON.stringify(e))
      return false
    }
  }

  Count() {
    return Db.objects("CartDB").length
  }

  WishListCount() {
    return Db.objects("WishListDB").length
  }

  async deleteDb(dbName) {
    try {
      await Db.write(() => {
        let objects = Db.objects(dbName)
        Db.delete(objects)
        return true
      })
    } catch (e) {
      console.log("Error on creatione")
      console.log(JSON.stringify(e))
      return false
    }
  }

  async deleteWishList(id) {
    try {
      await Db.write(() => {
        Db.delete(
          Db.objectForPrimaryKey("WishListDB", id)
        )

        return true
      })
    } catch (e) {
      console.log("Error on creatione")
      console.log(JSON.stringify(e))
      return false
    }
  }

  productExistInWishList(id) {
    let data = Db.objectForPrimaryKey(
      "WishListDB",
      id
    )
    return data != null
  }

  CartDataCount(id) {
    let data = Db.objectForPrimaryKey("CartDB", id)
    return data != null ? data.count : 0
  }

  productExist(id) {
    let data = Db.objectForPrimaryKey("CartDB", id)
    return data != null
  }

  async addCartData(item, count) {
    try {
      await Db.write(() => {
        let newProduct = Db.create(
          "CartDB",
          {
            Id: item.Id,
            Name: item.Name,
            ShortDescription: item.ShortDescription,
            FullDescription: item.FullDescription,
            AdminComment: item.AdminComment,
            DisplayOrder: item.DisplayOrder,
            Price: parseFloat(
              !!item.Price ? item.Price : 0
            ),
            OldPrice: parseFloat(
              item.OldPrice ? item.OldPrice : 0
            ),
            Sku: item.Sku,
            PriceValue: item.PriceValue,
            ProductCost: parseFloat(
              !!item.ProductCost
                ? item.ProductCost
                : 0
            ),
            OrderMinimumQuantity:
              item.OrderMinimumQuantity,
            OrderMaximumQuantity:
              item.OrderMaximumQuantity,
            DisplayOrder: item.DisplayOrder,
            IsTaxExempt: item.IsTaxExempt,
            TaxCategoryId: item.TaxCategoryId,
            Published: item.Published,
            CreatedOnUtc: item.CreatedOnUtc,
            UpdatedOnUtc: item.UpdatedOnUtc,
            ColorCode: item.ColorCode,
            GenericCode: item.GenericCode,
            CombinationCode: item.CombinationCode,
            TherapeuticName: item.TherapeuticName,
            GenericName: item.GenericName,
            AdultDosage: item.AdultDosage,
            ChildrenDosage: item.ChildrenDosage,
            Indication: item.Indication,
            ContraIndication: item.ContraIndication,
            SideEffectsOrAdverseEffects:
              item.SideEffectsOrAdverseEffects,
            PrecautionOrWarning:
              item.PrecautionOrWarning,
            DrugInteractions: item.DrugInteractions,
            RecommendedAdultDosage:
              item.RecommendedAdultDosage,
            RecommendedChildrenDosage:
              item.RecommendedChildrenDosage,
            PregnancyAlert: item.PregnancyAlert,
            LactationAlert: item.LactationAlert,
            DrivingAlert: item.DrivingAlert,
            LiverAlert: item.LiverAlert,
            KidneyAlert: item.KidneyAlert,
            SymptomsAndOverdoseTreatment:
              item.SymptomsAndOverdoseTreatment,
            StorageCondition: item.StorageCondition,
            AvailabiltyPackage:
              item.AvailabiltyPackage,
            Cautions: item.Cautions,
            RatingSum: item.RatingSumm,
            TotalReviews: item.TotalReviews,
            MinStockQuantity: item.MinStockQuantity,
            StockQuantity: item.StockQuantity,
            DiscountIds: [],
            ManufacturerIds: [],
            Images: [],
            ManufacturerNames: [],
            count: count,
          },
          "modified"
        )

        if (
          !!item.Images &&
          item.Images.length > 0
        ) {
          item.Images.map((imageItem) => {
            newProduct.Images.push({
              Id: imageItem.Id,
              PictureId: imageItem.PictureId,
              DisplayOrder: imageItem.DisplayOrder,
              Src: imageItem.Src,
            })
          })
        }

        if (
          !!item.DiscountIds &&
          item.DiscountIds.length > 0
        ) {
          item.DiscountIds.map((id) => {
            newProduct.DiscountIds.push({
              id: id,
            })
          })
        }

        if (
          item.ManufacturerIds != null &&
          item.ManufacturerIds.length > 0
        ) {
          item.ManufacturerIds.map((id) => {
            newProduct.ManufacturerIds.push({
              id: id,
            })
          })
        }
        if (
          !!item.ManufacturerNames &&
          item.ManufacturerIds.length > 0
        ) {
          item.ManufacturerNames.map((name) => {
            newProduct.ManufacturerNames.push({
              name: id,
            })
          })
        }

        return true
      })
    } catch (e) {
      console.log("Error on creatione")
      console.log(JSON.stringify(e))
      return false
    }
  }

  async addHealthRecordsData(item) {
    try {
      await Db.write(() => {
        let newProduct = Db.create(
          "HealthRecord",
          {
            Id: item.id,
            CustomerId: item.patientId,
            Title: item.title,
            Description: item.description,
            HealthRecordType: item.healthRecordType,
            HealthRecordDoctor:
              item.healthRecordDoctor,
            HealthRecordPatient:
              item.healthRecordPatient,
            LastModifiedDate: item.lastModifiedDate,
            CreatedOn: item.createdOnUtc,
            HealthRecordImages: [],
          },
          "modified"
        )

        if (item.healthRecordImages.length > 0) {
          item.healthRecordImages.map(
            (imageItem) => {
              newProduct.HealthRecordImages.push({
                HealthRecordId:
                  imageItem.healthRecordId,
                PictureUrl: imageItem.pictureUrl,
                Id: imageItem.id,
                PictureId: imageItem.pictureId,
              })
            }
          )
        }

        return true
      })
    } catch (e) {
      console.log("Error on creation")
      console.log(e)

      return false
    }
  }

  async deleteHealthRecord(id) {
    try {
      await Db.write(() => {
        Db.delete(
          Db.objectForPrimaryKey("HealthRecord", id)
        )

        return true
      })
    } catch (e) {
      console.log("Error on creatione")
      console.log(JSON.stringify(e))
      return false
    }
  }

  async AddSearchHistory(item) {
    try {
      await Db.write(() => {
        let searchObj = Db.create(
          "SearchHistory",
          {
            Id: item.Id,
            Name: item.Name,
            ShortDescription: item.ShortDescription,
            AdminComment: item.AdminComment,
            Price: parseFloat(item.Price),
            OldPrice: parseFloat(item.OldPrice),
            PriceValue: item.PriceValue,
            PriceWithDiscount:
              item.PriceWithDiscount != null
                ? parseFloat(item.PriceWithDiscount)
                : 0,
            ColorCode: item.ColorCode,
            GenericName: item.GenericName,
            Sku: item.Sku,
            SCorPHCDiscount:
              item.SCorPHCDiscount != null
                ? parseFloat(item.SCorPHCDiscount)
                : 0,
            PriceWithSCorPHCDiscount:
              item.PriceWithSCorPHCDiscount != null
                ? parseFloat(
                    item.PriceWithSCorPHCDiscount
                  )
                : 0,
            ManufactureName: item.ManufactureName,
            Images: [],
          },
          "modified"
        )

        if (item.Images.length > 0) {
          item.Images.map((imageItem) => {
            searchObj.Images.push({
              Id: imageItem.Id,
              PictureId: imageItem.PictureId,
              DisplayOrder: imageItem.DisplayOrder,
              Src: imageItem.Src,
            })
          })
        }
      })

      return true
    } catch (e) {
      console.log("Error on creatione", e)
      return false
    }
  }

  async addPillReminder(item) {
    try {
      await Db.write(() => {
        let newPilldata = Db.create(
          "PillReminder",
          {
            Id: item.id,
            CustomerId: item.patientId,
            PillName: item.pillName,
            PillType: item.pillType,
            ShapeType: item.shapeType,
            ColorCode: item.colorCode,
            Dosage: item.dosage,
            DoseTimes: item.doseTimes,
            Image: item.image,
            Alarm: item.alarm,
            CreatedOnUtc: item.createdOnUtc,
            IsDeleted: item.isDeleted,
            IsCompleted: item.isCompleted,
            IsModulerView: item.isModulerView,
            IsRecurring: item.isRecurring,
            IsSelectAll: item.isSelectAll,
            RecurringValue: item.recurringValue,
            Brand: item.brand,
            Generic: item.generic,
            PillReminderSlots: [],
          },
          "modified"
        )

        if (item.pillReminderSlots.length > 0) {
          item.pillReminderSlots.map(
            (imageItem) => {
              newPilldata.PillReminderSlots.push({
                Id: imageItem.id,
                PillReminderId:
                  imageItem.pillReminderId,
                Day: new Date(imageItem.day),
                Time: imageItem.time,
                IsSkipped: imageItem.isSkipped,
                IsTaken: imageItem.isTaken,
                CreatedOnUtc:
                  imageItem.createdOnUtc,
                UpdatedOnUtc:
                  imageItem.updatedOnUtc,
              })
            }
          )
        }

        return true
      })
    } catch (e) {
      console.log("Error on creation pillreminder")
      console.log(e)

      return false
    }
  }

  async addWishListData(item) {
    try {
      await Db.write(() => {
        let newProduct = Db.create(
          "WishListDB",
          {
            Id: item.Id,
            Name: item.Name,
            ShortDescription: item.ShortDescription,
            DisplayOrder: item.DisplayOrder,
            Price: parseFloat(item.Price),
            OldPrice: parseFloat(item.OldPrice),
            PriceWithDiscount:
              item.PriceWithDiscount != null
                ? parseFloat(item.PriceWithDiscount)
                : 0,
            Sku: item.Sku,
            PriceValue: item.PriceValue,
            ProductCost: item.ProductCost,
            OrderMinimumQuantity:
              item.OrderMinimumQuantity,
            OrderMaximumQuantity:
              item.OrderMaximumQuantity,
            DisplayOrder: item.DisplayOrder,
            IsTaxExempt: item.IsTaxExempt,
            TaxCategoryId: item.TaxCategoryId,
            Published: item.Published,
            Deleted: item.Deleted,
            CreatedOnUtc: item.CreatedOnUtc,
            UpdatedOnUtc: item.UpdatedOnUtc,
            ColorCode: item.ColorCode,
            GenericCode: item.GenericCode,
            PackageDescription:
              item.PackageDescription,
            PackageInNumbers: item.PackageInNumbers,
            PricePerUnit: item.PricePerUnit,
            TherapeuticName: item.TherapeuticName,
            GenericName: item.GenericName,
            AdultDosage: item.AdultDosage,
            ChildrenDosage: item.ChildrenDosage,
            Indication: item.Indication,
            ContraIndication: item.ContraIndication,
            SideEffectsOrAdverseEffects:
              item.SideEffectsOrAdverseEffects,
            PrecautionOrWarning:
              item.PrecautionOrWarning,
            DrugInteractions: item.DrugInteractions,
            RecomandAdultDosage:
              item.RecomandAdultDosage,
            RecomandChildrenDosage:
              item.RecomandChildrenDosage,
            PregnancyAlert: item.PregnancyAlert,
            LactationAlert: item.LactationAlert,
            DrivingAlert: item.DrivingAlert,
            LiverAlert: item.LiverAlert,
            KidneyAlert: item.KidneyAlert,
            ManufacturerName: item.ManufacturerName,
            SymptomsAndOverdoseTreatment:
              item.SymptomsAndOverdoseTreatment,
            StorageCondition: item.StorageCondition,
            AvailabiltyPackage:
              item.AvailabiltyPackage,
            ManufacturerId: item.ManufacturerId,
            IsPriceIncludingTax:
              item.IsPriceIncludingTax,
            TaxId: item.TaxId,
            DiscountIds: item.DiscountIds,
            ManufacturerIds:
              item.ManufacturerIds != null
                ? item.ManufacturerIds
                : [],
            Images: item.Images,
          },
          "modified"
        )
        return true
      })
    } catch (e) {
      console.log("Error on creation")
      console.log(e)

      return false
    }
  }

  async addOrderData(item) {
    console.log(item)
    try {
      await Db.write(() => {
        let newProduct = Db.create(
          "Orders",
          {
            OrderId: item.Id,
            StoreId: item.StoreId,
            PaymentMethodSystemName:
              item.PaymentMethodSystemName,
            CustomerCurrencyCode:
              item.CustomerCurrencyCode,
            OrderSubtotalInclTax: parseFloat(
              item.OrderSubtotalInclTax
            ),
            OrderSubtotalExclTax: parseFloat(
              item.OrderSubtotalExclTax
            ),
            OrderSubTotalDiscountInclTax:
              parseFloat(
                item.OrderSubTotalDiscountInclTax
              ),
            OrderSubTotalDiscountExclTax:
              parseFloat(
                item.OrderSubTotalDiscountExclTax
              ),
            OrderShippingInclTax: parseFloat(
              item.OrderShippingInclTax
            ),
            OrderShippingExclTax: parseFloat(
              item.OrderShippingExclTax
            ),
            PaymentMethodAdditionalFeeInclTax:
              parseFloat(
                item.PaymentMethodAdditionalFeeInclTax
              ),
            PaymentMethodAdditionalFeeExclTax:
              parseFloat(
                item.PaymentMethodAdditionalFeeExclTax
              ),
            TaxRates: item.TaxRates,
            OrderTax: parseFloat(item.OrderTax),
            OrderDiscount: parseFloat(
              item.OrderDiscount
            ),
            OrderTotal: parseFloat(item.OrderTotal),
            RefundedAmount: parseFloat(
              item.RefundedAmount
            ),
            CheckoutAttributeDescription:
              item.CheckoutAttributeDescription,
            CustomerLanguageId:
              item.CustomerLanguageId,
            CreatedOnUtc: item.CreatedOnUtc,
            CustomerId: item.CustomerId,
            OrderStatus: item.OrderStatus,
            OrderStatusId: item.OrderStatusId,
            PaymentStatus: item.PaymentStatus,
            ShippingStatus: item.ShippingStatus,
            CustomerTaxDisplayType:
              item.CustomerTaxDisplayType,
            MinimumOrder: item.MinimumOrder,
            LastActivityTimeStamp:
              item.LastActivityTimeStamp,
            PrescriptionId: !!item.PrescriptionId
              ? parseInt(item.PrescriptionId)
              : 0,
            BillRequired: item.BillRequired,
            DeviceType: item.DeviceType,
            Version: item.Version,
            FirstName: item.FirstName,
            LastName: item.LastName,
            Email: item.Email,
            Company: item.Company,
            City: item.City,
            Address1: item.Address1,
            Address2: item.Address2,
            ZipPostalCode: item.ZipPostalCode,
            PhoneNumber: item.PhoneNumber,
            Longitude: `${item.Longitude}`,
            Latitude: `${item.Latitude}`,
            AddressCategory: item.AddressCategory,
            PatientName: item.PatientName,
            CancelledBy: item.CancelledBy,
            PrescriptionUrl: item.PrescriptionUrl,
            PictureId: !!item.PictureId
              ? parseInt(item.PictureId)
              : 0,
            HasPrescription: item.HasPrescription,
            IsTest: item.IsTest,
            VendorName: item.VendorName,
            DoctorName: item.DoctorName,
            PackageCharge: item.PackageCharge,
            ServiceCharge: item.ServiceCharge,
            DeliveryFee: item.DeliveryFee,
            IsFeedback: item.IsFeedback,
            CustomerPhoneNumber:
              item.CustomerPhoneNumber,
            CustomerName: item.CustomerName,
            IsOrderForOther: item.IsOrderForOther,
            IsOrderForPhd: item.IsOrderForPhd,
            IsOrderForSeniorCitizen:
              item.IsOrderForSeniorCitizen,
            OrderTotalSCorPHCDiscount: parseFloat(
              item.OrderTotalSCorPHCDiscount
            ),
            IsReturnOrder: item.IsReturnOrder,
            OrderItems: [],
            OrderStatusLogs: [],
          },
          "modified"
        )

        if (item.OrderNotes.length > 0) {
          item.OrderNotes.map((imageItem) => {
            newProduct.OrderNotes.push({
              OrderId: imageItem.OrderId,
              ChangedBy: imageItem.ChangedBy,
              OrderStatusId:
                imageItem.OrderStatusId,
              CreatedOnUtc: imageItem.CreatedOnUtc,
              Comment: imageItem.Comment,
              UserId: imageItem.UserId,
            })
          })
        }
        if (item.OrderItems.length > 0) {
          item.OrderItems.map((item) => {
            let product = item.Product
            let productData = {
              Id: product.Id,
              Name: product.Name,
              ShortDescription:
                product.ShortDescription,
              Price: parseFloat(product.Price),
              OldPrice: parseFloat(
                product.OldPrice
              ),
              PriceWithDiscount:
                product.PriceWithDiscount,
              PriceValue: product.PriceValue,
              ColorCode: product.ColorCode,
              ManufactureName:
                product.ManufactureName,
              GenericName: product.GenericName,
              SCorPHCDiscount: parseFloat(
                product.SCorPHCDiscount
              ),
              PriceWithSCorPHCDiscount: parseFloat(
                product.PriceWithSCorPHCDiscount
              ),
              Sku: product.Sku,
              Images: [],
            }

            if (product.Images != null) {
              if (product.Images.length > 0) {
                product.Images.map((imageItem) => {
                  productData.Images.push({
                    Id: imageItem.Id,
                    PictureId: imageItem.PictureId,
                    Position: imageItem.position,
                    Src: imageItem.Src,
                  })
                })
              }
            }

            let orderItem =
              newProduct.OrderItems.push({
                Id: item.Id,
                Quantity: item.Quantity,
                UnitPriceInclTax: parseFloat(
                  item.UnitPriceInclTax
                ),
                UnitPriceExclTax: parseFloat(
                  item.UnitPriceExclTax
                ),
                PriceInclTax: parseFloat(
                  item.PriceInclTax
                ),
                PriceExclTax: parseFloat(
                  item.PriceExclTax
                ),
                DiscountAmountInclTax: parseFloat(
                  item.DiscountAmountInclTax
                ),
                DiscountAmountExclTax: parseFloat(
                  item.DiscountAmountExclTax
                ),
                OriginalProductCost:
                  item.OriginalProductCost,
                AttributeDescription:
                  item.AttributeDescription,
                DownloadCount: item.DownloadCount,
                IsDownloadActivated:
                  item.IsDownloadActivated,
                LicenseDownloadId:
                  item.LicenseDownloadId,
                ItemWeight: item.ItemWeight,
                ProductId: item.ProductId,
                ProductName: item.ProductName,
                ShoppingCartTypeId:
                  item.ShoppingCartTypeId,
                ColorCode: item.ColorCode,
                OrderId: item.OrderId,
                Product: productData,
              })

            // orderItem.Product = productData
          })
        }

        return true
      })
    } catch (e) {
      console.log("save cart items error")
      console.log(e)

      return false
    }
  }
  async addChatData(item){
    try {
      await Db.write(() => {
        let newChatdata = Db.create(
          "Chat",
          {
            id: item.id,
            pharmacyId: item.pharmacyId,
            userType: item.userType,
            patientName: item.patientName,
            pharmacistName: item.pharmacistName,
            profilePicture: item.profilePicture,
            patientId: item.patientId,
            pharmacistId: item.pharmacistId,
            lastMessage: item.lastMessage,
            messageType: item.messageType,
            messageCount: item.messageCount,
            lastUpdatedTimeTicks: item.lastUpdatedTimeTicks,
           
            chatMessages: [],
          },
          "modified"
        )

        if (item.chatMessages.length > 0) {
          item.chatMessages.map(
            (imageItem) => {
              newChatdata.chatMessages.push({
                id: imageItem.id,
                chatId:
                  imageItem.chatId,
                  messageBody: messageBody,
                  userType: imageItem.userType,
                  messageType: imageItem.messageType,
                  pictureId: imageItem.pictureId,
                  pictureUrl:
                  imageItem.pictureUrl,
                  parentChatId:
                  imageItem.parentChatId,
                  parentChatFeild:
                  imageItem.parentChatFeild,
                  createdOn:
                  imageItem.createdOn,
              })
            }
          )
        }

        return true
      })
    } catch (e) {
      console.log("Error on creation chat")
      console.log(e)

      return false
    }
  }
}

module.exports = new CartDB()
