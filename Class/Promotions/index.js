import React, { Component } from 'react'
import { Text, StyleSheet, View, StatusBar, Dimensions, ImageBackground, Image, ScrollView } from 'react-native'
import BackButtonHeader from "../Profile/components/backButtonHeader";
import { SafeAreaView } from "react-native-safe-area-context"
import Icon from 'react-native-vector-icons/FontAwesome5';
import { TouchableOpacity } from 'react-native-gesture-handler';
import images from "../BaseClass/Images";
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const { width, height } = Dimensions.get('window');

export default class Promotions extends Component {
  render() {
    return (
      <SafeAreaView style={{ height: "100%", width: "100%" ,backgroundColor:"#fff"}}>
      <ImageBackground style={{ height: "100%", width: "100%" }} source={images.bg}>
        <SafeAreaView style={{ flex: 1}}>
          {this.headerPromotions()}

          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.blankView}>
              <View style={{ padding: 27, paddingBottom: 20, flexDirection: 'row' }}>
                {/* header Post view */}
                <Image source={require('../../assets/frontCall.jpg')} style={{ borderRadius: 10, width: 44, height: 44 }} />

                {this.ownerPost()}
              </View>

              <Image source={require('../../assets/medPromotion.jpg')} style={{ width: width, height: height / 3.5 }} />
              {this.postFooter()}
            </View>


            <View style={styles.blankView}>
              <View style={{ padding: 27, paddingBottom: 20, flexDirection: 'row' }}>
                {/* header Post view */}
                <Image source={require('../../assets/frontCall.jpg')} style={{ borderRadius: 10, width: 44, height: 44 }} />

                {this.ownerPost()}
              </View>
              
              <Text style={{paddingLeft: 20, paddingBottom: 20}}>sdniweidnwininmin</Text>

              <Image source={require('../../assets/medPromotion.jpg')} style={{ width: width, height: height / 3.5 }} />
              {this.postFooter()}
            </View>
          </ScrollView>
          <StatusBar translucent barStyle="dark-content" backgroundColor="transparent" />
        </SafeAreaView>
      </ImageBackground>
      </SafeAreaView>
    )
  }

  ownerPost(){
    return(
      <View style={{ flexDirection: 'column', marginLeft: width / 30 }}>
        <Text style={{ fontSize: 16, fontWeight: '600' }}>
          David Ninh
        </Text>
        <Text style={{ fontSize: 12, color: '#858585' }}>
          1h
        </Text>
      </View>
    );
  }

  postFooter() {
    return (
      <View style={{ padding: 15, paddingLeft: width / 16, flexDirection: 'row' }}>
        {/* Like and Comment VIEW */}
        <TouchableOpacity style={{ flexDirection: 'row', marginRight: width / 10 }} onPress={() => alert('Here you can like the post')}>
          <MaterialIcon name="thumb-up-outline" size={22} color="#8D8D8D" />
          <Text style={styles.textInt}>75 Likes</Text>
        </TouchableOpacity>

        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => this.props.navigation.navigate('Comments')}>
          <MaterialIcon name="comment-text-outline" size={22} color="#8D8D8D" />
          <Text style={styles.textInt}>26 Comments</Text>
        </TouchableOpacity>
      </View>
    );
  }

  headerPromotions() {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
        <BackButtonHeader navigation={this.props.navigation} titleHeader={"Promotions"} />
        <TouchableOpacity>
          <Icon name="search" size={17} color="#000" style={{ marginRight: width / 12 }} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  blankView: {
    backgroundColor: 'white',
    width: width,
    height: height / 2,
    marginTop: height / 30,
    borderRadius: 30
  },
  textInt: {
    color: '#8D8D8D',
    marginLeft: width / 40
  }
})