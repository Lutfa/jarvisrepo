import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, StatusBar, ImageBackground, Dimensions, TextInput, Image } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import BackButtonHeader from '../../../Profile/components/backButtonHeader'
import images from "../../../BaseClass/Images";
import Icon from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient"

const { width, height } = Dimensions.get('window');

export default class Comments extends Component {
  constructor(props) {
    super(props)
    this.state = {
      message: null
    }
  }

  render() {
    return (
      <ImageBackground style={{ height: "100%", width: "100%" }} source={images.bg}>
        <SafeAreaView style={{ flex: 1 }}>
          {this.headerComments()}

          <View style={styles.blankView}>
            <View style={{flexDirection:'row', padding:30}}>
              <Image source={require('../../../../assets/frontCall.jpg')} style={{ borderRadius: 10, width: 44, height: 44 }} />
              
              <View>
                <LinearGradient
                  start={{ x: 0, y: 1 }}
                  end={{ x: 1, y: 1 }}
                  
                  colors={[
                    "#FFF6EF",
                    "#F1F3FF",
                    
                  ]}
                  style={{ marginLeft: 20, marginTop: 5, borderRadius: 15, height: 55, width: width /1.4, padding: 10 }}>
                  <Text style={{ fontSize: 13, color: "#000", fontWeight:'bold' }}>David Ninh</Text>
                  <Text style={{ fontSize: 13, color: "#000" }}>When is this offer ending at your outlet</Text>
                </LinearGradient>
              
                <View style={{flexDirection:'row', alignItems:'center', marginLeft: width/12}}>
                  <Text style={{color: '#858585'}}>2d</Text>
                  <Text style={{color: '#858585', fontWeight: 'bold', marginLeft: 10}}>Like</Text>
                  <Text style={{color: '#858585', fontWeight: 'bold', marginLeft: 10}}>Reply</Text>
                  
                  <TouchableOpacity style={{marginLeft: width/6, flexDirection:'row', marginTop: 5}}>
                    <Icon name="thumbs-up" size={18} color="#1D90FF" />
                    <Text style={{color:'#858585', marginLeft: 5}}>23</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>


            {this.footerChat()}
          </View>
        </SafeAreaView>
      </ImageBackground>
    )
  }


  headerComments() {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
        <BackButtonHeader navigation={this.props.navigation} titleHeader={"Comments"} />
        <TouchableOpacity>
          <Icon name="search" size={17} color="#000" style={{ marginRight: width / 12 }} />
        </TouchableOpacity>
        <StatusBar translucent barStyle="dark-content" backgroundColor="transparent" />
      </View>
    );
  }
  
  footerChat() {
    return (
      <View>
        {/* Input VIEW */}
        <View style={{ position: 'absolute', justifyContent: 'center', top: height / 1.6, marginLeft: width / 30, elevation: 40, borderRadius: 40, width: width / 1.35, height: height / 15, backgroundColor: 'white' }}>
          <TouchableOpacity style={{ position: 'absolute', marginLeft: width / 30 }}>
            <Icon name="smile" size={22} color="#000" />
          </TouchableOpacity>
          <TextInput
            style={styles.input}
            onChangeText={(text) => this.setState({ message: text })}
            value={this.state.message}
          />
        </View>
  
        <TouchableOpacity style={{ elevation: 20, position: 'absolute', borderRadius: 40, justifyContent: 'center', alignItems: 'center', marginLeft: width / 1.20, top: height / 1.6, width: 47, height: 47, backgroundColor: '#FE3C28' }}>
          <Icon name="paper-plane" size={22} color="#fff" />
        </TouchableOpacity>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  blankView: {
    backgroundColor: 'white',
    width: width,
    height: height,
    marginTop: height / 30,
    borderRadius: 30
  },
  input: {
    height: height / 15,
    padding: 10,
    width: width / 1.55,
    marginLeft: width / 10,
    borderRadius: 30
  }
})
